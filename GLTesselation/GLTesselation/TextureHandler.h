#ifndef DEF_TEXTUREHANDLER_H
#define DEF_TEXTUREHANDLER_H

#include "GDevice.h"
#include "TextureEXType.h"
#include <stdio.h>
#include <vector>
#include <string>

class TextureHandler
{
private:

	// Attributes
	GDevice* mDevice;

	// Private Methods
	BIGINT LoadBMP( const CCHAR* pTextName, BBOOL pWrap );
	BIGINT LoadPNG( const CCHAR* pTextName, BBOOL pWrap );
	BIGINT LoadJPG( const CCHAR* pTextName, BBOOL pWrap );
	BIGINT LoadDDS( const CCHAR* pTextName, BBOOL pWrap );
	BIGINT LoadTGA( const CCHAR* pTextName, BBOOL pWrap );

public:

	// Constructor & Destructor
	TextureHandler(GDevice* pDevice);
	~TextureHandler();

	// Methods
	BIGINT Load(const CCHAR* pTextName, ExtType pType, BBOOL pWrap);
	BIGINT LoadArray(std::vector<std::string> pFilenames, std::vector<ExtType> pTypes, BBOOL pWrap);

};

#endif