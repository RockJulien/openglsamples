#ifndef DEF_OCELOTCAMERA_H
#define DEF_OCELOTCAMERA_H

#include "IOcelotCamera.h"
#include "OcelotCameraInfo.h"
#include "..\Maths\Plane.h"
#include "..\Maths\MatrixView.h"
#include "..\Maths\MatrixProjection.h"
#include <vector>

namespace Ocelot
{
	class OcelotCamera : public IOcelotCamera
	{
	private:

		// Attributes
		MatrixView       m_matView;
		MatrixProjection m_matProj;

		// View and Projection attributes.
		OcelotCameraInfo m_info;

		// Camera's frustum planes.
		std::vector<Plane> m_frustrumPlanes;

		// Private Methods.
		FFLOAT distanceToPlane(const Vector3& position, Plane plane); // will be used in CullMesh(...);

	public:

		// Constructor & Destructor
		OcelotCamera();
		OcelotCamera(OcelotCameraInfo info);
		~OcelotCamera();

		// Methods
		VVOID update(FFLOAT deltaTime);
		VVOID computeFrustumPlane();
		BBOOL cullMesh(const FFLOAT& posX, const FFLOAT& posY, const FFLOAT& posZ, const FFLOAT& radius);

		// Accessors
		inline MatrixView       View() const { return m_matView;}
		inline MatrixProjection Proj() const { return m_matProj;}
		inline OcelotCameraInfo Info() const { return m_info;}
		inline VVOID            SetView(MatrixView view) { m_matView = view;}
		inline VVOID            SetProj(MatrixProjection proj) { m_matProj = proj;}
		inline VVOID            SetInfo(OcelotCameraInfo info) { m_info = info;}
	};
}

#endif