#ifndef DEF_IOCELOTCAMERACONTROLLER_H
#define DEF_IOCELOTCAMERACONTROLLER_H

#include "CameraDataType.h"
#include "..\OSCheck.h"

namespace Ocelot
{
	enum Direction
	{
		Forward,
		Backward,
		LeftSide,
		RightSide
	};

	// Interface for camera controllers
	class IOcelotCameraController
	{
	public:

			// Constructor & Destructor
			IOcelotCameraController(){}
	virtual ~IOcelotCameraController(){}

			// Methods
	virtual VVOID update(FFLOAT deltaTime) = 0;
	virtual VVOID move(Direction dir)      = 0;
	virtual VVOID pitch(FFLOAT angle)      = 0;
	virtual VVOID yaw(FFLOAT angle)        = 0;
	virtual VVOID roll(FFLOAT angle)       = 0;

	};
}

#endif