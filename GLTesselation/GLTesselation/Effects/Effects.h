#ifndef DEF_EFFECTS_H
#define DEF_EFFECTS_H

// Includes
#include "TessellationEffect.h"

// Namespaces

// Class definition
class Effects
{
private:

	// Attributes
	static TessellationEffect* sTessellationFX;
	
public:

	// Methods
	static VVOID InitializeAll(GDevice* pDevice);
	static VVOID DestroyAll();

	// Shaders handles
	static TessellationEffect* TessellationFX();

};

#endif