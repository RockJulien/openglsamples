#ifndef DEF_INPUTLAYOUT_H
#define DEF_INPUTLAYOUT_H

// Includes
#include <vector>
#include "..\GDevice.h"
#include "InputElementDesc.h"

// Class definition
class InputLayout
{
private:

	// Attributes
	GDevice*					  mDevice;
	std::vector<InputElementDesc> mLayout;

public:

	// Constructor
	InputLayout(GDevice* pDevice);
	~InputLayout();

	// Methods
	VVOID AddElement(const InputElementDesc& pDesc);
	VVOID SetVertexLayout(const UBIGINT& pVBO_Id);
	VVOID BindVertexLayout(const UBIGINT& pShaderId);

	// Accessors
	const std::vector<InputElementDesc>& Layout() const;

};

#endif