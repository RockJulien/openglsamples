#include "OcelotLight.h"

using namespace Ocelot;
using namespace Ocelot::OcelotColor;

OcelotParallelLight::OcelotParallelLight() :
m_cAmbient(GREY), m_cDiffuse(LIGHTGREY), m_cSpecular(DARKGREY), m_vDirection(0.0f, -0.8f, -1.0f), m_fSpecularPower(5.0f)
{

}

OcelotParallelLight::OcelotParallelLight(Vector3 direction, Color ambient, Color diffuse, Color specular, FFLOAT power) :
m_cAmbient(ambient), m_cDiffuse(diffuse), m_cSpecular(specular), m_vDirection(direction), m_fSpecularPower(power)
{

}

OcelotParallelLight::~OcelotParallelLight()
{

}

OcelotParallelLight::operator float* ()
{
	FFLOAT* matVec3[16];
	FFLOAT* amb = (FFLOAT*)m_cAmbient;
	FFLOAT* dif = (FFLOAT*)m_cDiffuse;
	FFLOAT* spe = (FFLOAT*)m_cSpecular;
	FFLOAT* dir = (FFLOAT*)m_vDirection;

	matVec3[0] = amb;   // R
	matVec3[1] = amb+1; // G
	matVec3[2] = amb+2; // B
	matVec3[3] = amb+3; // A
	matVec3[4] = dif;   // R
	matVec3[5] = dif+1; // G
	matVec3[6] = dif+2; // B
	matVec3[7] = dif+3; // A
	matVec3[8] = spe;   // R
	matVec3[9] = spe+1; // G
	matVec3[10] = spe+2; // B
	matVec3[11] = spe+3; // A
	matVec3[12] = dir;   // X
	matVec3[13] = dir+1; // Y
	matVec3[14] = dir+2; // Z
	matVec3[15] = &m_fSpecularPower;

	return matVec3[0];
}

OcelotParallelLight::operator const float* () const
{
	const FFLOAT* matVec3[16];
	const FFLOAT* amb = (const FFLOAT*)m_cAmbient;
	const FFLOAT* dif = (const FFLOAT*)m_cDiffuse;
	const FFLOAT* spe = (const FFLOAT*)m_cSpecular;
	const FFLOAT* dir = (const FFLOAT*)m_vDirection;

	matVec3[0] = amb;   // R
	matVec3[1] = amb+1; // G
	matVec3[2] = amb+2; // B
	matVec3[3] = amb+3; // A
	matVec3[4] = dif;   // R
	matVec3[5] = dif+1; // G
	matVec3[6] = dif+2; // B
	matVec3[7] = dif+3; // A
	matVec3[8] = spe;   // R
	matVec3[9] = spe+1; // G
	matVec3[10] = spe+2; // B
	matVec3[11] = spe+3; // A
	matVec3[12] = dir;   // X
	matVec3[13] = dir+1; // Y
	matVec3[14] = dir+2; // Z
	matVec3[15] = &m_fSpecularPower;

	return matVec3[0];
}

OcelotPointLight::OcelotPointLight() :
m_cAmbient(GREY), m_cDiffuse(LIGHTGREY), m_cSpecular(DARKGREY), m_vPosition(0.0f, 50.0f, 0.0f), 
m_vAttenuation(0.0f, 1.0f, 0.0f), m_fRange(50.0f), m_fSpecularPower(5.0f)
{

}

OcelotPointLight::OcelotPointLight(Vector3 position, Vector3 attenuation, Color ambient, Color diffuse, Color specular, FFLOAT range, FFLOAT power) :
m_cAmbient(ambient), m_cDiffuse(diffuse), m_cSpecular(specular), m_vPosition(position), 
m_vAttenuation(attenuation), m_fRange(range), m_fSpecularPower(power)
{

}

OcelotPointLight::~OcelotPointLight()
{

}

OcelotPointLight::operator float* ()
{
	FFLOAT* matVec3[20];
	FFLOAT* amb = (FFLOAT*)m_cAmbient;
	FFLOAT* dif = (FFLOAT*)m_cDiffuse;
	FFLOAT* spe = (FFLOAT*)m_cSpecular;
	FFLOAT* pos = (FFLOAT*)m_vPosition;
	FFLOAT* att = (FFLOAT*)m_vAttenuation;

	matVec3[0] = amb;   // R
	matVec3[1] = amb+1; // G
	matVec3[2] = amb+2; // B
	matVec3[3] = amb+3; // A
	matVec3[4] = dif;   // R
	matVec3[5] = dif+1; // G
	matVec3[6] = dif+2; // B
	matVec3[7] = dif+3; // A
	matVec3[8] = spe;   // R
	matVec3[9] = spe+1; // G
	matVec3[10] = spe+2; // B
	matVec3[11] = spe+3; // A
	matVec3[12] = pos;   // X
	matVec3[13] = pos+1; // Y
	matVec3[14] = pos+2; // Z
	matVec3[15] = att;   // X
	matVec3[16] = att+1; // Y
	matVec3[17] = att+2; // Z
	matVec3[18] = &m_fRange;
	matVec3[19] = &m_fSpecularPower;

	return matVec3[0];
}

OcelotPointLight::operator const float* () const
{
	const FFLOAT* matVec3[20];
	const FFLOAT* amb = (const FFLOAT*)m_cAmbient;
	const FFLOAT* dif = (const FFLOAT*)m_cDiffuse;
	const FFLOAT* spe = (const FFLOAT*)m_cSpecular;
	const FFLOAT* pos = (const FFLOAT*)m_vPosition;
	const FFLOAT* att = (const FFLOAT*)m_vAttenuation;

	matVec3[0] = amb;   // R
	matVec3[1] = amb+1; // G
	matVec3[2] = amb+2; // B
	matVec3[3] = amb+3; // A
	matVec3[4] = dif;   // R
	matVec3[5] = dif+1; // G
	matVec3[6] = dif+2; // B
	matVec3[7] = dif+3; // A
	matVec3[8] = spe;   // R
	matVec3[9] = spe+1; // G
	matVec3[10] = spe+2; // B
	matVec3[11] = spe+3; // A
	matVec3[12] = pos;   // X
	matVec3[13] = pos+1; // Y
	matVec3[14] = pos+2; // Z
	matVec3[15] = att;   // X
	matVec3[16] = att+1; // Y
	matVec3[17] = att+2; // Z
	matVec3[18] = &m_fRange;
	matVec3[19] = &m_fSpecularPower;

	return matVec3[0];
}

OcelotSpotLight::OcelotSpotLight() :
m_cAmbient(GREY), m_cDiffuse(LIGHTGREY), m_cSpecular(DARKGREY), m_vPosition(0.0f, 50.0f, 50.0f), m_vDirection(Vector3(0.0f, 0.0f, 0.0f) - m_vPosition), 
m_vAttenuation(0.0f, 1.0f, 0.0f), m_fRange(50.0f), m_fSpotPower(50.0f), m_fSpecularPower(5.0f)
{
	
}

OcelotSpotLight::OcelotSpotLight(Vector3 position, Vector3 direction, Vector3 attenuation, Color ambient, Color diffuse, Color specular, FFLOAT range, FFLOAT spotPower, FFLOAT specPower) :
m_cAmbient(ambient), m_cDiffuse(diffuse), m_cSpecular(specular), m_vPosition(position), m_vDirection(direction), 
m_vAttenuation(attenuation), m_fRange(range), m_fSpotPower(spotPower), m_fSpecularPower(specPower)
{

}

OcelotSpotLight::~OcelotSpotLight()
{

}

OcelotSpotLight::operator float* ()
{
	FFLOAT* matVec3[24];
	FFLOAT* amb = (FFLOAT*)m_cAmbient;
	FFLOAT* dif = (FFLOAT*)m_cDiffuse;
	FFLOAT* spe = (FFLOAT*)m_cSpecular;
	FFLOAT* pos = (FFLOAT*)m_vPosition;
	FFLOAT* dir = (FFLOAT*)m_vDirection;
	FFLOAT* att = (FFLOAT*)m_vAttenuation;

	matVec3[0] = amb;   // R
	matVec3[1] = amb+1; // G
	matVec3[2] = amb+2; // B
	matVec3[3] = amb+3; // A
	matVec3[4] = dif;   // R
	matVec3[5] = dif+1; // G
	matVec3[6] = dif+2; // B
	matVec3[7] = dif+3; // A
	matVec3[8] = spe;   // R
	matVec3[9] = spe+1; // G
	matVec3[10] = spe+2; // B
	matVec3[11] = spe+3; // A
	matVec3[12] = pos;   // X
	matVec3[13] = pos+1; // Y
	matVec3[14] = pos+2; // Z
	matVec3[15] = dir;   // X
	matVec3[16] = dir+1; // Y
	matVec3[17] = dir+2; // Z
	matVec3[18] = att;   // X
	matVec3[19] = att+1; // Y
	matVec3[20] = att+2; // Z
	matVec3[21] = &m_fRange;
	matVec3[22] = &m_fSpotPower;
	matVec3[23] = &m_fSpecularPower;

	return matVec3[0];
}

OcelotSpotLight::operator const float* () const
{
	const FFLOAT* matVec3[24];
	const FFLOAT* amb = (const FFLOAT*)m_cAmbient;
	const FFLOAT* dif = (const FFLOAT*)m_cDiffuse;
	const FFLOAT* spe = (const FFLOAT*)m_cSpecular;
	const FFLOAT* pos = (const FFLOAT*)m_vPosition;
	const FFLOAT* dir = (const FFLOAT*)m_vDirection;
	const FFLOAT* att = (const FFLOAT*)m_vAttenuation;

	matVec3[0] = amb;   // R
	matVec3[1] = amb+1; // G
	matVec3[2] = amb+2; // B
	matVec3[3] = amb+3; // A
	matVec3[4] = dif;   // R
	matVec3[5] = dif+1; // G
	matVec3[6] = dif+2; // B
	matVec3[7] = dif+3; // A
	matVec3[8] = spe;   // R
	matVec3[9] = spe+1; // G
	matVec3[10] = spe+2; // B
	matVec3[11] = spe+3; // A
	matVec3[12] = pos;   // X
	matVec3[13] = pos+1; // Y
	matVec3[14] = pos+2; // Z
	matVec3[15] = dir;   // X
	matVec3[16] = dir+1; // Y
	matVec3[17] = dir+2; // Z
	matVec3[18] = att;   // X
	matVec3[19] = att+1; // Y
	matVec3[20] = att+2; // Z
	matVec3[21] = &m_fRange;
	matVec3[22] = &m_fSpotPower;
	matVec3[23] = &m_fSpecularPower;

	return matVec3[0];
}
