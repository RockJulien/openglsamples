#include "Effects.h"

// Namespaces used

// Static initialization
TessellationEffect* Effects::sTessellationFX = NULL;

/*****************************************************************************************************/
VVOID Effects::InitializeAll(GDevice* pDevice)
{
	if
		( Effects::sTessellationFX == NULL )
	{
		Effects::sTessellationFX = new TessellationEffect( pDevice );
	}
}

/*****************************************************************************************************/
VVOID Effects::DestroyAll()
{
	DeletePointer( Effects::sTessellationFX );
}

/*****************************************************************************************************/
TessellationEffect* Effects::TessellationFX()
{
	return Effects::sTessellationFX;
}

