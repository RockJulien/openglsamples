#ifndef DEF_TESSELATIONEFFECT_H
#define DEF_TESSELATIONEFFECT_H

// Includes
#include <vector>
#include <string>
#include "..\GDevice.h"
#include "..\Material.h"
#include "..\Maths\Color.h"
#include "..\Maths\Matrix4x4.h"
#include "..\OcelotLight.h"
#include "EffectTechnique.h"

// Namespaces


// Class definition
class TessellationEffect
{
private:

	// Attributes
	EffectTechnique* mCurrentTechnique;
	GDevice*		 mDevice;

public:

	// Constructor & Destructor
	TessellationEffect(GDevice* pDevice);
	~TessellationEffect();

	// Methods
	VVOID SetWorldViewProj(const Matrix4x4& pMatrix);
	VVOID SetWorld(const Matrix4x4& pMatrix);
	VVOID SetWorldInvTranspose(const Matrix4x4& pMatrix);
	VVOID SetTexTransform(const Matrix4x4& pMatrix);
	VVOID SetEyePosW(const Vector3& pEyePos);
	VVOID SetFogColor(const Ocelot::Color& pFogColor);
	VVOID SetFogStart(const FFLOAT& pFogStart);
	VVOID SetFogRange(const FFLOAT& pFogRange);
	VVOID SetDirLights(const Ocelot::OcelotParallelLight* pLights, UBIGINT pCount);
	VVOID SetMaterial(const Material& pMaterial);
	VVOID SetDiffuseMap(const UBIGINT& pUnit);

	VVOID MakeCurrent(EffectTechnique* pCurrent);

	// Technique getters
	EffectTechnique* TessTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
	
};

#endif