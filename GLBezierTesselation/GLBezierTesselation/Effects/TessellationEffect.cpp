#include "TessellationEffect.h"
#include "Constants.h"
#include <sstream>

// Namespaces used
using namespace std;
using namespace Ocelot;

/*************************************************************************************************/
TessellationEffect::TessellationEffect(GDevice* pDevice) :
mCurrentTechnique(NULL), mDevice(pDevice)
{
	
}

/*************************************************************************************************/
TessellationEffect::~TessellationEffect()
{

}

/*************************************************************************************************/
VVOID TessellationEffect::SetWorldViewProj(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gWorldViewProj");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/*************************************************************************************************/
VVOID TessellationEffect::SetWorld(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gWorld");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/*************************************************************************************************/
VVOID TessellationEffect::SetWorldInvTranspose(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gWorldInvTranspose");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/***********************************************************************************************/
VVOID TessellationEffect::SetTexTransform(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gTexTransform");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/*************************************************************************************************/
VVOID TessellationEffect::SetEyePosW(const Vector3& pEyePos)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gEyePosW");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform3fv( lShaderLocation, 1, pEyePos );
}

/*************************************************************************************************/
VVOID TessellationEffect::SetFogColor(const Color& pFogColor)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gFogColor");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv( lShaderLocation, 1, pFogColor );
}

/*************************************************************************************************/
VVOID TessellationEffect::SetFogStart(const FFLOAT& pFogStart)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gFogStart");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform1f(lShaderLocation, pFogStart);
}

/*************************************************************************************************/
VVOID TessellationEffect::SetFogRange(const FFLOAT& pFogRange)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gFogRange");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform1f(lShaderLocation, pFogRange);
}

/*************************************************************************************************/
VVOID TessellationEffect::SetDirLights(const OcelotParallelLight* pLights, UBIGINT pCount)
{
	if(pLights == NULL)
	{
		return;
	}

	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gLightCount");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform1i(lShaderLocation, pCount);

	for( UBIGINT lCurrLight = 0; lCurrLight < pCount; lCurrLight++ )
	{
		stringstream lAmbientShaderVarName;
		lAmbientShaderVarName << "gDirLights[" << lCurrLight << "].Ambient";
		lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), lAmbientShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->glUniform4fv(lShaderLocation, 1, pLights[lCurrLight].Ambient());

		stringstream lDiffuseShaderVarName;
		lDiffuseShaderVarName << "gDirLights[" << lCurrLight << "].Diffuse";
		lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), lDiffuseShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->glUniform4fv(lShaderLocation, 1, pLights[lCurrLight].Diffuse());

		stringstream lSpecularShaderVarName;
		lSpecularShaderVarName << "gDirLights[" << lCurrLight << "].Specular";
		lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), lSpecularShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->glUniform4fv(lShaderLocation, 1, pLights[lCurrLight].Specular());

		stringstream lDirectionShaderVarName;
		lDirectionShaderVarName << "gDirLights[" << lCurrLight << "].Direction";
		lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), lDirectionShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->glUniform3fv(lShaderLocation, 1, pLights[lCurrLight].Direction());

		GLenum lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Set Effect Light FAILURE !!! ");
		}
	}
}

/*************************************************************************************************/
VVOID TessellationEffect::SetMaterial(const Material& pMaterial)
{
	UBIGINT lShaderLocation;

	// Now, pass material object
	// Obviously opengl does not allow to pass an entire struct as DirectX does
	// so, we ll pass elements one by one. It could avoid padding issues that appear
	// in DirectX managing data by one or four. (e.g: vec3 need an extra float as padding).
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Ambient");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pMaterial.Ambient);

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Diffuse");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pMaterial.Diffuse);

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Specular");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pMaterial.Specular);

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Reflect");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pMaterial.Reflect);

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Set Material Failure !!! ");
	}
}

/***********************************************************************************************/
VVOID TessellationEffect::SetDiffuseMap(const UBIGINT& pUnit)
{
	UBIGINT lShaderLocation;

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gDiffuseMap");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform1i(lShaderLocation, pUnit );
}

/*************************************************************************************************/
VVOID TessellationEffect::MakeCurrent(EffectTechnique* pCurrent)
{
	if
		( pCurrent == NULL )
	{
		return;
	}

	pCurrent->MakeCurrent();
	this->mCurrentTechnique = pCurrent;

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Tessellation Effect Make Current FAILURE !!!");
	}
}

/*************************************************************************************************/
EffectTechnique* TessellationEffect::TessTech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cTessTechName, pStagePaths, pIncludes );
}
