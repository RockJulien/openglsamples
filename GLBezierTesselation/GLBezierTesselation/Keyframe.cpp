#include "Keyframe.h"

// Namespaces used


/**********************************************************************************/
Keyframe::Keyframe() : 
TimePos(0.0f),
Translation(0.0f, 0.0f, 0.0f),
Scale(1.0f, 1.0f, 1.0f),
RotationQuat(0.0f, 0.0f, 0.0f, 1.0f)
{

}

/**********************************************************************************/
Keyframe::~Keyframe()
{

}
