#ifndef DEF_GEOMETRYHELPER_H
#define DEF_GEOMETRYHELPER_H

// Includes
#include "MeshData.h"

// Class definition
class GeometryHelper
{
private:

	// Private Methods
	static VVOID Subdivide(MeshData& pMeshData);
	static VVOID BuildCylinderTopCap(FFLOAT pBottomRadius, FFLOAT pTopRadius, FFLOAT pHeight, UBIGINT pSliceCount, UBIGINT pStackCount, MeshData& pMeshData);
	static VVOID BuildCylinderBottomCap(FFLOAT pBottomRadius, FFLOAT pTopRadius, FFLOAT pHeight, UBIGINT pSliceCount, UBIGINT pStackCount, MeshData& pMeshData);

public:

	// Constructor
	GeometryHelper();

	// Methods
	// Creates a box centered at the origin with the given dimensions.
	static VVOID CreateBox(FFLOAT pWidth, FFLOAT pHeight, FFLOAT pDepth, MeshData& pMeshData);

	// Creates a sphere centered at the origin with the given pRadius.
	// The slices and stacks parameters control the degree of tessellation.
	static VVOID CreateSphere(FFLOAT pRadius, UBIGINT pSliceCount, UBIGINT pStackCount, MeshData& pMeshData);

	// Creates a geosphere centered at the origin with the given pRadius.  
	// The Depth controls the level of tessellation.
	static VVOID CreateGeosphere(FFLOAT pRadius, UBIGINT pNumSubdivisions, MeshData& pMeshData);

	// Creates a cylinder parallel to the y-axis, and centered about the origin.  
	// The bottom and top pRadius can vary to form various cone shapes rather than true
	// cylinders.  The slices and stacks parameters control the degree of tessellation.
	static VVOID CreateCylinder(FFLOAT pBottomRadius, FFLOAT pTopRadius, FFLOAT pHeight, UBIGINT pSliceCount, UBIGINT pStackCount, MeshData& pMeshData);

	// Creates an mxn grid in the xz-plane with m rows and n columns, centered
	// at the origin with the specified pWidth and pDepth.
	static VVOID CreateGrid(FFLOAT pWidth, FFLOAT pDepth, UBIGINT pRowCount, UBIGINT pColumnCount, MeshData& pMeshData);

	// Creates a quad covering the screen in NDC coordinates.  
	// This is useful for postprocessing effects.
	static VVOID CreateFullscreenQuad(MeshData& pMeshData);

};

#endif