#ifndef DEF_PLANE_H
#define DEF_PLANE_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   Plane.h/.cpp
/   
/  Description: This Header allows us to create planes in a 
/               3D world by respecting the equation of a plane
/               Ax + By + Cz + D = 0 where A, B and C are one
/               of the normals of the plane.
/               Useful for a good view frustrum optimisation
/               algorithm.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    17/02/2012
/*****************************************************************/

#include "Vector3.h"

namespace Ocelot
{
	class Plane
	{
	private:

		// Attributes
		float m_fA;
		float m_fB;
		float m_fC;
		float m_fD;

	public:

		// Constructors & Destructor
		Plane();
		Plane(float a, float b, float c, float d);
		~Plane();

		// Methods
		void  PlaneNormalize();
		float PlaneDotCoord(const Vector3& point);
		float PlaneDot(const Vector4& point);
		float PlaneDotNormal(const Vector3& point);
		void  PlaneRescale(Plane& out, const float& scale);
		void  PlaneCreateFrom3Points(const Vector3& point1, const Vector3& point2, const Vector3& point3);

		// Accessors
		inline float getA() const { return m_fA;}
		inline float getB() const { return m_fB;}
		inline float getC() const { return m_fC;}
		inline float getD() const { return m_fD;}
		inline void  A(float a) { m_fA = a;}
		inline void  B(float b) { m_fB = b;}
		inline void  C(float c) { m_fC = c;}
		inline void  D(float d) { m_fD = d;}
		inline Vector3 GetNormal() 
		{ 
			PlaneNormalize();
			return  Vector3(m_fA, m_fB, m_fC); 
		}

		// Operators overload
		/*********************** Cast Operators ***********************/
		operator float* ();
		operator const float* () const;
		/*********************** unary operators **********************/
		Plane  operator + () const;
		Plane  operator - () const;
		/*********************** assgm operators **********************/
		Plane& operator *= (float );
		Plane& operator /= (float );
		/*********************** binary operators *********************/
		Plane  operator * (float ) const;
		Plane  operator / (float ) const;
		bool   operator == (const Plane& ) const;
		bool   operator != (const Plane& ) const;
		friend Plane operator * (float , const Plane&);
	};
}

#endif