#ifndef DEF_UTILITY_H
#define DEF_UTILITY_H

#include <cmath>
#include <cassert>
#include <limits>
#include <stdlib.h>

#define CUBE(X) (X) * (X) * (X)
#define SQUARE(X) (X) * (X)

const float PI        = 3.141592653589793238462f;
const float PISQUARE  = SQUARE(PI);
const float PICUBE    = CUBE(PI);
const float TWOPI     = PI * 2.0f;
const float HALFPI    = PI / 2.0f;
const float QUARTERPI = PI / 4.0f;

const float COS0	  = 1.0f;
const float COSPI     = -1.0f;
const float COSTWOPI  = 1.0f;
const float COS30     = sqrtf(3.0f) / 2.0f;
const float COS45	  = sqrtf(2.0f) / 2.0f;
const float COS60	  = 0.5f;
const float COS90	  = 0.0f;

const float SIN0	  = 0.0f;
const float SINPI     = 0.0f;
const float SINTWOPI  = 0.0f;
const float SIN30	  = 0.5f;
const float SIN45	  = COS45;
const float SIN60	  = COS30;
const float SIN90	  = 1.0f;

const int     MaxInt    = (std::numeric_limits<int>::max)();
const double  MaxDouble = (std::numeric_limits<double>::max)();
const double  MinDouble = (std::numeric_limits<double>::min)();
const float   MaxFloat  = (std::numeric_limits<float>::max)();
const float   MinFloat  = (std::numeric_limits<float>::min)();
const float   INFINITAS  = (std::numeric_limits<float>::infinity)(); // infinity in latin

template<class T>
inline bool Equal(T a, T b)
{
	if(a == b) 
		return true;
	return false;
}

template<class F>
inline bool Different(F a, F b)
{
	if(a != b) 
		return true;
	return false;
}

inline int AutoRoundF(float val)
{
	int integral   = (int)val;
	float mantissa = val - integral;

	if(mantissa < 0.5f) return integral;

	return integral + 1;
}

inline int AutoRoundD(double val)
{
	int integral   = (int)val;
	double mantissa = val - integral;

	if(mantissa < 0.5f) return integral;

	return integral + 1;
}

template<class T, class K, class Q>
inline void Clamp(const T& min, K& toFix, const Q& max)
{
	if((float)min > (float)max) return;

	if(toFix > (K)max)
		toFix = (K)max;

	if(toFix < (K)min)
		toFix = (K)min;
}

template<class T>
inline T Higher(const T& val1, const T& val2)
{
	if(val1 > val2)
		return val1;
	return val2;
}

template<class T>
inline T Higher3Val(const T& val1, const T& val2, const T& val3)
{
	return Higher<T>(Higher<T>(val1, val2), val3);
}

template<class Z>
inline Z Smaller(const Z& val1, const Z& val2)
{
	if(val1 > val2)
		return val2;
	return val1;
}

inline float RandFloat()
{
	return (float)(((float)rand())/(float)RAND_MAX);
}

// give a random float number between val1 & val2.
inline float RandBetween(float val1, float val2)
{
	return val1 + RandFloat() * ( val2 - val1 );
}

// give a random float number between -1.0f & 1.0f.
inline float RandUnity()
{
	return ((float)((rand()%20000) - 10000) / 10000.0f);
}

// return a value between -1 and 1;
inline float ClampedRand()
{
	return RandFloat() - RandFloat();
}

inline bool RandBool()
{
	if(RandFloat() > 0.5f) 
		return true;
	return false;
}

inline float DegToRad(float deg)
{
	return TWOPI * (deg / 360.0f);
}

inline float RadToDeg(float rad)
{
	return (rad * 360.0f) / TWOPI;
}

inline float Cotangent(float radian)
{
	return 1.0f / tan(radian);
}

inline float Cosecant(float radian)
{
	return 1.0f / sin(radian);
}

inline float Secant(float radian)
{
	return 1.0f / cos(radian);
}

inline float AngleFromXY(float x, float y)
{
	float theta = 0.0f;
 
	// Quadrant I or IV
	if(x >= 0.0f) 
	{
		// If x = 0, then atanf(y/x) = +pi/2 if y > 0
		//                atanf(y/x) = -pi/2 if y < 0
		theta = atanf(y / x); // in [-pi/2, +pi/2]

		if(theta < 0.0f)
		{
			theta += 2.0f * PI; // in [0, 2*pi).
		}
	}
	// Quadrant II or III
	else
	{
		theta = atanf(y / x) + PI; // in [0, 2*pi).
	}

	return theta;
}

inline float absf(float f)
{
	if(f < 0.0f)
	{
		return -f;
	}
	else
	{
		return f;
	}
}

#endif
