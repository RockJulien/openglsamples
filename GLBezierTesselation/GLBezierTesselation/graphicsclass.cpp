////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "graphicsclass.h"
#include "TGALoader.h"
#include <fstream>
#include "MathUtilities.h"
#include <time.h>
#include "systemclass.h"
#include "MeshData.h"

using namespace std;
using namespace Ocelot;

/*********************************************************************************************/
GraphicsClass::GraphicsClass() :
mOpenGL(NULL), mCamera(NULL), mController(NULL), 
mQuadPatchVAO(0), mQuadPatchVB(0)
{
	srand((UBIGINT)time(NULL));

	// Initialize the camera.
	OcelotCameraInfo lCamInfo( Vector3( 0.0f, 2.0f, -15.0f ), 
							   Vector3( 0.0f, 0.0f, 1.0f ), 
							   (FFLOAT)(800 / 600), 
							   1000.0f );
	lCamInfo.SetNear(1.0f);
	this->mCamera = new OcelotCamera( lCamInfo );

	// And create the controller
	this->mController = ControlFact->createController( this->mCamera, 
													   CAMERA_CONTROLLER_FLYER );
}

/*********************************************************************************************/
GraphicsClass::~GraphicsClass()
{
	
}

/*********************************************************************************************/
BBOOL GraphicsClass::Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight)
{
	BBOOL lResult = false;

	// Store a pointer to the OpenGL class object.
	this->mOpenGL = pGDevice;
	this->mClientWidth  = pClientWidth;
	this->mClientHeight = pClientHeight;
	
	Effects::InitializeAll( this->mOpenGL );
	InputLayouts::InitializeAll( this->mOpenGL );

	vector<string> lIncludes;
	StageDescription lDescription;
	lDescription.SetVertexStagePath( "Shaders/BezierTesselation.vs" );
	lDescription.SetHullStagePath( "Shaders/BezierTesselation.tcs" );
	lDescription.SetDomainStagePath( "Shaders/BezierTesselation.tes" );
	lDescription.SetPixelStagePath( "Shaders/BezierTesselation.ps" );
	this->mTessTech = Effects::TessellationFX()->TessTech( lDescription, lIncludes );
	this->mTessTech->SetLayout( InputLayouts::PosLayout() );

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Tesselation technique get FAILURE !!! ");
	}

	this->BuildQuadPatchBuffer();

	lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Build Quad patch FAILURE !!! ");
	}

	return true;
}

/*********************************************************************************************/
VVOID GraphicsClass::OnResize(BIGINT pClientWidth, BIGINT pClientHeight)
{
	this->mClientWidth  = pClientWidth;
	this->mClientHeight = pClientHeight;

	FFLOAT lAspect = (FFLOAT)pClientWidth / pClientHeight;

	// refresh the aspect of the camera if the
	// window is resized.
	OcelotCameraInfo lInfo = this->mCamera->Info();
	if
		( this->mCamera != NULL )
	{
		lInfo.SetAspect( lAspect );  // aspect ratio
		this->mCamera->SetInfo( lInfo );
	}

	glViewport( 0, 0, pClientWidth, pClientHeight );
}

/*********************************************************************************************/
VVOID GraphicsClass::Shutdown()
{
	DeletePointer( this->mCamera );
	DeletePointer( this->mController );
	DeletePointer( this->mTessTech );

	// Release VAOs, VBOs and IBOs
	// Release Land buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mQuadPatchVB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mQuadPatchVAO );

	Effects::DestroyAll();
	InputLayouts::DestroyAll();
}

/*********************************************************************************************/
BBOOL GraphicsClass::Update(FFLOAT pDeltaTime)
{
	// Update the camera.
	this->mController->update( pDeltaTime );

	return true;
}

/*********************************************************************************************/
BBOOL GraphicsClass::Render()
{
	// Clear the buffers to begin the scene.
	this->mOpenGL->StartRender( true, true );

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Start rendering FAILURE !!! ");
	}

	glClearColor( 0.75f, 0.75f, 0.75f, 1.0f ); // Silver
	glClearDepth( 1.0f );
	glClearStencil( 0 );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

	Matrix4x4 lView     = this->mCamera->View();
	Matrix4x4 lProj     = this->mCamera->Proj();
	Matrix4x4 lViewProj = lView * lProj;

	OcelotCameraInfo lInfo = this->mCamera->Info();

	if
		( Effects::TessellationFX() != NULL )
	{
		Effects::TessellationFX()->MakeCurrent( this->mTessTech );

		Matrix4x4 lWorld; // Identity
		Matrix4x4 lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		Matrix4x4 lWorldViewProj = lWorld * lViewProj;

		Effects::TessellationFX()->SetWorldViewProj( lWorldViewProj );

		// Wireframe mode.
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

		// Make current the patch VAO.
		this->mOpenGL->glBindVertArray( this->mQuadPatchVAO );

		// Draw the quad and tesselate it.
		this->mOpenGL->glPatchParameteri( GL_PATCH_VERTICES, 16 );
		glDrawArrays( GL_PATCHES, 0, 16 );

		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
	}

	// Present the rendered scene to the screen.
	this->mOpenGL->StopRender();

	return true;
}

/*********************************************************************************************/
VVOID GraphicsClass::BuildQuadPatchBuffer()
{
	DomeVertex lVertices[16] = 
	{
		// Row 0
		DomeVertex( -10.0f, -10.0f, +15.0f ),
		DomeVertex( -5.0f,  0.0f, +15.0f ),
		DomeVertex( +5.0f,  0.0f, +15.0f ),
		DomeVertex( +10.0f, 0.0f, +15.0f ), 

		// Row 1
		DomeVertex( -15.0f, 0.0f, +5.0f ),
		DomeVertex( -5.0f,  0.0f, +5.0f ),
		DomeVertex( +5.0f,  20.0f, +5.0f ),
		DomeVertex( +15.0f, 0.0f, +5.0f ), 

		// Row 2
		DomeVertex( -15.0f, 0.0f, -5.0f ),
		DomeVertex( -5.0f,  0.0f, -5.0f ),
		DomeVertex( +5.0f,  0.0f, -5.0f ),
		DomeVertex( +15.0f, 0.0f, -5.0f ), 

		// Row 3
		DomeVertex( -10.0f, 10.0f, -15.0f ),
		DomeVertex( -5.0f,  0.0f, -15.0f ),
		DomeVertex( +5.0f,  0.0f, -15.0f ),
		DomeVertex( +25.0f, 10.0f, -15.0f )
	};

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mQuadPatchVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mQuadPatchVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mQuadPatchVB );

	// Bind the vertex buffer and load the vertex (position, texture, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mQuadPatchVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, 16 * sizeof(DomeVertex), lVertices, GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::PosLayout()->SetVertexLayout( this->mQuadPatchVB );
}

/*********************************************************************************************/
IOcelotCameraController* GraphicsClass::Controller()
{
	return this->mController;
}
