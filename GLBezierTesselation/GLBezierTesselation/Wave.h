#ifndef DEF_WAVE_H
#define DEF_WAVE_H

// Includes
#include "OSCheck.h"
#include "Maths\Vector3.h"

// Class definition
class Wave
{
private:

	// Attributes
	Vector3* mPrevSolution;
	Vector3* mCurrSolution;
	Vector3* mNormals;
	Vector3* mTangentX;

	UBIGINT  mNumRows;
	UBIGINT  mNumCols;

	UBIGINT  mVertexCount;
	UBIGINT  mTriangleCount;

	// Simulation constants we can precompute.
	FFLOAT   mK1;
	FFLOAT   mK2;
	FFLOAT   mK3;

	FFLOAT   mTimeStep;
	FFLOAT   mSpatialStep;

public:

	// Constructor & Destructor
	Wave();
	~Wave();

	// Methods
	VVOID Init(UBIGINT pRowCount, UBIGINT pColCount, FFLOAT pDx, FFLOAT pDeltaTime, FFLOAT pSpeed, FFLOAT pDamping);
	VVOID Update(FFLOAT pDeltaTime);
	VVOID Disturb(UBIGINT pI, UBIGINT pJ, FFLOAT pMagnitude);

	// Operators
	// Returns the solution at the ith grid point.
	const Vector3& operator[] (BIGINT pIndex) const;

	// Accessors
	// Returns the solution normal at the ith grid point.
	const Vector3& Normal(BIGINT pIndex) const;
	// Returns the unit tangent vector at the ith grid point in the local x-axis direction.
	const Vector3& TangentX(BIGINT pIndex) const;
	UBIGINT RowCount() const;
	UBIGINT ColumnCount() const;
	UBIGINT VertexCount() const;
	UBIGINT TriangleCount() const;
	FFLOAT  Width() const;
	FFLOAT  Depth() const;

};

#endif
