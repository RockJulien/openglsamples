#include "OcelotOrbiterController.h"

using namespace Ocelot;

OcelotOrbiterController::OcelotOrbiterController() :
IOcelotCameraController(), m_instance(NULL), m_vRight(1.0f, 0.0f, 0.0f)
{
	
}

OcelotOrbiterController::OcelotOrbiterController(OcelotCamera* camera) :
IOcelotCameraController(), m_instance(camera), m_vRight(1.0f, 0.0f, 0.0f)
{
	
}

OcelotOrbiterController::~OcelotOrbiterController()
{

}

VVOID OcelotOrbiterController::update(FFLOAT deltaTime)
{
	// update the camera.
	m_instance->update(deltaTime);

	// apply movements
	applyMovements();
}

VVOID OcelotOrbiterController::move(Direction dir)
{
	// Nothing to do.
}

VVOID OcelotOrbiterController::applyMovements()
{
	// Take back useful camera vectors.
	OcelotCameraInfo tempInfo = m_instance->Info();

	Vector3 eye     = tempInfo.Eye();
	Vector3 lookAt  = tempInfo.LookAt();
	Vector3 up      = tempInfo.Up();
	MatrixView view = m_instance->View();

	// keep axes orthogonal to each other with a unit length as well
	lookAt.Vec3Normalise();
	up = lookAt.Vec3CrossProduct(m_vRight);
	up.Vec3Normalise();
	m_vRight  = up.Vec3CrossProduct(lookAt);
	m_vRight.Vec3Normalise();

	// reset the view
	FFLOAT posX = -eye.Vec3DotProduct(m_vRight);
	FFLOAT posY = -eye.Vec3DotProduct(up);
	FFLOAT posZ = -eye.Vec3DotProduct(lookAt);

	view.set_11(m_vRight.getX());
	view.set_21(m_vRight.getY());
	view.set_31(m_vRight.getZ());
	view.set_41(posX);
	view.set_12(up.getX());
	view.set_22(up.getY());
	view.set_32(up.getZ());
	view.set_42(posY);
	view.set_13(lookAt.getX());
	view.set_23(lookAt.getY());
	view.set_33(lookAt.getZ());
	view.set_43(posZ);
	view.set_14(0.0f);
	view.set_24(0.0f);
	view.set_34(0.0f);
	view.set_44(1.0f);

	// reset lookat and up vectors in camera which changed as seen above.
	tempInfo.SetLookAt(lookAt);
	tempInfo.SetUp(up);
	m_instance->SetInfo(tempInfo);

	// as well as the view.
	m_instance->SetView(view);
}

VVOID OcelotOrbiterController::pitch(FFLOAT angle)
{
	Matrix4x4 rotat;
	rotat.Matrix4x4RotationAxis(rotat, m_vRight, -angle);

	// grab useful attributes.
	OcelotCameraInfo tempInfo = m_instance->Info();

	Vector3 up     = tempInfo.Up();
	Vector3 lookAt = tempInfo.LookAt();

	// compute rotation.
	up     = toVec3(up.Vec3VecMatrixTransformNormal(rotat));
	lookAt = toVec3(lookAt.Vec3VecMatrixTransformNormal(rotat));

	// reset changed attributes.
	tempInfo.SetUp(up);
	tempInfo.SetLookAt(lookAt);
	m_instance->SetInfo(tempInfo);
}

VVOID OcelotOrbiterController::yaw(FFLOAT angle)
{
	Matrix4x4 rotat;
	rotat.Matrix4x4Identity();

	rotat.Matrix4x4RotationY(rotat, angle);

	// grab useful attributes.
	OcelotCameraInfo tempInfo = m_instance->Info();

	Vector3 up     = tempInfo.Up();
	Vector3 lookAt = tempInfo.LookAt();

	// compute rotation.
	m_vRight = toVec3(m_vRight.Vec3VecMatrixTransformNormal(rotat));
	up       = toVec3(up.Vec3VecMatrixTransformNormal(rotat));
	lookAt   = toVec3(lookAt.Vec3VecMatrixTransformNormal(rotat));

	// reset changed attributes.
	tempInfo.SetUp(up);
	tempInfo.SetLookAt(lookAt);
	m_instance->SetInfo(tempInfo);
}

VVOID OcelotOrbiterController::roll(FFLOAT angle)
{
	// only for flying simulator camera.
	// very special.
}

Vector3 OcelotOrbiterController::toVec3(const Vector4& toCast)
{
	return Vector3(toCast.getX(), toCast.getY(), toCast.getZ());
}
