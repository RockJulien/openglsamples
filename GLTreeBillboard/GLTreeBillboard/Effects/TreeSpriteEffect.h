#ifndef DEF_TREESPRITEEFFECT_H
#define DEF_TREESPRITEEFFECT_H

// Includes
#include <map>
#include <string>
#include "..\GDevice.h"
#include "..\Material.h"
#include "..\Maths\Color.h"
#include "..\Maths\Matrix4x4.h"
#include "..\OcelotLight.h"
#include "EffectTechnique.h"

// Namespaces


// Class definition
class TreeSpriteEffect
{
private:

	std::map<std::string, USMALLINT> mFlagMap;

	// Attributes
	EffectTechnique* mCurrentTechnique;
	GDevice*		 mDevice;

public:

	// Constructor & Destructor
	TreeSpriteEffect(GDevice* pDevice);
	~TreeSpriteEffect();

	// Methods
	VVOID SetViewProj(const Matrix4x4& pMatrix);
	VVOID SetEyePosW(const Vector3& pEyePos);
	VVOID SetFogColor(const Ocelot::Color& pFogColor);
	VVOID SetFogStart(const FFLOAT& pFogStart);
	VVOID SetFogRange(const FFLOAT& pFogRange);
	VVOID SetTexTransform(const Matrix4x4& pMatrix);
	VVOID SetDirLights(const Ocelot::OcelotParallelLight* pLights, UBIGINT pCount);
	VVOID SetMaterial(const Material& pMaterial);
	VVOID SetTreeTextureMapArray(const UBIGINT& pTexArray);

	VVOID MakeCurrent(EffectTechnique* pCurrent);

	// Technique getters
	EffectTechnique* Light3Tech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
	EffectTechnique* Light3TexAlphaClipTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
	EffectTechnique* Light3TexAlphaClipFogTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);

};

#endif