#ifndef DEF_INPUTLAYOUTS_H
#define DEF_INPUTLAYOUTS_H

// Includes
#include "InputLayout.h"

// Namespaces

// Class definition
class InputLayouts
{
private:

	// Attributes
	static InputLayout*	sPosNormalTexLayout;
	static InputLayout* sTreePointSpriteLayout;

public:

	// Constructor
	static VVOID InitializeAll(GDevice*	pDevice);
	static VVOID DestroyAll();

	// Layout handles
	static InputLayout* PosNormalTexLayout();
	static InputLayout* TreePointSpriteLayout();

};

#endif