#include "Effects.h"

// Namespaces used

// Static initialization
BasicEffect*      Effects::sBasicFX = NULL;
TreeSpriteEffect* Effects::sTreeSpriteFX = NULL;

/*****************************************************************************************************/
VVOID Effects::InitializeAll(GDevice* pDevice)
{
	if
		( Effects::sBasicFX == NULL )
	{
		Effects::sBasicFX = new BasicEffect( pDevice );
	}

	if
		( Effects::sTreeSpriteFX == NULL )
	{
		Effects::sTreeSpriteFX = new TreeSpriteEffect( pDevice );
	}
}

/*****************************************************************************************************/
VVOID Effects::DestroyAll()
{
	DeletePointer( Effects::sBasicFX );
	DeletePointer( Effects::sTreeSpriteFX );
}

/*****************************************************************************************************/
BasicEffect*      Effects::BasicFX()
{
	return Effects::sBasicFX;
}

/*****************************************************************************************************/
TreeSpriteEffect* Effects::TreeSpriteFX()
{
	return Effects::sTreeSpriteFX;
}

