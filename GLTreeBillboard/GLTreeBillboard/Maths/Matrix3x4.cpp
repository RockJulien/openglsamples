#include "Matrix3x4.h"

using namespace Ocelot;

Matrix3x4::Matrix3x4()
{
	m_f11 = 0.0f;
	m_f12 = 0.0f;
	m_f13 = 0.0f;
	m_f14 = 0.0f;
	m_f21 = 0.0f;
	m_f22 = 0.0f;
	m_f23 = 0.0f;
	m_f24 = 0.0f;
	m_f31 = 0.0f;
	m_f32 = 0.0f;
	m_f33 = 0.0f;
	m_f34 = 0.0f;
}

Matrix3x4::Matrix3x4(const Matrix3x4& m)
{
	*this = m;
}

Matrix3x4::Matrix3x4(const float _11, const float _12, const float _13, const float _14,
	const float _21, const float _22, const float _23, const float _24,
	const float _31, const float _32, const float _33, const float _34)
{
	m_f11 = _11;
	m_f12 = _12;
	m_f13 = _13;
	m_f14 = _14;
	m_f21 = _21;
	m_f22 = _22;
	m_f23 = _23;
	m_f24 = _24;
	m_f31 = _31;
	m_f32 = _32;
	m_f33 = _33;
	m_f34 = _34;
}

Vector3 Matrix3x4::Transform(const Vector3 &v) const
{
	return Vector3(
		v.getX() * m_f11 + v.getY() * m_f12 + v.getZ() * m_f13 + m_f14,
		v.getX() * m_f21 + v.getY() * m_f22 + v.getZ() * m_f23 + m_f24,
		v.getX() * m_f31 + v.getY() * m_f32 + v.getZ() * m_f33 + m_f34);
}

void Matrix3x4::Matrix3x4Multiply(Matrix3x4 &out, const Matrix3x4 &mat)
{
	out.set11(mat.get11() * m_f11 + mat.get21() * m_f12 + mat.get31() * m_f13);
	out.set12(mat.get12() * m_f11 + mat.get22() * m_f12 + mat.get32() * m_f13);
	out.set13(mat.get13() * m_f11 + mat.get23() * m_f12 + mat.get33() * m_f13);
	out.set14(mat.get14() * m_f11 + mat.get24() * m_f12 + mat.get34() * m_f13 + m_f14);
	out.set21(mat.get11() * m_f21 + mat.get21() * m_f22 + mat.get31() * m_f23);
	out.set22(mat.get12() * m_f21 + mat.get22() * m_f22 + mat.get32() * m_f23);
	out.set23(mat.get13() * m_f21 + mat.get23() * m_f22 + mat.get33() * m_f23);
	out.set24(mat.get14() * m_f21 + mat.get24() * m_f22 + mat.get34() * m_f23 + m_f24);
	out.set31(mat.get11() * m_f31 + mat.get21() * m_f32 + mat.get31() * m_f33);
	out.set32(mat.get12() * m_f31 + mat.get22() * m_f32 + mat.get32() * m_f33);
	out.set33(mat.get13() * m_f31 + mat.get23() * m_f32 + mat.get33() * m_f33);
	out.set34(mat.get14() * m_f31 + mat.get24() * m_f32 + mat.get34() * m_f33 + m_f34);
}

float Matrix3x4::GetDeterminant() const
{
	return m_f31 * m_f22 * m_f13 + m_f21 * m_f32 * m_f13 + m_f31 * m_f12 * m_f23 - m_f11 * m_f32 * m_f23 - m_f21 * m_f12 * m_f33 + m_f11 * m_f22 * m_f33;
}

void Matrix3x4::Matrix3x4Inverse(Matrix3x4 &out)
{
	float det = GetDeterminant();
	if(abs(det) < FLT_EPSILON)
	{
		return;
	}
	det = 1.0f / det;

	out.m_f11 = (-m_f32 * m_f23 + m_f22 * m_f33) * det;
	out.m_f21 = (m_f31 * m_f23 - m_f21 * m_f33) * det;
	out.m_f31 = (-m_f31 * m_f22 + m_f21 * m_f32) * det;

	out.m_f12 = (m_f32 * m_f13 - m_f12 * m_f33) * det;
	out.m_f22 = (-m_f31 * m_f13 + m_f11 * m_f33) * det;
	out.m_f32 = (m_f31 * m_f12 - m_f11 * m_f32) * det;

	out.m_f13 = (-m_f22 * m_f13 + m_f12 * m_f23) * det;
	out.m_f23 = (m_f21 * m_f13 - m_f11 * m_f23) * det;
	out.m_f33 = (-m_f21 * m_f12 + m_f11 * m_f22) * det;

	out.m_f14 = (m_f32 * m_f23 * m_f14 - m_f22 * m_f33 * m_f14 - m_f32 * m_f13 * m_f24
				+m_f12 * m_f33 * m_f24 + m_f22 * m_f13 * m_f34 - m_f12 * m_f23 * m_f34) * det;

	out.m_f24 = (-m_f31 * m_f23 * m_f14 + m_f21 * m_f33 * m_f14 + m_f31 * m_f13 * m_f24
				-m_f11 * m_f33 * m_f24 - m_f21 * m_f13 * m_f34 + m_f11 * m_f23 * m_f34) * det;
	out.m_f34 =(m_f31 * m_f22 * m_f14 - m_f21 * m_f32 * m_f14 - m_f31 * m_f12 * m_f24
			   +m_f11 * m_f32 * m_f24 + m_f21 * m_f12 * m_f34 - m_f11 * m_f22 * m_f34) * det;
}

void Matrix3x4::SetOrientationAndPosition(Quaternion &q, const Vector3 &position)
{
	Matrix3x3 orient = q.QuaternionToMatrix3Normalized();

	m_f11 = orient.get_11();
	m_f12 = orient.get_12();
	m_f13 = orient.get_13();
	m_f14 = position.getX();

	m_f21 = orient.get_21();
	m_f22 = orient.get_22();
	m_f23 = orient.get_23();
	m_f24 = position.getY();

	m_f31 = orient.get_31();
	m_f32 = orient.get_32();
	m_f33 = orient.get_33();
	m_f34 = position.getZ();
}


Vector3 Matrix3x4::TransformInverse(const Vector3 &v) const
{
	Vector3 temp = v;
	temp.x(temp.getX() - m_f14);
	temp.y(temp.getY() - m_f24);
	temp.z(temp.getZ() - m_f34);

	return Vector3(temp.getX() * m_f11 + temp.getY() * m_f21 + temp.getZ() * m_f31,
				   temp.getX() * m_f12 + temp.getY() * m_f22 + temp.getZ() * m_f32,
				   temp.getX() * m_f13 + temp.getY() * m_f23 + temp.getZ() * m_f33);
}

Vector3 Matrix3x4::TransformDirection(const Vector3 &v) const
{
	return Vector3(v.getX() * m_f11 + v.getY() * m_f12 + v.getZ() * m_f13,
		           v.getX() * m_f21 + v.getY() * m_f22 + v.getZ() * m_f23,
				   v.getX() * m_f31 + v.getY() * m_f32 + v.getZ() * m_f33);
}
/*
0 1 2  3
4 5 6  7
8 9 10 11

11 12 13 14
21 22 23 24
31 32 33 34
*/

Vector3 Matrix3x4::GetAxisVector(int i) const
{
	switch(i)
	{
	case 0:
		return Vector3(m_f11, m_f21, m_f31); 
	case 1:
		return Vector3(m_f12, m_f22, m_f32); 
	case 2:
		return Vector3(m_f13, m_f23, m_f33); 
	case 3:
		return Vector3(m_f14, m_f24, m_f34); 
	default:
		return Vector3(0.0f);
	}
}

Vector3 Matrix3x4::TransformDirectionInverse(const Vector3 &v) const
{
	return Vector3(v.getX() * m_f11 + v.getY() * m_f21 + v.getZ() * m_f31,
		           v.getX() * m_f12 + v.getY() * m_f22 + v.getZ() * m_f32,
				   v.getX() * m_f13 + v.getY() * m_f23 + v.getZ() * m_f33);
}