#ifndef DEF_BOUNDINGSPHERE_H
#define DEF_BOUNDINGSPHERE_H

// Includes
#include "OSCheck.h"
#include "Maths\Vector3.h"

// Class definition
class BoundingSphere
{
private:

	// Attributes
	Vector3 mCenter;
	FFLOAT  mRadius;

public:

	// Constructor
	BoundingSphere();
	~BoundingSphere();

	// Accessors
	Vector3 Center() const;
	FFLOAT  Radius() const;
	VVOID   SetCenter(const Vector3& pCenter);
	VVOID   SetRadius(const FFLOAT& pRadius);

};

#endif