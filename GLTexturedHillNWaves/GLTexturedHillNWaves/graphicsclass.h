////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _GRAPHICSCLASS_H_
#define _GRAPHICSCLASS_H_

///////////////////////
// INCLUDES //
///////////////////////
#include "GDevice.h"
#include "Camera\OcelotCamera.h"
#include "Camera\OcelotCameraFactory.h"
#include "GeometryHelper.h"
#include "OcelotLight.h"
#include "Material.h"
#include "Wave.h"
#include "Effects\Effects.h"
#include "Effects\InputLayouts.h"
#include "TextureManager\TextureManager.h"

using namespace Ocelot;

/////////////
// GLOBALS //
/////////////
const BBOOL  FULL_SCREEN   = false;
const BBOOL  VSYNC_ENABLED = true;
const FFLOAT SCREEN_DEPTH  = 1.0f;
const FFLOAT SCREEN_NEAR   = 0.1f;

////////////////////////////////////////////////////////////////////////////////
// Class name: GraphicsClass
////////////////////////////////////////////////////////////////////////////////
class GraphicsClass
{
public:

	// Constructor & Destructor
	GraphicsClass();
	~GraphicsClass();

	// Methods
	BBOOL Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight);
	VVOID OnResize(BIGINT pClientWidth, BIGINT pClientHeight);
	VVOID Shutdown();
	BBOOL Update(FFLOAT pDeltaTime);
	BBOOL Render();

	// Accessors
	IOcelotCameraController* Controller();

private:

	// Forbidden
	GraphicsClass(const GraphicsClass&);

	// Private Methods
	FFLOAT  GetHillHeight( FFLOAT pX, FFLOAT pZ ) const;
	Vector3 GetHillNormal( FFLOAT pX, FFLOAT pZ ) const;
	VVOID   BuildLandGeometryBuffers();
	VVOID   BuildWaveGeometryBuffers();

private:

	// Attributes
	// Graphic device
	OpenGLClass*             mOpenGL;

	// Camera
	OcelotCamera*            mCamera;
	IOcelotCameraController* mController;
	
	// Buffers
	UBIGINT					 mLandVAO;
	UBIGINT					 mLandVB;
	UBIGINT					 mLandIB;
	UBIGINT					 mWaveVAO;
	UBIGINT					 mWaveVB;
	UBIGINT					 mWaveIB;

	// Wave
	Wave					 mWaves;

	// Lights
	Ocelot::OcelotParallelLight mDirLights[3];
	Material				    mLandMat;
	Material				    mWaveMat;

	// Transforms
	// Define transformations from local spaces to world space.
	Matrix4x4		 mLandWorld;
	Matrix4x4		 mWaveWorld;
	Matrix4x4		 mLandTexTransform;
	Matrix4x4		 mWaveTexTransform;

	// Index and Offset Count
	UBIGINT			 mLandIndexCount;
	Vector2			 mWaterTexOffset;

	// Texture Manager
	TextureManager   mTexManager;
	UBIGINT			 mLandMapRV;
	UBIGINT			 mWaveMapRV;

	UBIGINT			 mDiffuseTexUnit;

	// Effects
	EffectTechnique* mLight3TexTech;
};

#endif