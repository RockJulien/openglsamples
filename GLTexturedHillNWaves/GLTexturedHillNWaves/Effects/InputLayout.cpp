#include "InputLayout.h"

// Namespaces used
using namespace std;

/***************************************************************************************************/
InputLayout::InputLayout(GDevice* pDevice) :
mLayout(), mDevice(pDevice)
{

}

/***************************************************************************************************/
InputLayout::~InputLayout()
{
	this->mLayout.clear();
}

/***************************************************************************************************/
VVOID InputLayout::AddElement(const InputElementDesc& pDesc)
{
	this->mLayout.push_back(pDesc);
}

/***************************************************************************************************/
VVOID InputLayout::SetVertexLayout(const UBIGINT& pVBO_Id)
{
	// Specify the layout to the shader for allowing it
	// to know the vertex details.
	for(UBIGINT lCurrElem = 0; lCurrElem < (UBIGINT)this->mLayout.size(); lCurrElem++)
	{
		InputElementDesc lDesc = this->mLayout[lCurrElem];
		// Enable the three vertex array attributes.
		this->mDevice->glEnableVertAttrArrays( lDesc.SemanticIndex() );
		this->mDevice->glBindBuffer( GL_ARRAY_BUFFER, pVBO_Id );
		this->mDevice->glVertAttrPointer( lDesc.SemanticIndex(), 
										  lDesc.DataCount(), 
										  lDesc.DataType(), 
										  lDesc.NeedNormalization(), 
										  lDesc.Stride(), 
										  lDesc.Offset() );

		GLenum lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Vertex attribute pointer failure !!! ");
		}
	}
}

/***************************************************************************************************/
VVOID InputLayout::BindVertexLayout(const UBIGINT& pShaderId)
{
	for(UBIGINT lCurrElem = 0; lCurrElem < (UBIGINT)this->mLayout.size(); lCurrElem++)
	{
		InputElementDesc lDesc = this->mLayout[lCurrElem];
		// Bind shader variables by grabbing their location in the
		// shader code. (only the one as input for the vertex stage).
		this->mDevice->glBindAttrLocation( pShaderId, 
										   lDesc.SemanticIndex(), 
										   lDesc.SemanticName().c_str() );

		GLenum lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Binding attribute location failure !!! ");
		}
	}
}

/***************************************************************************************************/
const std::vector<InputElementDesc>& InputLayout::Layout() const
{
	return this->mLayout;
}
