#include "Constants.h"

// Namespaces used
using namespace std;

/************************************************************************************************/
const string Constants::cLight1TechName = "Light1Tech";
const string Constants::cLight2TechName = "Light2Tech";
const string Constants::cLight3TechName = "Light3Tech";

const string Constants::cLight0TexTechName = "Light0TexTech";
const string Constants::cLight1TexTechName = "Light1TexTech";
const string Constants::cLight2TexTechName = "Light2TexTech";
const string Constants::cLight3TexTechName = "Light3TexTech";