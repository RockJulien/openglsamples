#include "Effects.h"

// Namespaces used

// Static initialization
BasicEffect* Effects::sBasicFX = NULL;

/*****************************************************************************************************/
VVOID Effects::InitializeAll(GDevice* pDevice)
{
	if
		( Effects::sBasicFX == NULL )
	{
		Effects::sBasicFX = new BasicEffect( pDevice );
	}
}

/*****************************************************************************************************/
VVOID Effects::DestroyAll()
{
	DeletePointer( Effects::sBasicFX );
}

/*****************************************************************************************************/
BasicEffect* Effects::BasicFX()
{
	return Effects::sBasicFX;
}
