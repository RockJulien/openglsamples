#include "BasicEffect.h"
#include "Constants.h"
#include <sstream>

// Namespaces used
using namespace std;
using namespace Ocelot;

/*************************************************************************************************/
BasicEffect::BasicEffect(GDevice* pDevice) :
mCurrentTechnique(NULL), mDevice(pDevice)
{
	// Init the flags map.
	this->mFlagMap.insert(pair<string, BBOOL>(Constants::cLight1TechName, false));
	this->mFlagMap.insert(pair<string, BBOOL>(Constants::cLight2TechName, false));
	this->mFlagMap.insert(pair<string, BBOOL>(Constants::cLight3TechName, false));

	this->mFlagMap.insert(pair<string, BBOOL>(Constants::cLight0TexTechName, true));
	this->mFlagMap.insert(pair<string, BBOOL>(Constants::cLight1TexTechName, true));
	this->mFlagMap.insert(pair<string, BBOOL>(Constants::cLight2TexTechName, true));
	this->mFlagMap.insert(pair<string, BBOOL>(Constants::cLight3TexTechName, true));

}

/*************************************************************************************************/
BasicEffect::~BasicEffect()
{

}

/*************************************************************************************************/
VVOID BasicEffect::SetWorldViewProj(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gWorldViewProj");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/*************************************************************************************************/
VVOID BasicEffect::SetWorld(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gWorld");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/*************************************************************************************************/
VVOID BasicEffect::SetWorldInvTranspose(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gWorldInvTranspose");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/***********************************************************************************************/
VVOID BasicEffect::SetTexTransform(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gTexTransform");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/*************************************************************************************************/
VVOID BasicEffect::SetEyePosW(const Vector3& pEyePos)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gEyePosW");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform3fv( lShaderLocation, 1, pEyePos );
}

/*************************************************************************************************/
VVOID BasicEffect::SetDirLights(const OcelotParallelLight* pLights, UBIGINT pCount)
{
	if(pLights == NULL)
	{
		return;
	}

	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gLightCount");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform1i(lShaderLocation, pCount);

	for( UBIGINT lCurrLight = 0; lCurrLight < pCount; lCurrLight++ )
	{
		stringstream lAmbientShaderVarName;
		lAmbientShaderVarName << "gDirLights[" << lCurrLight << "].Ambient";
		lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), lAmbientShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->glUniform4fv(lShaderLocation, 1, pLights[lCurrLight].Ambient());

		stringstream lDiffuseShaderVarName;
		lDiffuseShaderVarName << "gDirLights[" << lCurrLight << "].Diffuse";
		lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), lDiffuseShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->glUniform4fv(lShaderLocation, 1, pLights[lCurrLight].Diffuse());

		stringstream lSpecularShaderVarName;
		lSpecularShaderVarName << "gDirLights[" << lCurrLight << "].Specular";
		lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), lSpecularShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->glUniform4fv(lShaderLocation, 1, pLights[lCurrLight].Specular());

		stringstream lDirectionShaderVarName;
		lDirectionShaderVarName << "gDirLights[" << lCurrLight << "].Direction";
		lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), lDirectionShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->glUniform3fv(lShaderLocation, 1, pLights[lCurrLight].Direction());

		GLenum lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Set Effect Light FAILURE !!! ");
		}
	}
}

/*************************************************************************************************/
VVOID BasicEffect::SetMaterial(const Material& pMaterial)
{
	UBIGINT lShaderLocation;

	// Now, pass material object
	// Obviously opengl does not allow to pass an entire struct as DirectX does
	// so, we ll pass elements one by one. It could avoid padding issues that appear
	// in DirectX managing data by one or four. (e.g: vec3 need an extra float as padding).
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Ambient");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pMaterial.Ambient);

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Diffuse");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pMaterial.Diffuse);

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Specular");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pMaterial.Specular);

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Reflect");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pMaterial.Reflect);

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Set Material Failure !!! ");
	}
}

/***********************************************************************************************/
VVOID BasicEffect::SetDiffuseMap(const UBIGINT& pUnit)
{
	UBIGINT lShaderLocation;

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gDiffuseMap");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform1i(lShaderLocation, pUnit );
}

/*************************************************************************************************/
VVOID BasicEffect::MakeCurrent(EffectTechnique* pCurrent)
{
	if
		( pCurrent == NULL )
	{
		return;
	}

	pCurrent->MakeCurrent();
	this->mCurrentTechnique = pCurrent;

	// Check technique's particularities ans set proper flags
	map<string, BBOOL>::const_iterator lIter = this->mFlagMap.find( this->mCurrentTechnique->Name() );

	if
		( lIter != this->mFlagMap.end() )
	{
		BBOOL lUseTexture = lIter->second;

		UBIGINT lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), "gUseTexure" );
		if
			( lShaderLocation == -1 )
		{
			return;
		}

		this->mDevice->glUniform1i( lShaderLocation, lUseTexture );
	}
}

/*************************************************************************************************/
EffectTechnique* BasicEffect::Light1Tech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight1TechName, pStagePaths, pIncludes );
}

/*************************************************************************************************/
EffectTechnique* BasicEffect::Light2Tech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight2TechName, pStagePaths, pIncludes );
}

/*************************************************************************************************/
EffectTechnique* BasicEffect::Light3Tech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight3TechName, pStagePaths, pIncludes );
}

/*************************************************************************************************/
EffectTechnique* BasicEffect::Light0TexTech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight0TexTechName, pStagePaths, pIncludes );
}

/*************************************************************************************************/
EffectTechnique* BasicEffect::Light1TexTech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight1TexTechName, pStagePaths, pIncludes );
}

/*************************************************************************************************/
EffectTechnique* BasicEffect::Light2TexTech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight2TexTechName, pStagePaths, pIncludes );
}

/*************************************************************************************************/
EffectTechnique* BasicEffect::Light3TexTech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight3TexTechName, pStagePaths, pIncludes );
}
