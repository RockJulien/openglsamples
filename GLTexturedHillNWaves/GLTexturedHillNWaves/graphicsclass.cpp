////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "graphicsclass.h"
#include "TGALoader.h"
#include <fstream>
#include "MathUtilities.h"
#include <time.h>
#include "systemclass.h"
#include "MeshData.h"

using namespace std;
using namespace Ocelot;

/*********************************************************************************************/
GraphicsClass::GraphicsClass() :
mOpenGL(NULL), mCamera(NULL), mController(NULL), 
mLandVAO(0), mLandVB(0), mLandIB(0), mWaveVAO(0),
mWaveVB(0), mWaveIB(0), mLight3TexTech(NULL)
{
	srand((UBIGINT)time(NULL));

	// Initialize the camera.
	OcelotCameraInfo lCamInfo( Vector3( 0.0f, 2.0f, -15.0f ), 
							   Vector3( 0.0f, 0.0f, 1.0f ), 
							   (FFLOAT)(800 / 600), 
							   1000.0f );
	lCamInfo.SetNear(1.0f);
	this->mCamera = new OcelotCamera( lCamInfo );

	// And create the controller
	this->mController = ControlFact->createController( this->mCamera, 
													   CAMERA_CONTROLLER_FLYER );

	this->mLandTexTransform.Matrix4x4Scaling( this->mLandTexTransform, 5.0f, 5.0f, 0.0f );

	// Initialize the light object(s).
	this->mDirLights[0].SetAmbient( Color( 0.2f, 0.2f, 0.2f, 1.0f ) );
	this->mDirLights[0].SetDiffuse( Color( 0.5f, 0.5f, 0.5f, 1.0f ) );
	this->mDirLights[0].SetSpecular( Color( 0.5f, 0.5f, 0.5f, 1.0f ) );
	this->mDirLights[0].SetDirection( Vector3( 0.57735f, -0.57735f, 0.57735f ) );

	this->mDirLights[1].SetAmbient( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[1].SetDiffuse( Color( 0.20f, 0.20f, 0.20f, 1.0f ) );
	this->mDirLights[1].SetSpecular( Color( 0.25f, 0.25f, 0.25f, 1.0f ) );
	this->mDirLights[1].SetDirection( Vector3( -0.57735f, -0.57735f, 0.57735f ) );

	this->mDirLights[2].SetAmbient( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[2].SetDiffuse( Color( 0.2f, 0.2f, 0.2f, 1.0f ) );
	this->mDirLights[2].SetSpecular( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[2].SetDirection( Vector3( 0.0f, -0.707f, -0.707f ) );

	// Initialize materials
	this->mLandMat.Ambient  = Color( 0.5f, 0.5f, 0.5f, 1.0f );
	this->mLandMat.Diffuse  = Color( 1.0f, 1.0f, 1.0f, 1.0f );
	this->mLandMat.Specular = Color( 0.2f, 0.2f, 0.2f, 16.0f );

	this->mWaveMat.Ambient  = Color( 0.5f, 0.5f, 0.5f, 1.0f );
	this->mWaveMat.Diffuse  = Color( 1.0f, 1.0f, 1.0f, 1.0f );
	this->mWaveMat.Specular = Color( 0.8f, 0.8f, 0.8f, 32.0f );

	this->mDiffuseTexUnit = 1;
}

/*********************************************************************************************/
GraphicsClass::~GraphicsClass()
{
	
}

/*********************************************************************************************/
BBOOL GraphicsClass::Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight)
{
	BBOOL lResult = false;

	// Store a pointer to the OpenGL class object.
	this->mOpenGL = pGDevice;

	Effects::InitializeAll( this->mOpenGL );
	InputLayouts::InitializeAll( this->mOpenGL );

	// Initialize the waves
	this->mWaves.Init( 160, 160, 1.0f, 0.03f, 3.25f, 0.4f );

	// Initialize the texture manager
	this->mTexManager.Initialize( this->mOpenGL );

	// Init the diffuse map to apply to the mesh.
	ExtType lType = ExtType::eDDS;

	this->mLandMapRV = this->mTexManager.CreateTexture( "Textures/grass.dds", lType );
	this->mWaveMapRV = this->mTexManager.CreateTexture( "Textures/water2.dds", lType );

	// Initialize the technique.
	vector<string> lIncludes;
	lIncludes.push_back("Shaders/LightHelper.helper"); // Need light include
	StageDescription lDescription;
	lDescription.SetVertexStagePath("Shaders/Basic.vs");
	lDescription.SetPixelStagePath("Shaders/Basic.ps");
	this->mLight3TexTech = Effects::BasicFX()->Light3TexTech( lDescription, lIncludes );
	this->mLight3TexTech->SetLayout( InputLayouts::PosNormalTexLayout() );

	this->BuildLandGeometryBuffers();
	this->BuildWaveGeometryBuffers();

	return true;
}

/*********************************************************************************************/
VVOID GraphicsClass::OnResize(BIGINT pClientWidth, BIGINT pClientHeight)
{
	FFLOAT lAspect = (FFLOAT)pClientWidth / pClientHeight;

	// refresh the aspect of the camera if the
	// window is resized.
	OcelotCameraInfo lInfo = this->mCamera->Info();
	if( this->mCamera != NULL )
	{
		lInfo.SetAspect( lAspect );  // aspect ratio
		this->mCamera->SetInfo( lInfo );
	}

	glViewport( 0, 0, pClientWidth, pClientHeight );
}

/*********************************************************************************************/
VVOID GraphicsClass::Shutdown()
{
	DeletePointer( this->mCamera );
	DeletePointer( this->mController );
	DeletePointer( this->mLight3TexTech );

	glDeleteTextures( 1, &this->mLandMapRV );
	glDeleteTextures( 1, &this->mWaveMapRV );

	// Release VAOs, VBOs and IBOs
	// Release shapes buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mLandVB );
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mLandIB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mLandVAO );

	// Release Skull buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mWaveVB );
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mWaveIB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mWaveVAO );

	Effects::DestroyAll();
	InputLayouts::DestroyAll();
}

/*********************************************************************************************/
BBOOL GraphicsClass::Update(FFLOAT pDeltaTime)
{
	// Update the camera.
	this->mController->update( pDeltaTime );

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Start Update Failure !!! ");
	}

	//
	// Every quarter second, generate a random wave.
	//
	static FFLOAT sBase = 0.0f;
	FFLOAT lTotalTime = SystemClass::GetTotalTime();
	if( (lTotalTime - sBase) >= 0.25f )
	{
		sBase += 0.25f;
 
		ULINT lRow = 5 + rand() % (this->mWaves.RowCount() - 10 );
		ULINT lCol = 5 + rand() % (this->mWaves.ColumnCount() - 10 );

		FFLOAT lMagnitude = RandBetween( 1.0f, 2.0f );

		this->mWaves.Disturb( lRow, lCol, lMagnitude );
	}

	this->mWaves.Update( pDeltaTime );

	//
	// Update the wave vertex buffer with the new solution.
	//
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mWaveVB );
	SimpleVertex* lVBO = reinterpret_cast<SimpleVertex*>( this->mOpenGL->glMapBuffer( GL_ARRAY_BUFFER, GL_WRITE_ONLY ) );
	if 
		( lVBO )
	{
		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Map Waves VBO Failure !!! ");
		}

		// Update waves VBO
		for( UBIGINT lCurrVertex = 0; lCurrVertex < this->mWaves.VertexCount(); lCurrVertex++ )
		{
			lVBO[ lCurrVertex ].Position = this->mWaves[ lCurrVertex ];
			lVBO[ lCurrVertex ].Normal   = this->mWaves.Normal( lCurrVertex );

			// Derive tex-coords in [0,1] from position.
			lVBO[ lCurrVertex ].TexCoord.x( 0.5f + this->mWaves[ lCurrVertex ].getX() / this->mWaves.Width() );
			lVBO[ lCurrVertex ].TexCoord.y( 0.5f - this->mWaves[ lCurrVertex ].getZ() / this->mWaves.Depth() );
		}

		this->mOpenGL->glUnmapBuffer( GL_ARRAY_BUFFER );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Unmap Waves VBO Failure !!! ");
		}
	}

	//
	// Animate water texture coordinates.
	//

	// Tile water texture.
	Matrix4x4 lWavesScale;
	lWavesScale.Matrix4x4Scaling( lWavesScale, 5.0f, 5.0f, 0.0f );

	// Translate texture over time.
	this->mWaterTexOffset.y( this->mWaterTexOffset.getY() + 0.05f * pDeltaTime );
	this->mWaterTexOffset.x( this->mWaterTexOffset.getX() + 0.1f * pDeltaTime );	
	Matrix4x4 lWavesOffset;
	lWavesOffset.Matrix4x4Translation( lWavesOffset, this->mWaterTexOffset.getX(), this->mWaterTexOffset.getY(), 0.0f );

	// Combine scale and translation.
	this->mWaveTexTransform = lWavesScale * lWavesOffset;

	return true;
}

/*********************************************************************************************/
BBOOL GraphicsClass::Render()
{
	// Clear the buffers to begin the scene.
	this->mOpenGL->StartRender( true, true );

	glClearColor( 0.69f, 0.77f, 0.87f, 1.0f );
	glClearDepth( 1.0f );
	glClearStencil( 0 );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

	GLenum lError = glGetError();
	if( 
		lError != GL_NO_ERROR )
	{
		throw exception(" Before rendering FAILURE !!! ");
	}

	GLuint lStatus = this->mOpenGL->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	Matrix4x4 lView  = this->mCamera->View();
	Matrix4x4 lProj  = this->mCamera->Proj();
	Matrix4x4 lViewProj = lView * lProj;
	OcelotCameraInfo lInfo = this->mCamera->Info();

	if
		( Effects::BasicFX() != NULL )
	{
		// Make the technique current
		Effects::BasicFX()->MakeCurrent( this->mLight3TexTech );

		Effects::BasicFX()->SetDirLights( this->mDirLights, 2 );
		Effects::BasicFX()->SetEyePosW( lInfo.Eye() );

		// Active the first texture unit
		this->mOpenGL->glActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mLandMapRV );

		//
		// Draw the hills
		//
		Matrix4x4 lWorld = this->mLandWorld;
		Matrix4x4 lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		Matrix4x4 lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetTexTransform( this->mLandTexTransform );
		Effects::BasicFX()->SetMaterial( this->mLandMat );
		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		this->mOpenGL->glBindVertArray( this->mLandVAO );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Land Failure !!! ");
		}

		glDrawElements( GL_TRIANGLES, this->mLandIndexCount, GL_UNSIGNED_INT, NULL );
		
		// Unbind Land VAO
		this->mOpenGL->glBindVertArray( 0 );

		// Active the first texture unit
		this->mOpenGL->glActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mWaveMapRV );

		//
		// Draw the waves.
		//
		lWorld = this->mWaveWorld;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetTexTransform( this->mWaveTexTransform );
		Effects::BasicFX()->SetMaterial( this->mWaveMat );
		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		this->mOpenGL->glBindVertArray( this->mWaveVAO );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Waves Failure !!! ");
		}

		glDrawElements( GL_TRIANGLES, 3 * this->mWaves.TriangleCount(), GL_UNSIGNED_INT, NULL );

		// Unbind Waves VAO
		this->mOpenGL->glBindVertArray( 0 );
	}

	lStatus = this->mOpenGL->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	// Present the rendered scene to the screen.
	this->mOpenGL->StopRender();

	return true;
}

/*********************************************************************************************/
FFLOAT  GraphicsClass::GetHillHeight( FFLOAT pX, FFLOAT pZ ) const
{
	return 0.3f * ( pZ * sinf( 0.1f * pX ) + pX * cosf( 0.1f * pZ ) );
}

/*********************************************************************************************/
Vector3 GraphicsClass::GetHillNormal( FFLOAT pX, FFLOAT pZ ) const
{
	// Normal = (-df / dx, 1, -df / dz)
	Vector3 lNormal( -0.03f * pZ * cosf( 0.1f * pX ) - 0.3f * cosf( 0.1f * pZ ),
					 1.0f,
					 -0.3f * sinf( 0.1f * pX ) + 0.03f * pX * sinf( 0.1f * pZ ) );
	
	Vector3 lUnitNormal = lNormal.Vec3Normalise();

	return lUnitNormal;
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildLandGeometryBuffers()
{
	MeshData lGrid;

	GeometryHelper::CreateGrid( 160.0f, 160.0f, 50, 50, lGrid );

	this->mLandIndexCount = (UBIGINT)lGrid.Indices.size();

	//
	// Extract the vertex elements we are interested and apply the height function to
	// each vertex.  
	//
	std::vector<SimpleVertex> lVertices( lGrid.Vertices.size() );
	for( UBIGINT lCurrVert = 0; lCurrVert < (UBIGINT)lGrid.Vertices.size(); lCurrVert++ )
	{
		Vector3 lPosition = lGrid.Vertices[ lCurrVert ].Position;

		lPosition.y( this->GetHillHeight( lPosition.getX(), lPosition.getZ() ) );
		
		lVertices[ lCurrVert ].Position = lPosition;
		lVertices[ lCurrVert ].Normal   = this->GetHillNormal( lPosition.getX(), lPosition.getZ() );
		lVertices[ lCurrVert ].TexCoord = lGrid.Vertices[ lCurrVert ].TexCoord;
	}

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Land Geometry Failure !!! ");
	}

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mLandVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mLandVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mLandVB );

	// Bind the vertex buffer and load the vertex (position, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mLandVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, lVertices.size() * sizeof(SimpleVertex), &lVertices[0], GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::PosNormalTexLayout()->SetVertexLayout( this->mLandVB );

	// Generate an Index buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mLandIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mLandIB );
	this->mOpenGL->glBufferData( GL_ELEMENT_ARRAY_BUFFER, this->mLandIndexCount * sizeof(UBIGINT), &lGrid.Indices[0], GL_STATIC_DRAW );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Land Geometry Failure !!! ");
	}

	this->mOpenGL->glBindVertArray( 0 );
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildWaveGeometryBuffers()
{
	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Wave Geometry Failure !!! ");
	}

	// Create the vertex buffer.  Note that we allocate space only, as
	// we will be updating the data every time step of the simulation.

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mWaveVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mWaveVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mWaveVB );

	// Bind the vertex buffer and load the vertex (position, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mWaveVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, this->mWaves.VertexCount() * sizeof(SimpleVertex), NULL, GL_DYNAMIC_DRAW );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Wave Geometry Failure !!! ");
	}

	// Set the vertex stage layout.
	InputLayouts::PosNormalTexLayout()->SetVertexLayout( this->mWaveVB );

	// Create the index buffer.  The index buffer is fixed, so we only 
	// need to create and set once.

	vector<UBIGINT> lIndices( 3 * this->mWaves.TriangleCount() ); // 3 indices per face

	// Iterate over each quad.
	UBIGINT lRowCount = this->mWaves.RowCount();
	UBIGINT lColCount = this->mWaves.ColumnCount();
	BIGINT lCounter = 0;
	for( UBIGINT lCurrRow = 0; lCurrRow < lRowCount - 1; lCurrRow++ )
	{
		for( UBIGINT lCurrCol = 0; lCurrCol < lColCount - 1; lCurrCol++ )
		{
			lIndices[ lCounter ]   = lCurrRow * lColCount + lCurrCol;
			lIndices[ lCounter + 1 ] = lCurrRow * lColCount + lCurrCol + 1;
			lIndices[ lCounter + 2 ] = (lCurrRow + 1) * lColCount + lCurrCol;

			lIndices[ lCounter + 3 ] = (lCurrRow + 1) * lColCount + lCurrCol;
			lIndices[ lCounter + 4 ] = lCurrRow * lColCount + lCurrCol + 1;
			lIndices[ lCounter + 5 ] = (lCurrRow + 1) * lColCount + lCurrCol + 1;

			lCounter += 6; // next quad
		}
	}

	// Generate a Index buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mWaveIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mWaveIB );
	this->mOpenGL->glBufferData( GL_ELEMENT_ARRAY_BUFFER, lIndices.size() * sizeof(UBIGINT), &lIndices[0], GL_STATIC_DRAW );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Wave Geometry Failure !!! ");
	}

	// Unbind the VAO
	this->mOpenGL->glBindVertArray( 0 );
}

/*********************************************************************************************/
IOcelotCameraController* GraphicsClass::Controller()
{
	return this->mController;
}
