#ifndef DEF_MESHDATA_H
#define DEF_MESHDATA_H

// Includes
#include "VertexTypes.h"
#include <vector>

// Class definition
struct MeshData
{
	std::vector<Ocelot::EnhancedVertex> Vertices;
	std::vector<UBIGINT>				Indices;
};

#endif