////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _GRAPHICSCLASS_H_
#define _GRAPHICSCLASS_H_

///////////////////////
// INCLUDES //
///////////////////////
#include "GDevice.h"
#include "Camera\OcelotCamera.h"
#include "Camera\OcelotCameraFactory.h"
#include "GeometryHelper.h"
#include "OcelotLight.h"
#include "Material.h"
#include "Wave.h"
#include "BlurFilter.h"
#include "Effects\Effects.h"
#include "Effects\InputLayouts.h"
#include "TextureManager\TextureManager.h"

using namespace Ocelot;

/////////////
// GLOBALS //
/////////////
const BBOOL  FULL_SCREEN   = false;
const BBOOL  VSYNC_ENABLED = true;
const FFLOAT SCREEN_DEPTH  = 1.0f;
const FFLOAT SCREEN_NEAR   = 0.1f;

///////////////////////
// Rendering Options //
///////////////////////
enum RenderOptions
{
	eLighting = 0,
	eTextures = 1,
	eTexturesAndFog = 2
};

////////////////////////////////////////////////////////////////////////////////
// Class name: GraphicsClass
////////////////////////////////////////////////////////////////////////////////
class GraphicsClass
{
public:

	// Constructor & Destructor
	GraphicsClass();
	~GraphicsClass();

	// Methods
	BBOOL Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight);
	VVOID OnResize(BIGINT pClientWidth, BIGINT pClientHeight);
	VVOID Shutdown();
	BBOOL Update(FFLOAT pDeltaTime);
	BBOOL Render();

	// Accessors
	IOcelotCameraController* Controller();
	VVOID SetOption(RenderOptions pOption);

private:

	// Forbidden
	GraphicsClass(const GraphicsClass&);

	// Private Methods
	VVOID   DrawWrapper();
	VVOID   DrawScreenQuad();
	FFLOAT  GetHillHeight( FFLOAT pX, FFLOAT pZ ) const;
	Vector3 GetHillNormal( FFLOAT pX, FFLOAT pZ ) const;
	VVOID   BuildLandGeometryBuffers();
	VVOID   BuildWaveGeometryBuffers();
	VVOID   BuildCrateGeometryBuffers();
	VVOID   BuildScreenQuadGeometryBuffers();
	VVOID   BuildOffscreenViews();

	VVOID   ReleaseOffscreenViews();

private:

	// Attributes
	// Graphic device
	OpenGLClass*             mOpenGL;

	// Camera
	OcelotCamera*            mCamera;
	IOcelotCameraController* mController;

	// Screen bounds
	BIGINT					 mClientWidth;
	BIGINT					 mClientHeight;
	
	// Buffers
	UBIGINT					 mLandVAO;
	UBIGINT					 mLandVB;
	UBIGINT					 mLandIB;
	UBIGINT					 mWaveVAO;
	UBIGINT					 mWaveVB;
	UBIGINT					 mWaveIB;
	UBIGINT					 mBoxVAO;
	UBIGINT					 mBoxVB;
	UBIGINT					 mBoxIB;
	UBIGINT					 mScreenQuadVAO;
	UBIGINT					 mScreenQuadVB;
	UBIGINT					 mScreenQuadIB;

	// Offscreen Frame buffers
	UBIGINT					 mOffscreenRTV; // Frame buffer
	UBIGINT					 mOffscreenRBO; // Render buffer

	// Wave
	Wave					 mWaves;

	// Blur filter
	BlurFilter				 mBlur;

	// Lights
	Ocelot::OcelotParallelLight mDirLights[3];
	Material				    mLandMat;
	Material				    mWaveMat;
	Material					mBoxMat;

	// Transforms
	// Define transformations from local spaces to world space.
	Matrix4x4		 mLandWorld;
	Matrix4x4		 mWaveWorld;
	Matrix4x4		 mBoxWorld;
	Matrix4x4		 mLandTexTransform;
	Matrix4x4		 mWaveTexTransform;

	// Index and Offset Count
	UBIGINT			 mLandIndexCount;
	UBIGINT			 mWaveIndexCount;
	Vector2			 mWaterTexOffset;

	// Texture Manager
	TextureManager   mTexManager;
	UBIGINT			 mLandMapRV;
	UBIGINT			 mWaveMapRV;
	UBIGINT			 mBoxMapRV;
	UBIGINT			 mOffscreenRV1;
	UBIGINT			 mOffscreenRV2;

	UBIGINT			 mDiffuseTexUnit;
	UBIGINT			 mBlurUnits[2];

	// Effects
	EffectTechnique* mLight3Tech;
	EffectTechnique* mLight3TexTech;
	EffectTechnique* mLight3TexFogTech;
	EffectTechnique* mLight3TexAlphaClipTech;
	EffectTechnique* mLight3TexAlphaClipFogTech;
	EffectTechnique* mLight0TexTech;

	EffectTechnique* mBoxTech;
	EffectTechnique* mLandNWaveTech;

	// Options
	RenderOptions    mRenderOptions;
};

#endif