////////////////////////////////////////////////////////////////////////////////
// Filename: Horizontal Blur.cs
////////////////////////////////////////////////////////////////////////////////

const float gWeights[11] = { 0.05f, 0.05f, 0.1f, 0.1f, 0.1f, 0.2f, 0.1f, 0.1f, 0.1f, 0.05f, 0.05f };
const int   gBlurRadius  = 5;

#define N 256
#define CacheSize (N + 2 * gBlurRadius)
shared  vec4 gCache[ CacheSize ];
const ivec2 tileSize = ivec2(N, 1);

// Same as [numthreads(N, 1, 1)] in DirectX
layout(local_size_x=N, local_size_y=1, local_size_z=1) in;

// Imitate DirectX call to sync all and wait for group tasks to be done.
void GroupMemoryBarrierWithGroupSync() 
{ 
    memoryBarrierShared();
    barrier(); 
}

///////////////////////
// UNIFORM VARIABLES //
///////////////////////
layout(binding=0, rgba8) uniform image2D gInput;
layout(binding=1, rgba8) uniform image2D gOutput;
uniform ivec2     gImageLength; // Width on X and Height on Y

////////////////////////////////////////////////////////////////////////////////
// Compute Shader Stage
////////////////////////////////////////////////////////////////////////////////
void main(void)
{
	const ivec2 tile_xy   = ivec2(gl_WorkGroupID.xy); 
	const ivec2 thread_xy = ivec2(gl_LocalInvocationID.xy); 
	const ivec2 pixel_xy  = tile_xy * tileSize + thread_xy;
    ivec3 lGroupThreadID    = ivec3( thread_xy, 1 );
    ivec3 lDispatchThreadID = ivec3( pixel_xy, 1 );

    //
	// Fill local thread storage to reduce bandwidth. To blur 
	// N pixels, we will need to load N + 2 * BlurRadius pixels
	// due to the blur radius.
	//

    // This thread group runs N threads. To get the extra 2 * BlurRadius pixels, 
	// have 2 * BlurRadius threads sample an extra pixel.
    if( lGroupThreadID.x < gBlurRadius )
	{
		// Clamp out of bound samples that occur at image borders.
		int x = max( lDispatchThreadID.x - gBlurRadius, 0 );
		gCache[ lGroupThreadID.x ] = imageLoad( gInput, ivec2( x, lDispatchThreadID.y ) );
	}
	if( lGroupThreadID.x >= N - gBlurRadius )
	{
		// Clamp out of bound samples that occur at image borders.
		int x = min( lDispatchThreadID.x + gBlurRadius, gImageLength.x - 1 );
		gCache[ lGroupThreadID.x + 2 * gBlurRadius ] = imageLoad( gInput, ivec2( x, lDispatchThreadID.y ) );
	}

	// Clamp out of bound samples that occur at image borders.
	gCache[ lGroupThreadID.x + gBlurRadius ] = imageLoad( gInput, ivec2( min( lDispatchThreadID.xy, gImageLength.xy - 1) ) );

	// Wait for all threads to finish.
	GroupMemoryBarrierWithGroupSync();

    //
	// Now blur each pixel.
	//
	vec4 lBlurColor = vec4( 0.0, 0.0, 0.0, 0.0 );
	for( int i = -gBlurRadius; i <= gBlurRadius; i++ )
	{
		int k = lGroupThreadID.x + gBlurRadius + i;
		
		lBlurColor += gWeights[ i + gBlurRadius ] * gCache[ k ];
	}

    // Store result to output image. 
    imageStore( gOutput, ivec2( lDispatchThreadID.xy ), lBlurColor );
}
