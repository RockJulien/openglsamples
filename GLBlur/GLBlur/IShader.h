#ifndef DEF_ISHADER_H
#define DEF_ISHADER_H

#include "GDevice.h"
#include "OcelotLight.h"
#include "OcelotFog.h"

using namespace Ocelot;

class IShader
{
public:

		// Constructor & Destructor
		IShader() {}
virtual ~IShader(){}

	// Methods
virtual BBOOL Initialize(GDevice* device, HWND winInst) = 0;
virtual BBOOL UpdateShader(GDevice* device, FFLOAT* world, FFLOAT* view, FFLOAT* proj,
					       FFLOAT* worldInv, OcelotSpotLight* light, OcelotFog* fog, 
					       FFLOAT* eye, BIGINT* textUnit = NULL) = 0;
virtual VVOID Release(GDevice* device) = 0;

};

#endif