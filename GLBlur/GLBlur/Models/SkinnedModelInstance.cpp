#include "SkinnedModelInstance.h"

// Namespaces used
using namespace Ocelot;

/*************************************************************************************/
SkinnedModelInstance::SkinnedModelInstance()
{

}

/*************************************************************************************/
SkinnedModelInstance::~SkinnedModelInstance()
{
	
}

/*************************************************************************************/
void SkinnedModelInstance::Update(FFLOAT pDeltaTime)
{
	this->TimePos += pDeltaTime;
	this->Model->SkinnedData().ComputeFinalTransforms(this->ClipName, this->TimePos, this->FinalTransforms);

	// Loop animation
	if(this->TimePos > this->Model->SkinnedData().ClipEndTime(this->ClipName))
	{
		this->TimePos = 0.0f;
	}
}
