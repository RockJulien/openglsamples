#ifndef DEF_BLURFILTER_H
#define DEF_BLURFILTER_H

// Includes
#include "GDevice.h"
#include "Effects\EffectTechnique.h"

// Namespaces


// Class definition
class BlurFilter
{
private:

	// Attributes
	GDevice* mDevice;
	BIGINT   mLength[2];
	UBIGINT  mUnits[2];

	// Two for ping ponging
	UBIGINT  mBlurredOutputTexRV1;
	UBIGINT  mBlurredOutputTexRV2;

	// Techniques
	EffectTechnique* mHorizontalBlur;
	EffectTechnique* mVerticalBlur;

	VVOID    ReleaseTextures();
	VVOID    ReleaseTechniques();

public:

	// Constructor & Destructor
	BlurFilter();
	~BlurFilter();

	// Methods
	// The width and height should match the dimensions of the input texture to blur.
	// It is OK to call Init() again to reinitialize the blur filter with a different 
	// dimension or format.
	VVOID Initialize(GDevice* pDevice, UBIGINT pUnits[2], UBIGINT pWidth, UBIGINT pHeight);

	// Blurs the input texture blurCount times.  
	// Note that this modifies the input texture, not a copy of it.
	VVOID BlurInPlace(const UBIGINT& pInputID, const UBIGINT& pOutputID, BIGINT pBlurCount);

	// Generate Gaussian blur weights.
	VVOID SetGaussianWeights(FFLOAT pSigma);

	// Manually specify blur weights.
	VVOID SetWeights(const FFLOAT pWeights[9]);

	UBIGINT GetBlurredOutput();
};

#endif