#include "BlurFilter.h"
#include "Effects\Effects.h"

// Namespaces used
using namespace std;

/***************************************************************************************************/
VVOID BlurFilter::ReleaseTextures()
{
	glDeleteTextures( 1, &this->mBlurredOutputTexRV1 );
	glDeleteTextures( 1, &this->mBlurredOutputTexRV2 );
}

/***************************************************************************************************/
VVOID BlurFilter::ReleaseTechniques()
{
	DeletePointer( this->mHorizontalBlur );
	DeletePointer( this->mVerticalBlur );
}

/***************************************************************************************************/
BlurFilter::BlurFilter() :
mBlurredOutputTexRV1(0), mBlurredOutputTexRV2(0),
mHorizontalBlur(NULL), mVerticalBlur(NULL)
{
	
}

/***************************************************************************************************/
BlurFilter::~BlurFilter()
{
	this->ReleaseTextures();
	this->ReleaseTechniques();
}

/***************************************************************************************************/
VVOID BlurFilter::Initialize(GDevice* pDevice, UBIGINT pUnits[2], UBIGINT pWidth, UBIGINT pHeight)
{
	this->mDevice = pDevice;

	// Start fresh.
	this->ReleaseTextures();
	this->ReleaseTechniques();

	this->mLength[0] = pWidth;
	this->mLength[1] = pHeight;
	this->mUnits[0]  = pUnits[0];
	this->mUnits[1]  = pUnits[1];

	// Generate images
	glGenTextures( 1, &this->mBlurredOutputTexRV1 );
	glGenTextures( 1, &this->mBlurredOutputTexRV2 );

	// Create two empty images for blur ping ponging.
	this->mDevice->glTextureStorage2D( this->mBlurredOutputTexRV1, GL_TEXTURE_2D, 1, GL_RGBA8, pWidth, pHeight );
	this->mDevice->glTextureStorage2D( this->mBlurredOutputTexRV2, GL_TEXTURE_2D, 1, GL_RGBA8, pWidth, pHeight );

	// Get techniques
	vector<string> lIncludes;
	StageDescription lDescription;
	lDescription.SetComputeStagePath("Shaders/HBlur.computeShader");
	this->mHorizontalBlur = Effects::BlurFX()->HorzBlurTech( lDescription, lIncludes );
	this->mHorizontalBlur->SetLayout( NULL ); // No Layout, but need link.
	lDescription.SetComputeStagePath("Shaders/VBlur.computeShader");
	this->mVerticalBlur   = Effects::BlurFX()->VertBlurTech( lDescription, lIncludes );
	this->mVerticalBlur->SetLayout( NULL ); // No Layout, but need link.
}

/***************************************************************************************************/
VVOID BlurFilter::BlurInPlace(const UBIGINT& pInputID, const UBIGINT& pOutputID, BIGINT pBlurCount)
{
	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Blur start Failure !!!");
	}

	//
	// Run the compute shader to blur the offscreen texture.
	// 
	if
		( Effects::BlurFX() != NULL )
	{
		for
			( BIGINT lCurrPass = 0; lCurrPass < pBlurCount; lCurrPass++ )
		{
			// HORIZONTAL blur pass.
			Effects::BlurFX()->MakeCurrent( this->mHorizontalBlur );

			Effects::BlurFX()->SetInputMap( this->mUnits[ 0 ], pInputID );
			Effects::BlurFX()->SetOutputMap( this->mUnits[ 1 ], this->mBlurredOutputTexRV1 );
			Effects::BlurFX()->SetImagesLength( this->mLength );

			// How many groups do we need to dispatch to cover a row of pixels, where each
			// group covers 256 pixels (the 256 is defined in the ComputeShader).
			UBIGINT lGroupXCount = (UBIGINT)ceilf( this->mLength[ 0 ] / 256.0f );
			this->mDevice->glDispatchCompute( lGroupXCount, this->mLength[ 1 ], 1 );

			Effects::BlurFX()->SetInputMap( this->mUnits[ 0 ], 0 );
			Effects::BlurFX()->SetOutputMap( this->mUnits[ 1 ], 0 );

			// VERTICAL blur pass.
			Effects::BlurFX()->MakeCurrent( this->mVerticalBlur );

			Effects::BlurFX()->SetInputMap( this->mUnits[ 0 ], this->mBlurredOutputTexRV1 );
			Effects::BlurFX()->SetOutputMap( this->mUnits[ 1 ], pOutputID );
			Effects::BlurFX()->SetImagesLength( this->mLength );

			// How many groups do we need to dispatch to cover a column of pixels, where each
			// group covers 256 pixels  (the 256 is defined in the ComputeShader).
			UBIGINT lGroupYCount = (UBIGINT)ceilf( this->mLength[ 1 ] / 256.0f );
			this->mDevice->glDispatchCompute( this->mLength[ 0 ], lGroupYCount, 1 );

			Effects::BlurFX()->SetInputMap( this->mUnits[ 0 ], 0 );
			Effects::BlurFX()->SetOutputMap( this->mUnits[ 1 ], 0 );
		}
	}
}

/***************************************************************************************************/
VVOID BlurFilter::SetGaussianWeights(FFLOAT pSigma)
{
	FFLOAT lD = 2.0f * SQUARE( pSigma );

	FFLOAT lWeights[9];
	FFLOAT lSum = 0.0f;
	for
		( BIGINT i = 0; i < 8; ++i)
	{
		FFLOAT lX = (FFLOAT)i;
		lWeights[i] = expf( -lX * lX / lD );

		lSum += lWeights[ i ];
	}

	// Divide by the sum so all the weights add up to 1.0.
	for
		( BIGINT i = 0; i < 8; i++ )
	{
		lWeights[ i ] /= lSum;
	}

	Effects::BlurFX()->MakeCurrent( this->mHorizontalBlur );
	Effects::BlurFX()->SetWeights( lWeights );

	Effects::BlurFX()->MakeCurrent( this->mVerticalBlur );
	Effects::BlurFX()->SetWeights( lWeights );
}

/***************************************************************************************************/
VVOID BlurFilter::SetWeights(const FFLOAT pWeights[9])
{
	Effects::BlurFX()->MakeCurrent( this->mHorizontalBlur );
	Effects::BlurFX()->SetWeights( pWeights );

	Effects::BlurFX()->MakeCurrent( this->mVerticalBlur );
	Effects::BlurFX()->SetWeights( pWeights );
}

/***************************************************************************************************/
UBIGINT BlurFilter::GetBlurredOutput()
{
	return this->mBlurredOutputTexRV1;
}
