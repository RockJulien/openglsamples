////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "graphicsclass.h"
#include "TGALoader.h"
#include <fstream>
#include "MathUtilities.h"
#include <time.h>
#include "systemclass.h"
#include "MeshData.h"

using namespace std;
using namespace Ocelot;

/*********************************************************************************************/
GraphicsClass::GraphicsClass() :
mOpenGL(NULL), mCamera(NULL), mController(NULL), 
mLandVAO(0), mLandVB(0), mLandIB(0), mWaveVAO(0),
mWaveVB(0), mWaveIB(0), mBoxVAO(0), mBoxVB(0), mLight0TexTech(NULL),
mBoxIB(0), mLight3Tech(NULL), mLight3TexTech(NULL),
mLight3TexFogTech(NULL), mLight3TexAlphaClipTech(NULL),
mScreenQuadVAO(0), mScreenQuadVB(0), mScreenQuadIB(0), mWaveIndexCount(0),
mLight3TexAlphaClipFogTech(NULL), mRenderOptions(RenderOptions::eTexturesAndFog)
{
	srand((UBIGINT)time(NULL));

	// Initialize the camera.
	OcelotCameraInfo lCamInfo( Vector3( 0.0f, 2.0f, -15.0f ), 
							   Vector3( 0.0f, 0.0f, 1.0f ), 
							   (FFLOAT)(800 / 600), 
							   1000.0f );
	lCamInfo.SetNear(1.0f);
	this->mCamera = new OcelotCamera( lCamInfo );

	// And create the controller
	this->mController = ControlFact->createController( this->mCamera, 
													   CAMERA_CONTROLLER_FLYER );

	Matrix4x4 lBoxScale;
	Matrix4x4 lBoxOffset;
	lBoxScale.Matrix4x4Scaling( lBoxScale, 15.0f, 15.0f, 15.0f );
	lBoxOffset.Matrix4x4Translation( lBoxOffset, 8.0f, 5.0f, -15.0f );
	this->mBoxWorld.Matrix4x4Mutliply( this->mBoxWorld, lBoxScale, lBoxOffset );

	this->mLandTexTransform.Matrix4x4Scaling( this->mLandTexTransform, 5.0f, 5.0f, 0.0f );

	// Initialize the light object(s).
	this->mDirLights[0].SetAmbient( Color( 0.2f, 0.2f, 0.2f, 1.0f ) );
	this->mDirLights[0].SetDiffuse( Color( 0.5f, 0.5f, 0.5f, 1.0f ) );
	this->mDirLights[0].SetSpecular( Color( 0.5f, 0.5f, 0.5f, 1.0f ) );
	this->mDirLights[0].SetDirection( Vector3( 0.57735f, -0.57735f, 0.57735f ) );

	this->mDirLights[1].SetAmbient( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[1].SetDiffuse( Color( 0.20f, 0.20f, 0.20f, 1.0f ) );
	this->mDirLights[1].SetSpecular( Color( 0.25f, 0.25f, 0.25f, 1.0f ) );
	this->mDirLights[1].SetDirection( Vector3( -0.57735f, -0.57735f, 0.57735f ) );

	this->mDirLights[2].SetAmbient( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[2].SetDiffuse( Color( 0.2f, 0.2f, 0.2f, 1.0f ) );
	this->mDirLights[2].SetSpecular( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[2].SetDirection( Vector3( 0.0f, -0.707f, -0.707f ) );

	// Initialize materials
	this->mLandMat.Ambient  = Color( 0.5f, 0.5f, 0.5f, 1.0f );
	this->mLandMat.Diffuse  = Color( 1.0f, 1.0f, 1.0f, 1.0f );
	this->mLandMat.Specular = Color( 0.2f, 0.2f, 0.2f, 16.0f );

	this->mWaveMat.Ambient  = Color( 0.5f, 0.5f, 0.5f, 1.0f );
	this->mWaveMat.Diffuse  = Color( 1.0f, 1.0f, 1.0f, 0.5f );
	this->mWaveMat.Specular = Color( 0.8f, 0.8f, 0.8f, 32.0f );

	this->mBoxMat.Ambient   = Color( 0.5f, 0.5f, 0.5f, 1.0f );
	this->mBoxMat.Diffuse   = Color( 1.0f, 1.0f, 1.0f, 1.0f );
	this->mBoxMat.Specular  = Color( 0.4f, 0.4f, 0.4f, 16.0f );

	this->mBlurUnits[0]   = 0;
	this->mBlurUnits[1]   = 1;
	this->mDiffuseTexUnit = 2;
}

/*********************************************************************************************/
GraphicsClass::~GraphicsClass()
{
	
}

/*********************************************************************************************/
BBOOL GraphicsClass::Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight)
{
	BBOOL lResult = false;

	// Store a pointer to the OpenGL class object.
	this->mOpenGL = pGDevice;
	this->mClientWidth  = pClientWidth;
	this->mClientHeight = pClientHeight;
	
	Effects::InitializeAll( this->mOpenGL );
	InputLayouts::InitializeAll( this->mOpenGL );

	// Initialize the waves
	this->mWaves.Init( 160, 160, 1.0f, 0.03f, 5.0f, 0.3f );
	this->mBlur.Initialize( this->mOpenGL, this->mBlurUnits, pClientWidth, pClientHeight );

	// Initialize the texture manager
	this->mTexManager.Initialize( this->mOpenGL );

	// Init the diffuse map to apply to the mesh.
	ExtType lType = ExtType::eDDS;

	this->mLandMapRV = this->mTexManager.CreateTexture( "Textures/grass.dds", lType );
	this->mWaveMapRV = this->mTexManager.CreateTexture( "Textures/water2.dds", lType );
	this->mBoxMapRV  = this->mTexManager.CreateTexture( "Textures/WireFence.dds", lType );

	// Initialize the technique(s).
	vector<string> lIncludes;
	lIncludes.push_back("Shaders/LightHelper.helper"); // Need light include
	StageDescription lDescription;
	lDescription.SetVertexStagePath("Shaders/Basic.vs");
	lDescription.SetPixelStagePath("Shaders/Basic.ps");
	this->mLight3Tech = Effects::BasicFX()->Light3Tech( lDescription, lIncludes );
	this->mLight3Tech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight3TexTech = Effects::BasicFX()->Light3TexTech( lDescription, lIncludes );
	this->mLight3TexTech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight3TexFogTech = Effects::BasicFX()->Light3TexFogTech( lDescription, lIncludes );
	this->mLight3TexFogTech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight3TexAlphaClipTech = Effects::BasicFX()->Light3TexAlphaClipTech( lDescription, lIncludes );
	this->mLight3TexAlphaClipTech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight3TexAlphaClipFogTech = Effects::BasicFX()->Light3TexAlphaClipFogTech( lDescription, lIncludes );
	this->mLight3TexAlphaClipFogTech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight0TexTech = Effects::BasicFX()->Light0TexTech( lDescription, lIncludes );
	this->mLight0TexTech->SetLayout( InputLayouts::PosNormalTexLayout() );

	this->BuildLandGeometryBuffers();
	this->BuildWaveGeometryBuffers();
	this->BuildCrateGeometryBuffers();
	this->BuildScreenQuadGeometryBuffers();
	this->BuildOffscreenViews();

	return true;
}

/*********************************************************************************************/
VVOID GraphicsClass::OnResize(BIGINT pClientWidth, BIGINT pClientHeight)
{
	this->mClientWidth  = pClientWidth;
	this->mClientHeight = pClientHeight;

	// Recreate the resources that depend on the client area size.
	this->BuildOffscreenViews();
	this->mBlur.Initialize( this->mOpenGL, this->mBlurUnits, pClientWidth, pClientHeight );

	FFLOAT lAspect = (FFLOAT)pClientWidth / pClientHeight;

	// refresh the aspect of the camera if the
	// window is resized.
	OcelotCameraInfo lInfo = this->mCamera->Info();
	if
		( this->mCamera != NULL )
	{
		lInfo.SetAspect( lAspect );  // aspect ratio
		this->mCamera->SetInfo( lInfo );
	}

	glViewport( 0, 0, pClientWidth, pClientHeight );
}

/*********************************************************************************************/
VVOID GraphicsClass::Shutdown()
{
	DeletePointer( this->mCamera );
	DeletePointer( this->mController );
	DeletePointer( this->mLight3Tech );
	DeletePointer( this->mLight3TexTech );
	DeletePointer( this->mLight3TexFogTech );
	DeletePointer( this->mLight3TexAlphaClipTech );
	DeletePointer( this->mLight3TexAlphaClipFogTech );

	glDeleteTextures( 1, &this->mLandMapRV );
	glDeleteTextures( 1, &this->mWaveMapRV );
	glDeleteTextures( 1, &this->mBoxMapRV );

	this->ReleaseOffscreenViews();

	// Release VAOs, VBOs and IBOs
	// Release Land buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mLandVB );
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mLandIB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mLandVAO );

	// Release Wave buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mWaveVB );
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mWaveIB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mWaveVAO );

	// Release Box buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mBoxVB );
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mBoxIB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mBoxVAO );

	// Release Trees buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mScreenQuadVB );
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mScreenQuadIB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mScreenQuadVAO );

	Effects::DestroyAll();
	InputLayouts::DestroyAll();
}

/*********************************************************************************************/
BBOOL GraphicsClass::Update(FFLOAT pDeltaTime)
{
	// Update the camera.
	this->mController->update( pDeltaTime );

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Start Update Failure !!! ");
	}

	//
	// Every quarter second, generate a random wave.
	//
	static FFLOAT sBase = 0.0f;
	FFLOAT lTotalTime = SystemClass::GetTotalTime();
	if( (lTotalTime - sBase) >= 0.25f )
	{
		sBase += 0.25f;
 
		ULINT lRow = 5 + rand() % (this->mWaves.RowCount() - 10 );
		ULINT lCol = 5 + rand() % (this->mWaves.ColumnCount() - 10 );

		FFLOAT lMagnitude = RandBetween( 0.5f, 1.0f );

		this->mWaves.Disturb( lRow, lCol, lMagnitude );
	}

	this->mWaves.Update( pDeltaTime );

	//
	// Update the wave vertex buffer with the new solution.
	//
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mWaveVB );
	SimpleVertex* lVBO = reinterpret_cast<SimpleVertex*>( this->mOpenGL->glMapBuffer( GL_ARRAY_BUFFER, GL_WRITE_ONLY ) );
	if 
		( lVBO )
	{
		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Map Waves VBO Failure !!! ");
		}

		// Update waves VBO
		for( UBIGINT lCurrVertex = 0; lCurrVertex < this->mWaves.VertexCount(); lCurrVertex++ )
		{
			lVBO[ lCurrVertex ].Position = this->mWaves[ lCurrVertex ];
			lVBO[ lCurrVertex ].Normal   = this->mWaves.Normal( lCurrVertex );

			// Derive tex-coords in [0,1] from position.
			lVBO[ lCurrVertex ].TexCoord.x( 0.5f + this->mWaves[ lCurrVertex ].getX() / this->mWaves.Width() );
			lVBO[ lCurrVertex ].TexCoord.y( 0.5f - this->mWaves[ lCurrVertex ].getZ() / this->mWaves.Depth() );
		}

		this->mOpenGL->glUnmapBuffer( GL_ARRAY_BUFFER );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Unmap Waves VBO Failure !!! ");
		}
	}

	//
	// Animate water texture coordinates.
	//

	// Tile water texture.
	Matrix4x4 lWavesScale;
	lWavesScale.Matrix4x4Scaling( lWavesScale, 5.0f, 5.0f, 0.0f );

	// Translate texture over time.
	this->mWaterTexOffset.y( this->mWaterTexOffset.getY() + 0.05f * pDeltaTime );
	this->mWaterTexOffset.x( this->mWaterTexOffset.getX() + 0.1f * pDeltaTime );	
	Matrix4x4 lWavesOffset;
	lWavesOffset.Matrix4x4Translation( lWavesOffset, this->mWaterTexOffset.getX(), this->mWaterTexOffset.getY(), 0.0f );

	// Combine scale and translation.
	this->mWaveTexTransform = lWavesScale * lWavesOffset;

	return true;
}

/*********************************************************************************************/
BBOOL GraphicsClass::Render()
{
	// Clear the buffers to begin the scene.
	this->mOpenGL->StartRender( true, true );

	glBindTexture( GL_TEXTURE_2D, 0 );
	// Bind Frame buffer for rendering to Offscreen FBO.
	this->mOpenGL->glBindFramebuffer( GL_FRAMEBUFFER, this->mOffscreenRTV );

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Set Offscreen FBO rendering target FAILURE !!! ");
	}

	glClearColor( 0.75f, 0.75f, 0.75f, 1.0f ); // Silver
	glClearDepth( 1.0f );
	glClearStencil( 0 );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

	//
	// Draw the scene to the offscreen texture
	//
	this->DrawWrapper();

	GLenum lStatus = this->mOpenGL->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	//
	// Restore the back buffer.  The offscreen render target will serve as an input into
	// the compute shader for blurring, so we must unbind it from the OM stage before we
	// can use it as an input into the compute shader.
	//
	this->mOpenGL->glBindFramebuffer( GL_FRAMEBUFFER, 0 );
	
	this->mBlur.BlurInPlace( this->mOffscreenRV1, this->mOffscreenRV2, 4 );

	//
	// Draw fullscreen quad with texture of blurred scene on it.
	//
	glClearColor( 0.75f, 0.75f, 0.75f, 1.0f ); // Silver
	glClearDepth( 1.0f );
	glClearStencil( 0 );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

	this->DrawScreenQuad();

	// Present the rendered scene to the screen.
	this->mOpenGL->StopRender();

	return true;
}

/*********************************************************************************************/
VVOID GraphicsClass::DrawWrapper()
{
	FFLOAT lBlendFactor[] = { 0.0f, 0.0f, 0.0f, 0.0f };

	GLenum lError = glGetError();
	if( 
		lError != GL_NO_ERROR )
	{
		throw exception(" Before rendering FAILURE !!! ");
	}

	GLuint lStatus = this->mOpenGL->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	Matrix4x4 lView  = this->mCamera->View();
	Matrix4x4 lProj  = this->mCamera->Proj();
	Matrix4x4 lViewProj = lView * lProj;
	OcelotCameraInfo lInfo = this->mCamera->Info();

	if
		( Effects::BasicFX() != NULL )
	{
		switch 
			( this->mRenderOptions )
		{
		case eLighting:
			this->mBoxTech = this->mLight3Tech;
			this->mLandNWaveTech = this->mLight3Tech;
			break;
		case eTextures:
			this->mBoxTech = this->mLight3TexAlphaClipTech;
			this->mLandNWaveTech = this->mLight3TexTech;
			break;
		case eTexturesAndFog:
			this->mBoxTech = this->mLight3TexAlphaClipFogTech;
			this->mLandNWaveTech = this->mLight3TexFogTech;
			break;
		}

		//
		// Draw the box with alpha clipping.
		// 

		// Make the box technique current
		Effects::BasicFX()->MakeCurrent( this->mBoxTech );

		Effects::BasicFX()->SetDirLights( this->mDirLights, 3 );
		Effects::BasicFX()->SetEyePosW( lInfo.Eye() );
		Effects::BasicFX()->SetFogColor( Color( 0.75f, 0.75f, 0.75f, 1.0f ) );
		Effects::BasicFX()->SetFogStart( 15.0f );
		Effects::BasicFX()->SetFogRange( 175.0f );

		// Active the first texture unit
		this->mOpenGL->glActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mBoxMapRV );

		//
		// Draw the box
		// 
		Matrix4x4 lWorld = this->mBoxWorld;
		Matrix4x4 lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		Matrix4x4 lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetTexTransform( Matrix4x4() );
		Effects::BasicFX()->SetMaterial( this->mBoxMat );
		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		this->mOpenGL->glBindVertArray( this->mBoxVAO );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw box Failure !!! ");
		}

		glDisable( GL_CULL_FACE );
		glDrawElements( GL_TRIANGLES, 36, GL_UNSIGNED_INT, NULL );
		glEnable( GL_CULL_FACE );

		// Unbind Land VAO
		this->mOpenGL->glBindVertArray( 0 );

		//
		// Draw the hills and water with texture and fog (no alpha clipping needed).
		//

		// Active Land and Wave technique
		Effects::BasicFX()->MakeCurrent( this->mLandNWaveTech );

		Effects::BasicFX()->SetDirLights( this->mDirLights, 3 );
		Effects::BasicFX()->SetEyePosW( lInfo.Eye() );
		Effects::BasicFX()->SetFogColor( Color( 0.75f, 0.75f, 0.75f, 1.0f ) );
		Effects::BasicFX()->SetFogStart( 15.0f );
		Effects::BasicFX()->SetFogRange( 175.0f );

		// Active the first texture unit
		this->mOpenGL->glActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mLandMapRV );

		//
		// Draw the hills
		//
		lWorld = this->mLandWorld;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetTexTransform( this->mLandTexTransform );
		Effects::BasicFX()->SetMaterial( this->mLandMat );
		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		this->mOpenGL->glBindVertArray( this->mLandVAO );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Land Failure !!! ");
		}

		glDrawElements( GL_TRIANGLES, this->mLandIndexCount, GL_UNSIGNED_INT, NULL );
		
		// Unbind Land VAO
		this->mOpenGL->glBindVertArray( 0 );

		// Active the first texture unit
		this->mOpenGL->glActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mWaveMapRV );
		
		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Waves Failure !!! ");
		}

		//
		// Draw the waves.
		//
		lWorld = this->mWaveWorld;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetTexTransform( this->mWaveTexTransform );
		Effects::BasicFX()->SetMaterial( this->mWaveMat );
		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		this->mOpenGL->glBindVertArray( this->mWaveVAO );

		// Enable blending
		glEnable( GL_BLEND );
		this->mOpenGL->glBlendColor( lBlendFactor[0], lBlendFactor[1], lBlendFactor[2], lBlendFactor[3] );
		this->mOpenGL->glBlendEquationSeparate( GL_FUNC_ADD, GL_FUNC_ADD );
		this->mOpenGL->glBlendFuncSeparate( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO );
		
		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Waves Failure !!! ");
		}

		glDrawElements( GL_TRIANGLES, 3 * this->mWaves.TriangleCount(), GL_UNSIGNED_INT, NULL );

		glDisable( GL_BLEND );

		// Unbind Waves VAO
		this->mOpenGL->glBindVertArray( 0 );
		
		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Waves Failure !!! ");
		}
	}

	lStatus = this->mOpenGL->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}
}

/*********************************************************************************************/
VVOID GraphicsClass::DrawScreenQuad()
{
	if
		( Effects::BasicFX() != NULL )
	{
		Effects::BasicFX()->MakeCurrent( this->mLight0TexTech );

		Effects::BasicFX()->SetWorld( Matrix4x4() );
		Effects::BasicFX()->SetWorldInvTranspose( Matrix4x4() );
		Effects::BasicFX()->SetWorldViewProj( Matrix4x4() );
		Matrix4x4 lScaling;
		lScaling.Matrix4x4Scaling( lScaling, 1.0f, -1.0f, 1.0f );
		Effects::BasicFX()->SetTexTransform( lScaling );

		// Active the first texture unit
		this->mOpenGL->glActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mBlur.GetBlurredOutput() );

		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		// Set the screen quad as the current VAO to use.
		this->mOpenGL->glBindVertArray( this->mScreenQuadVAO );

		GLenum lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw screen quad Failure !!! ");
		}

		glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL );

		// Unbind screen quad VAO
		this->mOpenGL->glBindVertArray( 0 );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw screen quad Failure !!! ");
		}
	}
}

/*********************************************************************************************/
FFLOAT  GraphicsClass::GetHillHeight( FFLOAT pX, FFLOAT pZ ) const
{
	return 0.3f * ( pZ * sinf( 0.1f * pX ) + pX * cosf( 0.1f * pZ ) );
}

/*********************************************************************************************/
Vector3 GraphicsClass::GetHillNormal( FFLOAT pX, FFLOAT pZ ) const
{
	// Normal = (-df / dx, 1, -df / dz)
	Vector3 lNormal( -0.03f * pZ * cosf( 0.1f * pX ) - 0.3f * cosf( 0.1f * pZ ),
					 1.0f,
					 -0.3f * sinf( 0.1f * pX ) + 0.03f * pX * sinf( 0.1f * pZ ) );
	
	Vector3 lUnitNormal = lNormal.Vec3Normalise();

	return lUnitNormal;
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildLandGeometryBuffers()
{
	MeshData lGrid;

	GeometryHelper::CreateGrid( 160.0f, 160.0f, 50, 50, lGrid );

	this->mLandIndexCount = (UBIGINT)lGrid.Indices.size();

	//
	// Extract the vertex elements we are interested and apply the height function to
	// each vertex.  
	//
	std::vector<SimpleVertex> lVertices( lGrid.Vertices.size() );
	for( UBIGINT lCurrVert = 0; lCurrVert < (UBIGINT)lGrid.Vertices.size(); lCurrVert++ )
	{
		Vector3 lPosition = lGrid.Vertices[ lCurrVert ].Position;

		lPosition.y( this->GetHillHeight( lPosition.getX(), lPosition.getZ() ) );
		
		lVertices[ lCurrVert ].Position = lPosition;
		lVertices[ lCurrVert ].Normal   = this->GetHillNormal( lPosition.getX(), lPosition.getZ() );
		lVertices[ lCurrVert ].TexCoord = lGrid.Vertices[ lCurrVert ].TexCoord;
	}

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Land Geometry Failure !!! ");
	}

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mLandVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mLandVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mLandVB );

	// Bind the vertex buffer and load the vertex (position, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mLandVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, lVertices.size() * sizeof(SimpleVertex), &lVertices[0], GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::PosNormalTexLayout()->SetVertexLayout( this->mLandVB );

	// Generate an Index buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mLandIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mLandIB );
	this->mOpenGL->glBufferData( GL_ELEMENT_ARRAY_BUFFER, this->mLandIndexCount * sizeof(UBIGINT), &lGrid.Indices[0], GL_STATIC_DRAW );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Land Geometry Failure !!! ");
	}

	this->mOpenGL->glBindVertArray( 0 );
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildWaveGeometryBuffers()
{
	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Wave Geometry Failure !!! ");
	}

	// Create the vertex buffer.  Note that we allocate space only, as
	// we will be updating the data every time step of the simulation.

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mWaveVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mWaveVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mWaveVB );

	// Bind the vertex buffer and load the vertex (position, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mWaveVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, this->mWaves.VertexCount() * sizeof(SimpleVertex), NULL, GL_DYNAMIC_DRAW );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Wave Geometry Failure !!! ");
	}

	// Set the vertex stage layout.
	InputLayouts::PosNormalTexLayout()->SetVertexLayout( this->mWaveVB );

	// Create the index buffer.  The index buffer is fixed, so we only 
	// need to create and set once.

	vector<UBIGINT> lIndices( 3 * this->mWaves.TriangleCount() ); // 3 indices per face

	// Iterate over each quad.
	UBIGINT lRowCount = this->mWaves.RowCount();
	UBIGINT lColCount = this->mWaves.ColumnCount();
	BIGINT lCounter = 0;
	for( UBIGINT lCurrRow = 0; lCurrRow < lRowCount - 1; lCurrRow++ )
	{
		for( UBIGINT lCurrCol = 0; lCurrCol < lColCount - 1; lCurrCol++ )
		{
			lIndices[ lCounter ]   = lCurrRow * lColCount + lCurrCol;
			lIndices[ lCounter + 1 ] = lCurrRow * lColCount + lCurrCol + 1;
			lIndices[ lCounter + 2 ] = (lCurrRow + 1) * lColCount + lCurrCol;

			lIndices[ lCounter + 3 ] = (lCurrRow + 1) * lColCount + lCurrCol;
			lIndices[ lCounter + 4 ] = lCurrRow * lColCount + lCurrCol + 1;
			lIndices[ lCounter + 5 ] = (lCurrRow + 1) * lColCount + lCurrCol + 1;

			lCounter += 6; // next quad
		}
	}

	// Generate a Index buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mWaveIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mWaveIB );
	this->mOpenGL->glBufferData( GL_ELEMENT_ARRAY_BUFFER, lIndices.size() * sizeof(UBIGINT), &lIndices[0], GL_STATIC_DRAW );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Wave Geometry Failure !!! ");
	}

	// Unbind the VAO
	this->mOpenGL->glBindVertArray( 0 );
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildCrateGeometryBuffers()
{
	MeshData lBox;

	GeometryHelper::CreateBox( 1.0f, 1.0f, 1.0f, lBox );

	//
	// Extract the vertex elements we are interested in and pack the
	// vertices of all the meshes into one vertex buffer.
	//
	vector<SimpleVertex> lVertices( (UBIGINT)lBox.Vertices.size() );

	UBIGINT lCounter = 0;
	for( UBIGINT lCurr = 0; lCurr < (UBIGINT)lBox.Vertices.size(); lCurr++, lCounter++ )
	{
		lVertices[ lCounter ].Position = lBox.Vertices[ lCurr ].Position;
		lVertices[ lCounter ].Normal   = lBox.Vertices[ lCurr ].Normal;
		lVertices[ lCounter ].TexCoord = lBox.Vertices[ lCurr ].TexCoord;
	}

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mBoxVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mBoxVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mBoxVB );

	// Bind the vertex buffer and load the vertex (position, texture, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mBoxVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, (UBIGINT)lBox.Vertices.size() * sizeof(SimpleVertex), &lVertices[0], GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::PosNormalTexLayout()->SetVertexLayout( this->mBoxVB );

	vector<UBIGINT> lIndices;
	lIndices.insert( lIndices.end(), lBox.Indices.begin(), lBox.Indices.end() );

	// Generate a index buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mBoxIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mBoxIB );
	this->mOpenGL->glBufferData( GL_ELEMENT_ARRAY_BUFFER, (UBIGINT)lBox.Indices.size() * sizeof(UBIGINT), &lIndices[0], GL_STATIC_DRAW );

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Box geometry Failed !!! ");
	}

	this->mOpenGL->glBindVertArray( 0 );
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildScreenQuadGeometryBuffers()
{
	MeshData lQuad;
	GeometryHelper::CreateFullscreenQuad( lQuad );

	//
	// Extract the vertex elements we are interested in and pack the
	// vertices of all the meshes into one vertex buffer.
	//
	vector<SimpleVertex> lVertices( lQuad.Vertices.size() );
	for
		( UBIGINT lCurrVert = 0; lCurrVert < lQuad.Vertices.size(); lCurrVert++ )
	{
		lVertices[ lCurrVert ].Position = lQuad.Vertices[ lCurrVert ].Position;
		lVertices[ lCurrVert ].Normal   = lQuad.Vertices[ lCurrVert ].Normal;
		lVertices[ lCurrVert ].TexCoord = lQuad.Vertices[ lCurrVert ].TexCoord;
	}

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Screen quad Geometry Failure !!! ");
	}

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mScreenQuadVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mScreenQuadVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mScreenQuadVB );

	// Bind the vertex buffer and load the vertex (position, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mScreenQuadVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, lVertices.size() * sizeof(SimpleVertex), &lVertices[0], GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::PosNormalTexLayout()->SetVertexLayout( this->mScreenQuadVB );

	// Generate a index buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mScreenQuadIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mScreenQuadIB );
	this->mOpenGL->glBufferData( GL_ELEMENT_ARRAY_BUFFER, (UBIGINT)lQuad.Indices.size() * sizeof(UBIGINT), &lQuad.Indices[0], GL_STATIC_DRAW );

	lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Screen quad geometry Failed !!! ");
	}

	this->mOpenGL->glBindVertArray( 0 );
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildOffscreenViews()
{
	// Start fresh
	this->ReleaseOffscreenViews();

	// Create views.
	// Create the first offscreen texture we're going to render to
	glGenTextures( 1, &this->mOffscreenRV1 );
 
	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture( GL_TEXTURE_2D, this->mOffscreenRV1 );
 
	// Give an empty image to OpenGL ( the last "0" ) (See if RGBA16 Works with Float, else set RGB with UInt)
	glTexImage2D( GL_TEXTURE_2D, 
				  0, 
				  GL_RGBA8, 
				  this->mClientWidth, 
				  this->mClientHeight, 
				  0, 
				  GL_RGBA, 
				  GL_UNSIGNED_BYTE, 
				  NULL );

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Build texture view Failure !!! ");
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Build texture view Failure !!! ");
	}

	// Create the second offscreen texture we're going to render to
	glGenTextures( 1, &this->mOffscreenRV2 );
 
	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture( GL_TEXTURE_2D, this->mOffscreenRV2 );
 
	// Give an empty image to OpenGL ( the last "0" ) (See if RGBA16 Works with Float, else set RGB with UInt)
	glTexImage2D( GL_TEXTURE_2D, 
				  0, 
				  GL_RGBA8, 
				  this->mClientWidth, 
				  this->mClientHeight, 
				  0, 
				  GL_RGBA, 
				  GL_UNSIGNED_BYTE, 
				  NULL );

	lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Build texture view Failure !!! ");
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Build texture view Failure !!! ");
	}

	// Create the Render target view object
	this->mOpenGL->glGenFrameBuffers( 1, &this->mOffscreenRTV );
	this->mOpenGL->glBindFramebuffer( GL_FRAMEBUFFER, this->mOffscreenRTV );

	// Set Texture as our colour attachement #0 to render to
	this->mOpenGL->glFrameBufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->mOffscreenRV1, 0 );

	lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Create FBO render target view Failure !!! ");
	}

	this->mOpenGL->glGenRenderBuffers( 1, &this->mOffscreenRBO );
	this->mOpenGL->glBindRenderbuffer( GL_RENDERBUFFER, this->mOffscreenRBO );
	this->mOpenGL->glRenderbufferStorage( GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, this->mClientWidth, this->mClientHeight );

	lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Create FBO render target view Failure !!! ");
	}

	// attach a renderbuffer to depth attachment point
	this->mOpenGL->glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, this->mOffscreenRBO );

	GLenum lStatus = this->mOpenGL->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if
		( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception("FBO Check FAILURE !!! ");
	}

	this->mOpenGL->glBindFramebuffer( GL_FRAMEBUFFER, 0 );
}

/*********************************************************************************************/
VVOID   GraphicsClass::ReleaseOffscreenViews()
{
	glDeleteTextures( 1, &this->mOffscreenRV1 );
	glDeleteTextures( 1, &this->mOffscreenRV2 );

	this->mOpenGL->glDeleteFrameBuffers( 1, &this->mOffscreenRTV );
	this->mOpenGL->glDeleteRenderBuffers( 1, &this->mOffscreenRBO );
}

/*********************************************************************************************/
IOcelotCameraController* GraphicsClass::Controller()
{
	return this->mController;
}

/*********************************************************************************************/
VVOID GraphicsClass::SetOption(RenderOptions pOption)
{
	this->mRenderOptions = pOption;
}
