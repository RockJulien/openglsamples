#include "BlurEffect.h"
#include "Constants.h"

// Namespaces used
using namespace std;

/*************************************************************************************************/
BlurEffect::BlurEffect(GDevice* pDevice) :
mCurrentTechnique(NULL), mDevice(pDevice)
{

}

/*************************************************************************************************/
BlurEffect::~BlurEffect()
{

}

/*************************************************************************************************/
VVOID BlurEffect::SetWeights(const FFLOAT pWeights[9])
{
	UBIGINT lShaderLocation;

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gWeights");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform1fv( lShaderLocation, 9, pWeights );
}

/*************************************************************************************************/
VVOID BlurEffect::SetInputMap(const UBIGINT& pInputUnit, const UBIGINT& pInputID)
{
	// For compute shader, we pass texture unit and texture ID thanks to the glBindImageTexture method 
	// aiming the binding value in the shader set by the shader programmer (1). As this is the output image,
	// the access is read write.
	GLboolean lIsNotLayered = GL_FALSE; 
	this->mDevice->glBindImageTexture( /*image*/pInputUnit, 
									   pInputID, 
									   /*level*/0, 
									   lIsNotLayered, 
									   /*layer*/0, 
									   GL_READ_ONLY, 
									   GL_RGBA8 );

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Compute Shader Input image binding Failure !!! ");
	}
}

/*************************************************************************************************/
VVOID BlurEffect::SetOutputMap(const UBIGINT& pOutputUnit, const UBIGINT& pOutputID)
{
	// For compute shader, we pass texture unit and texture ID thanks to the glBindImageTexture method 
	// aiming the binding value in the shader set by the shader programmer (1). As this is the output image,
	// the access is read write.
	GLboolean lIsNotLayered = GL_FALSE; 
	this->mDevice->glBindImageTexture( /*image*/pOutputUnit, 
									   pOutputID, 
									   /*level*/0, 
									   lIsNotLayered, 
									   /*layer*/0, 
									   GL_READ_WRITE, 
									   GL_RGBA8 );

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Compute Shader Output image binding Failure !!! ");
	}
}

/*************************************************************************************************/
VVOID BlurEffect::SetImagesLength(const BIGINT pLength[2])
{
	if 
		( pLength == NULL )
	{
		return;
	}

	UBIGINT lShaderLocation;

	lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), "gImageLength" );
	if
		( lShaderLocation == -1 )
	{
		return;
	}

	this->mDevice->glUniform2iv( lShaderLocation, 1, pLength );
}

/*************************************************************************************************/
VVOID BlurEffect::MakeCurrent(EffectTechnique* pCurrent)
{
	if
		( pCurrent == NULL )
	{
		return;
	}

	pCurrent->MakeCurrent();
	this->mCurrentTechnique = pCurrent;

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Blur Effect Make Current FAILURE !!!");
	}
}

/*************************************************************************************************/
EffectTechnique* BlurEffect::HorzBlurTech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cHorzBlurTechName, pStagePaths, pIncludes );
}

/*************************************************************************************************/
EffectTechnique* BlurEffect::VertBlurTech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cVertBlurTechName, pStagePaths, pIncludes );
}
