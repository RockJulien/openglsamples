#include "Effects.h"

// Namespaces used

// Static initialization
BasicEffect* Effects::sBasicFX = NULL;
BlurEffect*  Effects::sBlurFX  = NULL;

/*****************************************************************************************************/
VVOID Effects::InitializeAll(GDevice* pDevice)
{
	if
		( Effects::sBasicFX == NULL )
	{
		Effects::sBasicFX = new BasicEffect( pDevice );
	}

	if
		( Effects::sBlurFX == NULL )
	{
		Effects::sBlurFX = new BlurEffect( pDevice );
	}
}

/*****************************************************************************************************/
VVOID Effects::DestroyAll()
{
	DeletePointer( Effects::sBasicFX );
	DeletePointer( Effects::sBlurFX );
}

/*****************************************************************************************************/
BasicEffect*      Effects::BasicFX()
{
	return Effects::sBasicFX;
}

/*****************************************************************************************************/
BlurEffect* Effects::BlurFX()
{
	return Effects::sBlurFX;
}

