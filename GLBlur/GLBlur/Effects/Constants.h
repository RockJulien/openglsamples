#ifndef DEF_CONSTANTS_H
#define DEF_CONSTANTS_H

// Includes
#include <string>
#include "..\OSCheck.h"

// Constants class
class Constants
{
public:

	// Constants
	static const std::string cLight1TechName;
	static const std::string cLight2TechName;
	static const std::string cLight3TechName;

	static const std::string cLight0TexTechName;
	static const std::string cLight1TexTechName;
	static const std::string cLight2TexTechName;
	static const std::string cLight3TexTechName;

	static const std::string cLight0TexAlphaClipTechName;
	static const std::string cLight1TexAlphaClipTechName;
	static const std::string cLight2TexAlphaClipTechName;
	static const std::string cLight3TexAlphaClipTechName;

	static const std::string cLight1FogTechName;
	static const std::string cLight2FogTechName;
	static const std::string cLight3FogTechName;

	static const std::string cLight0TexFogTechName;
	static const std::string cLight1TexFogTechName;
	static const std::string cLight2TexFogTechName;
	static const std::string cLight3TexFogTechName;

	static const std::string cLight0TexAlphaClipFogTechName;
	static const std::string cLight1TexAlphaClipFogTechName;
	static const std::string cLight2TexAlphaClipFogTechName;
	static const std::string cLight3TexAlphaClipFogTechName;

	static const std::string cHorzBlurTechName;
	static const std::string cVertBlurTechName;

	// Bit Masks for Light and flag format on 8 Bits (0bLLLLTAFR)
	static const USMALLINT cLightCountMask = 0xF0;
	static const USMALLINT cFlagsMask      = 0x0F;
	static const USMALLINT cUseTextureMask = 0x08;
	static const USMALLINT cAlphaClipMask  = 0x04;
	static const USMALLINT cFogEnabledMask = 0x02;
	static const USMALLINT cReflectionEnabledMask = 0x01;

};

#endif