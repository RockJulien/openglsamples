#ifndef DEF_BLUREFFECT_H
#define DEF_BLUREFFECT_H

// Includes
#include <map>
#include <string>
#include "..\GDevice.h"
#include "EffectTechnique.h"

// Namespaces


// Class definition
class BlurEffect
{
private:

	// Attributes
	EffectTechnique* mCurrentTechnique;
	GDevice*		 mDevice;

public:

	// Constructor & Destructor
	BlurEffect(GDevice* pDevice);
	~BlurEffect();

	// Methods
	VVOID SetWeights(const FFLOAT pWeights[9]);
	VVOID SetInputMap(const UBIGINT& pInputUnit, const UBIGINT& pInputID);
	VVOID SetOutputMap(const UBIGINT& pOutputUnit, const UBIGINT& pOutputID);
	VVOID SetImagesLength(const BIGINT pLength[2]);

	VVOID MakeCurrent(EffectTechnique* pCurrent);

	EffectTechnique* HorzBlurTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
	EffectTechnique* VertBlurTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
};

#endif