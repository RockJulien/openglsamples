#include "TextureManager.h"
#include "..\TextureHandler.h"

// Namespaces used
using namespace std;

/***********************************************************************************************/
TextureManager::TextureManager() :
mUnitCount(0)
{

}

/***********************************************************************************************/
TextureManager::~TextureManager()
{
	this->mTextures.clear();
}

/***********************************************************************************************/
VVOID     TextureManager::Initialize(GDevice* pDevice)
{
	this->mDevice = pDevice;
}

/***********************************************************************************************/
BIGINT    TextureManager::CreateTexture(std::string pFilename, ExtType pType)
{
	BIGINT lNewTexture = -1; // Default meaning failure.

	// If already exists
	if(this->mTextures.find(pFilename) != this->mTextures.end())
	{
		lNewTexture = this->mTextures[pFilename];
	}
	// Else create it.
	else
	{
		TextureHandler lHandler( this->mDevice );
		lNewTexture = lHandler.Load( pFilename.c_str(), pType, true );
	}

	return lNewTexture;
}

/***********************************************************************************************/
UBIGINT   TextureManager::UnitCount() const
{
	return this->mUnitCount;
}

/***********************************************************************************************/
VVOID     TextureManager::SetUnitCount(UBIGINT pCount)
{
	this->mUnitCount = pCount;
}
