////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "graphicsclass.h"
#include "TGALoader.h"
#include <fstream>
#include "MathUtilities.h"
#include <time.h>
#include "systemclass.h"
#include "MeshData.h"

using namespace std;
using namespace Ocelot;

/*********************************************************************************************/
GraphicsClass::GraphicsClass() :
mOpenGL(NULL), mCamera(NULL), mController(NULL), mShapesVB(0), mShapesIB(0), 
mSkullVB(0), mSkullIB(0), mSkullIndexCount(0), mLightCount(1), mLight3Tech(NULL),
mLight1Tech(NULL), mLight2Tech(NULL)
{
	srand((UBIGINT)time(NULL));

	// Initialize the camera.
	OcelotCameraInfo lCamInfo( Vector3( 0.0f, 2.0f, -15.0f ), 
							   Vector3( 0.0f, 0.0f, 1.0f ), 
							   (FFLOAT)(800 / 600), 
							   1000.0f );
	lCamInfo.SetNear(1.0f);
	this->mCamera = new OcelotCamera( lCamInfo );

	// And create the controller
	this->mController = ControlFact->createController( this->mCamera, 
													   CAMERA_CONTROLLER_FLYER );

	// Initialize meshes matrices
	this->mGridWorld.Matrix4x4Identity();

	Matrix4x4 lBoxScale;
	Matrix4x4 lBoxOffset;
	lBoxScale.Matrix4x4Scaling( lBoxScale, 3.0f, 1.0f, 3.0f );
	lBoxOffset.Matrix4x4Translation( lBoxOffset, 0.0f, 0.5f, 0.0f );
	this->mBoxWorld.Matrix4x4Mutliply( this->mBoxWorld, lBoxScale, lBoxOffset );

	Matrix4x4 lSkullScale;
	Matrix4x4 lSkullOffset;
	lSkullScale.Matrix4x4Scaling( lSkullScale, 0.5f, 0.5f, 0.5f );
	lSkullOffset.Matrix4x4Translation( lSkullOffset, 0.0f, 1.0f, 0.0f );
	this->mSkullWorld.Matrix4x4Mutliply( this->mSkullWorld, lSkullScale, lSkullOffset );

	for( BIGINT lCurr = 0; lCurr < 5; lCurr++ )
	{
		Matrix4x4 lTranslationCyl1;
		Matrix4x4 lTranslationCyl2;
		lTranslationCyl1.Matrix4x4Translation( lTranslationCyl1, -5.0f, 1.5f, -10.0f + lCurr * 5.0f );
		lTranslationCyl2.Matrix4x4Translation( lTranslationCyl2, +5.0f, 1.5f, -10.0f + lCurr * 5.0f );
		this->mCylWorld[ lCurr * 2 + 0 ].Matrix4x4Mutliply( this->mCylWorld[ lCurr * 2 + 0 ], lTranslationCyl1 );
		this->mCylWorld[ lCurr * 2 + 1 ].Matrix4x4Mutliply( this->mCylWorld[ lCurr * 2 + 1 ], lTranslationCyl2 );

		Matrix4x4 lTranslationSphere1;
		Matrix4x4 lTranslationSphere2;
		lTranslationSphere1.Matrix4x4Translation( lTranslationSphere1, -5.0f, 3.5f, -10.0f + lCurr * 5.0f );
		lTranslationSphere2.Matrix4x4Translation( lTranslationSphere2, +5.0f, 3.5f, -10.0f + lCurr * 5.0f );
		this->mSphereWorld[ lCurr * 2 + 0 ].Matrix4x4Mutliply( this->mSphereWorld[ lCurr * 2 + 0 ], lTranslationSphere1 );
		this->mSphereWorld[ lCurr * 2 + 1 ].Matrix4x4Mutliply( this->mSphereWorld[ lCurr * 2 + 1 ], lTranslationSphere2 );
	}

	this->mDirLights[0].SetAmbient( Color( 0.2f, 0.2f, 0.2f, 1.0f ) );
	this->mDirLights[0].SetDiffuse( Color( 0.5f, 0.5f, 0.5f, 1.0f ) );
	this->mDirLights[0].SetSpecular( Color( 0.5f, 0.5f, 0.5f, 1.0f ) );
	this->mDirLights[0].SetDirection( Vector3( 0.57735f, -0.57735f, 0.57735f ) );

	this->mDirLights[1].SetAmbient( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[1].SetDiffuse( Color( 0.20f, 0.20f, 0.20f, 1.0f ) );
	this->mDirLights[1].SetSpecular( Color( 0.25f, 0.25f, 0.25f, 1.0f ) );
	this->mDirLights[1].SetDirection( Vector3( -0.57735f, -0.57735f, 0.57735f ) );

	this->mDirLights[2].SetAmbient( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[2].SetDiffuse( Color( 0.2f, 0.2f, 0.2f, 1.0f ) );
	this->mDirLights[2].SetSpecular( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[2].SetDirection( Vector3( 0.0f, -0.707f, -0.707f ) );

	this->mGridMat.Ambient  = Color( 0.48f, 0.77f, 0.46f, 1.0f );
	this->mGridMat.Diffuse  = Color( 0.48f, 0.77f, 0.46f, 1.0f );
	this->mGridMat.Specular = Color( 0.2f, 0.2f, 0.2f, 16.0f );

	this->mCylMat.Ambient  = Color( 0.7f, 0.85f, 0.7f, 1.0f );
	this->mCylMat.Diffuse  = Color( 0.7f, 0.85f, 0.7f, 1.0f );
	this->mCylMat.Specular = Color( 0.8f, 0.8f, 0.8f, 16.0f );

	this->mSphereMat.Ambient  = Color( 0.1f, 0.2f, 0.3f, 1.0f );
	this->mSphereMat.Diffuse  = Color( 0.2f, 0.4f, 0.6f, 1.0f );
	this->mSphereMat.Specular = Color( 0.9f, 0.9f, 0.9f, 16.0f );

	this->mBoxMat.Ambient  = Color( 0.651f, 0.5f, 0.392f, 1.0f );
	this->mBoxMat.Diffuse  = Color( 0.651f, 0.5f, 0.392f, 1.0f );
	this->mBoxMat.Specular = Color( 0.2f, 0.2f, 0.2f, 16.0f );

	this->mSkullMat.Ambient  = Color( 0.8f, 0.8f, 0.8f, 1.0f );
	this->mSkullMat.Diffuse  = Color( 0.8f, 0.8f, 0.8f, 1.0f );
	this->mSkullMat.Specular = Color( 0.8f, 0.8f, 0.8f, 16.0f );
}

/*********************************************************************************************/
GraphicsClass::~GraphicsClass()
{
	
}

/*********************************************************************************************/
BBOOL GraphicsClass::Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight)
{
	BBOOL lResult = false;

	// Store a pointer to the OpenGL class object.
	this->mOpenGL = pGDevice;

	Effects::InitializeAll( this->mOpenGL );
	InputLayouts::InitializeAll( this->mOpenGL );

	vector<string> lIncludes;
	lIncludes.push_back("Shaders/LightHelper.helper");
	StageDescription lDescription;
	lDescription.SetVertexStagePath("Shaders/Basic.vs");
	lDescription.SetPixelStagePath("Shaders/Basic.ps");
	this->mLight1Tech = Effects::BasicFX()->Light1Tech( lDescription, lIncludes );
	this->mLight1Tech->SetLayout( InputLayouts::PosNormalLayout() );
	this->mLight2Tech = Effects::BasicFX()->Light2Tech( lDescription, lIncludes );
	this->mLight2Tech->SetLayout( InputLayouts::PosNormalLayout() );
	this->mLight3Tech = Effects::BasicFX()->Light3Tech( lDescription, lIncludes );
	this->mLight3Tech->SetLayout( InputLayouts::PosNormalLayout() );

	this->BuildShapeGeometryBuffers();
	this->BuildSkullGeometryBuffers();

	return true;
}

/*********************************************************************************************/
VVOID GraphicsClass::OnResize(BIGINT pClientWidth, BIGINT pClientHeight)
{
	FFLOAT lAspect = (FFLOAT)pClientWidth / pClientHeight;

	// refresh the aspect of the camera if the
	// window is resized.
	OcelotCameraInfo lInfo = this->mCamera->Info();
	if( this->mCamera != NULL )
	{
		lInfo.SetAspect( lAspect );  // aspect ratio
		this->mCamera->SetInfo( lInfo );
	}

	glViewport( 0, 0, pClientWidth, pClientHeight );
}

/*********************************************************************************************/
VVOID GraphicsClass::Shutdown()
{
	Effects::DestroyAll();
	InputLayouts::DestroyAll();

	DeletePointer( this->mCamera );
	DeletePointer( this->mController );
	DeletePointer( this->mLight1Tech );
	DeletePointer( this->mLight2Tech );
	DeletePointer( this->mLight3Tech );

	// Release VAOs, VBOs and IBOs
	// Release shapes buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mShapesVB );
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mShapesIB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mShapesVAO );

	// Release Skull buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mSkullVB );
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mSkullIB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mSkullVAO );
}

/*********************************************************************************************/
BBOOL GraphicsClass::Update(FFLOAT pDeltaTime)
{
	// Update the camera.
	this->mController->update( pDeltaTime );

	return true;
}

/*********************************************************************************************/
BBOOL GraphicsClass::Render()
{
	// Clear the buffers to begin the scene.
	this->mOpenGL->StartRender( true, true );

	GLenum lError = glGetError();
	if( 
		lError != GL_NO_ERROR )
	{
		throw exception(" Before rendering FAILURE !!! ");
	}

	GLuint lStatus = this->mOpenGL->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	Matrix4x4 lView  = this->mCamera->View();
	Matrix4x4 lProj  = this->mCamera->Proj();
	Matrix4x4 lViewProj = lView * lProj;
	OcelotCameraInfo lInfo = this->mCamera->Info();

	EffectTechnique* lCurrTechnique = NULL;
	switch ( this->mLightCount )
	{
	case 1:
		{
			lCurrTechnique = this->mLight1Tech;
		}
		break;
	case 2:
		{
			lCurrTechnique = this->mLight2Tech;
		}
		break;
	case 3:
		{
			lCurrTechnique = this->mLight3Tech;
		}
		break;
	}

	if
		( Effects::BasicFX() != NULL )
	{
		// Make the technique current
		Effects::BasicFX()->MakeCurrent( lCurrTechnique );

		Effects::BasicFX()->SetDirLights( this->mDirLights, this->mLightCount );
		Effects::BasicFX()->SetEyePosW( lInfo.Eye() );

		// Draw the grid.
		Matrix4x4 lWorld = this->mGridWorld;
		Matrix4x4 lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		Matrix4x4 lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetMaterial( this->mGridMat );

		this->mOpenGL->glBindVertArray( this->mShapesVAO );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Grid Failure !!! ");
		}

		this->mOpenGL->glDrawElementsBaseVertex( GL_TRIANGLES, this->mGridIndexCount, GL_UNSIGNED_INT, (VVOID*)(this->mGridIndexOffset * sizeof(UBIGINT)), this->mGridVertexOffset );

		// Unbind the VAO
		this->mOpenGL->glBindVertArray( 0 );

		// Draw the box.
		lWorld = this->mBoxWorld;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetMaterial( this->mBoxMat );

		this->mOpenGL->glBindVertArray( this->mShapesVAO );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Box Failure !!! ");
		}

		this->mOpenGL->glDrawElementsBaseVertex( GL_TRIANGLES, this->mBoxIndexCount, GL_UNSIGNED_INT, (VVOID*)(this->mBoxIndexOffset * sizeof(UBIGINT)), this->mBoxVertexOffset );

		// Unbind the VAO
		this->mOpenGL->glBindVertArray( 0 );

		// Draw the cylinders.
		for( BIGINT lCurrCyl = 0; lCurrCyl < 10; lCurrCyl++ )
		{
			lWorld = this->mCylWorld[ lCurrCyl ];
			lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
			lWorldViewProj = lWorld * lViewProj;

			Effects::BasicFX()->SetWorld( lWorld );
			Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
			Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
			Effects::BasicFX()->SetMaterial( this->mCylMat );

			this->mOpenGL->glBindVertArray( this->mShapesVAO );

			lError = glGetError();
			if
				( lError != GL_NO_ERROR )
			{
				throw exception(" Draw Cylinder Failure !!! ");
			}

			this->mOpenGL->glDrawElementsBaseVertex( GL_TRIANGLES, this->mCylinderIndexCount, GL_UNSIGNED_INT, (VVOID*)(this->mCylinderIndexOffset * sizeof(UBIGINT)), this->mCylinderVertexOffset );

			// Unbind the VAO
			this->mOpenGL->glBindVertArray( 0 );
		}

		// Draw the spheres.
		for( BIGINT lCurrSphere = 0; lCurrSphere < 10; lCurrSphere++ )
		{
			lWorld = this->mSphereWorld[ lCurrSphere ];
			lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
			lWorldViewProj = lWorld * lViewProj;

			Effects::BasicFX()->SetWorld( lWorld );
			Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
			Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
			Effects::BasicFX()->SetMaterial( this->mSphereMat );

			this->mOpenGL->glBindVertArray( this->mShapesVAO );

			lError = glGetError();
			if
				( lError != GL_NO_ERROR )
			{
				throw exception(" Draw Sphere Failure !!! ");
			}

			this->mOpenGL->glDrawElementsBaseVertex( GL_TRIANGLES, this->mSphereIndexCount, GL_UNSIGNED_INT, (VVOID*)(this->mSphereIndexOffset * sizeof(UBIGINT)), this->mSphereVertexOffset );

			// Unbind the VAO
			this->mOpenGL->glBindVertArray( 0 );
		}

		// Draw the skull.
		lWorld = this->mSkullWorld;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetMaterial( this->mSkullMat );

		this->mOpenGL->glBindVertArray( this->mSkullVAO );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Skull Failure !!! ");
		}

		glDrawElements( GL_TRIANGLES, this->mSkullIndexCount, GL_UNSIGNED_INT, NULL );

		// Unbind the VAO
		this->mOpenGL->glBindVertArray( 0 );
	}

	lStatus = this->mOpenGL->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	// Present the rendered scene to the screen.
	this->mOpenGL->StopRender();

	return true;
}

/*********************************************************************************************/
VVOID GraphicsClass::BuildShapeGeometryBuffers()
{
	MeshData lBox;
	MeshData lGrid;
	MeshData lSphere;
	MeshData lCylinder;

	GeometryHelper::CreateBox( 1.0f, 1.0f, 1.0f, lBox );
	GeometryHelper::CreateGrid( 20.0f, 30.0f, 60, 40, lGrid );
	GeometryHelper::CreateSphere( 0.5f, 20, 20, lSphere );
	GeometryHelper::CreateCylinder( 0.5f, 0.3f, 3.0f, 20, 20, lCylinder );

	// Cache the vertex offsets to each object in the concatenated vertex buffer.
	this->mBoxVertexOffset      = 0;
	this->mGridVertexOffset     = (BIGINT)lBox.Vertices.size();
	this->mSphereVertexOffset   = this->mGridVertexOffset + (BIGINT)lGrid.Vertices.size();
	this->mCylinderVertexOffset = this->mSphereVertexOffset + (BIGINT)lSphere.Vertices.size();

	// Cache the index count of each object.
	this->mBoxIndexCount      = (UBIGINT)lBox.Indices.size();
	this->mGridIndexCount     = (UBIGINT)lGrid.Indices.size();
	this->mSphereIndexCount   = (UBIGINT)lSphere.Indices.size();
	this->mCylinderIndexCount = (UBIGINT)lCylinder.Indices.size();

	// Cache the starting index for each object in the concatenated index buffer.
	this->mBoxIndexOffset      = 0;
	this->mGridIndexOffset     = this->mBoxIndexCount;
	this->mSphereIndexOffset   = this->mGridIndexOffset + this->mGridIndexCount;
	this->mCylinderIndexOffset = this->mSphereIndexOffset + this->mSphereIndexCount;
	
	UBIGINT lTotalVertexCount = (UBIGINT)lBox.Vertices.size() + (UBIGINT)lGrid.Vertices.size() + (UBIGINT)lSphere.Vertices.size() + (UBIGINT)lCylinder.Vertices.size();
	UBIGINT lTotalIndexCount = this->mBoxIndexCount + this->mGridIndexCount + this->mSphereIndexCount + this->mCylinderIndexCount;

	//
	// Extract the vertex elements we are interested in and pack the
	// vertices of all the meshes into one vertex buffer.
	//
	vector<VoxelVertex> lVertices( lTotalVertexCount );

	UBIGINT lCounter = 0;
	for( UBIGINT lCurr = 0; lCurr < (UBIGINT)lBox.Vertices.size(); lCurr++, lCounter++ )
	{
		lVertices[ lCounter ].m_vPosition = lBox.Vertices[ lCurr ].Position;
		lVertices[ lCounter ].m_vNormal   = lBox.Vertices[ lCurr ].Normal;
	}

	for( UBIGINT lCurr = 0; lCurr < (UBIGINT)lGrid.Vertices.size(); lCurr++, lCounter++ )
	{
		lVertices[ lCounter ].m_vPosition = lGrid.Vertices[ lCurr ].Position;
		lVertices[ lCounter ].m_vNormal   = lGrid.Vertices[ lCurr ].Normal;
	}

	for( UBIGINT lCurr = 0; lCurr < (UBIGINT)lSphere.Vertices.size(); lCurr++, lCounter++ )
	{
		lVertices[ lCounter ].m_vPosition = lSphere.Vertices[ lCurr ].Position;
		lVertices[ lCounter ].m_vNormal   = lSphere.Vertices[ lCurr ].Normal;
	}

	for( UBIGINT lCurr = 0; lCurr < (UBIGINT)lCylinder.Vertices.size(); lCurr++, lCounter++ )
	{
		lVertices[ lCounter ].m_vPosition = lCylinder.Vertices[ lCurr ].Position;
		lVertices[ lCounter ].m_vNormal   = lCylinder.Vertices[ lCurr ].Normal;
	}

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mShapesVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mShapesVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mShapesVB );

	// Bind the vertex buffer and load the vertex (position, texture, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mShapesVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, lTotalVertexCount * sizeof(VoxelVertex), &lVertices[0], GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::PosNormalLayout()->SetVertexLayout( this->mShapesVB );

	//
	// Pack the indices of all the meshes into one index buffer.
	//
	vector<UBIGINT> lIndices;
	lIndices.insert( lIndices.end(), lBox.Indices.begin(), lBox.Indices.end() );
	lIndices.insert( lIndices.end(), lGrid.Indices.begin(), lGrid.Indices.end() );
	lIndices.insert( lIndices.end(), lSphere.Indices.begin(), lSphere.Indices.end() );
	lIndices.insert( lIndices.end(), lCylinder.Indices.begin(), lCylinder.Indices.end() );

	// Generate a index buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mShapesIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mShapesIB );
	this->mOpenGL->glBufferData( GL_ELEMENT_ARRAY_BUFFER, lTotalIndexCount * sizeof(UBIGINT), &lIndices[0], GL_STATIC_DRAW );

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Shapes geometries Failed !!! ");
	}

	this->mOpenGL->glBindVertArray( 0 );
}

/*********************************************************************************************/
VVOID GraphicsClass::BuildSkullGeometryBuffers()
{
	ifstream lSkullFile("Models/skull.txt");
	if
		( lSkullFile.is_open() == false )
	{
		MessageBox( 0, L"Models/skull.txt not found.", 0, 0 );
		return;
	}

	UBIGINT lVCount = 0;
	UBIGINT lTCount = 0;
	string lIgnore;

	lSkullFile >> lIgnore >> lVCount;
	lSkullFile >> lIgnore >> lTCount;
	lSkullFile >> lIgnore >> lIgnore >> lIgnore >> lIgnore;
	
	vector<VoxelVertex> lVertices( lVCount );
	for( UBIGINT lCurrVertex = 0; lCurrVertex < lVCount; lCurrVertex++ )
	{
		FFLOAT lX, lY, lZ;
		lSkullFile >> lX >> lY >> lZ;
		lVertices[lCurrVertex].m_vPosition = Vector3( lX, lY, lZ );
		lSkullFile >> lX >> lY >> lZ;
		lVertices[lCurrVertex].m_vNormal   = Vector3( lX, lY, lZ );
	}

	lSkullFile >> lIgnore;
	lSkullFile >> lIgnore;
	lSkullFile >> lIgnore;

	this->mSkullIndexCount = 3 * lTCount;
	vector<UBIGINT> lIndices( this->mSkullIndexCount );
	for( UBIGINT lCurrIndex = 0; lCurrIndex < lTCount; lCurrIndex++ )
	{
		lSkullFile >> lIndices[lCurrIndex * 3 + 0] >> lIndices[lCurrIndex * 3 + 1] >> lIndices[lCurrIndex * 3 + 2];
	}

	lSkullFile.close();

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mSkullVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mSkullVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mSkullVB );

	// Bind the vertex buffer and load the vertex (position, texture, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mSkullVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, lVCount * sizeof(VoxelVertex), &lVertices[0], GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::PosNormalLayout()->SetVertexLayout( this->mSkullVB );

	// Generate a index buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mSkullIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mSkullIB );
	this->mOpenGL->glBufferData( GL_ELEMENT_ARRAY_BUFFER, this->mSkullIndexCount * sizeof(UBIGINT), &lIndices[0], GL_STATIC_DRAW );

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Skull geometry Failed !!! ");
	}

	this->mOpenGL->glBindVertArray( 0 );
}

/*********************************************************************************************/
IOcelotCameraController* GraphicsClass::Controller()
{
	return this->mController;
}

/*********************************************************************************************/
VVOID GraphicsClass::SetLightCount(const UBIGINT& pLightCount)
{
	this->mLightCount = pLightCount;
}
