#ifndef DEF_CONSTANTS_H
#define DEF_CONSTANTS_H

// Includes
#include <string>
#include "..\OSCheck.h"

// Constants class
class Constants
{
public:

	// Constants
	static const std::string cLight1TechName;
	static const std::string cLight2TechName;
	static const std::string cLight3TechName;

};

#endif