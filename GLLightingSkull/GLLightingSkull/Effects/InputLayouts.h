#ifndef DEF_INPUTLAYOUTS_H
#define DEF_INPUTLAYOUTS_H

// Includes
#include "InputLayout.h"

// Namespaces

// Class definition
class InputLayouts
{
private:

	// Attributes
	static InputLayout*	sPosNormalLayout;

public:

	// Constructor
	static VVOID InitializeAll(GDevice*	pDevice);
	static VVOID DestroyAll();

	// Layout handles
	static InputLayout* PosNormalLayout();
};

#endif