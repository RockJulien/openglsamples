#include "InputLayouts.h"
#include "..\VertexTypes.h"
#include "..\GDevice.h"

// Namespaces used
using namespace Ocelot;

// Static variables initialization
InputLayout* InputLayouts::sPosNormalLayout = NULL;

/******************************************************************************************************/
VVOID InputLayouts::InitializeAll(GDevice* pDevice)
{
	if( InputLayouts::sPosNormalLayout == NULL )
	{
		UBIGINT lStride = sizeof(VoxelVertex);
		InputLayouts::sPosNormalLayout = new InputLayout( pDevice );
		InputLayouts::sPosNormalLayout->AddElement(InputElementDesc("PosL",    0, lStride, 3, GL_FLOAT));
		InputLayouts::sPosNormalLayout->AddElement(InputElementDesc("NormalL", 1, lStride, 3, GL_FLOAT, (UCCHAR*)NULL + (3 * sizeof(FFLOAT))));
	}
}

/******************************************************************************************************/
VVOID InputLayouts::DestroyAll()
{
	DeletePointer( InputLayouts::sPosNormalLayout );
}

/******************************************************************************************************/
InputLayout* InputLayouts::PosNormalLayout()
{
	return InputLayouts::sPosNormalLayout;
}
