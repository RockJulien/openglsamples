
/********************************************************************************************/
inline UCCHAR* GenericImage::Pixels() const
{
	return this->mPixels;
}

/********************************************************************************************/
inline UCCHAR* GenericImage::Map() const
{
	return this->mMap;
}

/********************************************************************************************/
inline UBIGINT GenericImage::Width() const
{
	return this->mWidth;
}

/********************************************************************************************/
inline UBIGINT GenericImage::Height() const
{
	return this->mHeight;
}

/********************************************************************************************/
inline UBIGINT GenericImage::MapEntries() const
{
	return this->mMapEntries;
}

/********************************************************************************************/
inline UBIGINT GenericImage::Format() const
{
	return this->mFormat;
}

/********************************************************************************************/
inline UBIGINT GenericImage::MapFormat() const
{
	return this->mMapFormat;
}

/********************************************************************************************/
inline BIGINT  GenericImage::Components() const
{
	return this->mComponents;
}

/********************************************************************************************/
inline VVOID   GenericImage::SetPixels(UCCHAR* pPixels)
{
	this->mPixels = pPixels;
}

/********************************************************************************************/
inline VVOID   GenericImage::SetMap(UCCHAR* pMap)
{
	this->mMap = pMap;
}

/********************************************************************************************/
inline VVOID   GenericImage::SetWidth(UBIGINT pWidth)
{
	this->mWidth = pWidth;
}

/********************************************************************************************/
inline VVOID   GenericImage::SetHeight(UBIGINT pHeight)
{
	this->mHeight = pHeight;
}

/********************************************************************************************/
inline VVOID   GenericImage::SetMapEntries(UBIGINT pMapEntries)
{
	this->mMapEntries = pMapEntries;
}

/********************************************************************************************/
inline VVOID   GenericImage::SetFormat(UBIGINT pFormat)
{
	this->mFormat = pFormat;
}

/********************************************************************************************/
inline VVOID   GenericImage::SetMapFormat(UBIGINT pMapFormat)
{
	this->mMapFormat = pMapFormat;
}

/********************************************************************************************/
inline VVOID   GenericImage::SetComponents(BIGINT pComponents)
{
	this->mComponents = pComponents;
}
