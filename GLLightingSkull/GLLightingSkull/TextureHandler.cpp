#include "TextureHandler.h"
#include "TGALoader.h"
#include <string>

// Namespaces used
using namespace std;

#define MakeFourCC(ch0, ch1, ch2, ch3)                              \
				  ((ULINT)(UCCHAR)(ch0) | ((ULINT)(UCCHAR)(ch1) << 8) |   \
                  ((ULINT)(UCCHAR)(ch2) << 16) | ((ULINT)(UCCHAR)(ch3) << 24 ))

#define FOURCC_DXT1	MakeFourCC('D', 'X', 'T', '1')
#define FOURCC_DXT3	MakeFourCC('D', 'X', 'T', '3')
#define FOURCC_DXT5	MakeFourCC('D', 'X', 'T', '5')

/****************************************************************************************************/
BIGINT TextureHandler::LoadBMP(GDevice* device, const CCHAR* textName, BBOOL wrap)
{
	return -1;
}

/****************************************************************************************************/
BIGINT TextureHandler::LoadPNG(GDevice* device, const CCHAR* textName, BBOOL wrap)
{
	return -1;
}

/****************************************************************************************************/
BIGINT TextureHandler::LoadJPG(GDevice* device, const CCHAR* textName, BBOOL wrap)
{
	return -1;
}

/****************************************************************************************************/
BIGINT TextureHandler::LoadDDS(GDevice* device, const CCHAR* pFilename, BBOOL wrap)
{
	UBIGINT lTextureId;

	// Check if a name is passed.
	if(pFilename == NULL)
	{
		return -1;
	}

	string lFileName = pFilename;
	string lFilePath; 
	// Extra pre-process: Handle a missing extension (add the file extension).
	if(lFileName.find(".dds") == string::npos)
	{
		/*strcat_s(pFilename, 256, this->mExtension);*/
		lFilePath = lFileName + ".dds";
	}
	else
	{
		lFilePath = pFilename;
	}

	// Start the extraction
	FILE* lInput = NULL;
	UCCHAR lFileHeader[124];
	CCHAR lDdsChecker[4];
	BIGINT lError = 0;
	UBIGINT lBufferSize = 0, lWidth = 0, lHeight = 0, lFourCC = 0;
	UBIGINT lLinearSize = 0, lMipMapCount = 0, lComponents = 0;
	UCCHAR* lImage = NULL;

	// Open the texture file (read binary mode).
	lError = fopen_s(&lInput, lFilePath.c_str(), "rb");
	if(lError != 0)
	{
		return -1;
	}

	// Verify the file is a true .dds file
	fread(lDdsChecker, 1, 4, lInput);
	if(strncmp(lDdsChecker, "DDS ", 4) != 0)
	{
		// Not a .dds
		lError = fclose(lInput);
		return -1;
	}

	// Grab the surface descriptor.
	fread(&lFileHeader, 124, 1, lInput);

	// Get the important information from the header.
	lWidth       = *(UBIGINT*)&(lFileHeader[12]);
	lHeight      = *(UBIGINT*)&(lFileHeader[8]);
	lLinearSize  = *(UBIGINT*)&(lFileHeader[16]);
	lMipMapCount = *(UBIGINT*)&(lFileHeader[24]);
	lFourCC      = *(UBIGINT*)&(lFileHeader[80]);

	lBufferSize  = lMipMapCount > 1 ? lLinearSize * 2 : lLinearSize;

	// The DDS loader will only support DXT1, DXT3 and DXT5
	// and we need to convert the fourCC flag value in such
	// a way that OpenGL understand it.
	lComponents = (lFourCC == FOURCC_DXT1) ? 3 : 4;

	// Allocate memory for the image data.
	// Allocate memory for the targa image data.
	lImage = new UCCHAR[lBufferSize];
	if(!lImage)
	{
		lError = fclose(lInput);
		return -1;
	}

	// Read the data into the buffer.
	fread(lImage, 1, lBufferSize, lInput);

	// Close the file
	lError = fclose(lInput);
	if(lError != 0)
	{
		DeletePointer( lImage );
		return -1;
	}

	// Generate an ID for the texture.
	glGenTextures(1, &lTextureId);

	// Bind the texture as a 2D texture.
	glBindTexture(GL_TEXTURE_2D, lTextureId);

	// Set the texture color to either wrap around or clamp to the edge.
	if(wrap)
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}
	else
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	}

	// Set the texture filtering.
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	// Missing mipmaps will not be a prob anymore.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, lMipMapCount - 1);

	USMALLINT glFormat = GL_RGBA8;

	// It will generate mip maps as it is decompressing
	if(lMipMapCount != 0)
	{
		// Load the image data into the texture unit,
		// and Generate mipmaps for the texture.
		BIGINT iBlockSize  = 0, iOffset = 0;
		UBIGINT tempWidth  = lWidth;
		UBIGINT tempHeight = lHeight;
		iBlockSize = (glFormat == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
		
		for(UBIGINT currMipMap = 0; currMipMap < lMipMapCount && (tempWidth || tempHeight); currMipMap++)
		{
			BIGINT iSize = ((tempWidth + 3) / 4) * ((tempHeight + 3) / 4) * iBlockSize;

			// Create the current texture at the current resolution.
			device->glCompressTextImg2D(GL_TEXTURE_2D,
										currMipMap,
										glFormat,
										tempWidth,
										tempHeight,
										0,
										iSize,
										&lImage[iOffset]);

			iOffset += iSize;
			// Half the image size for the next MipMap.
			if((tempWidth  /= 2) == 0) tempWidth  = 1;
			if((tempHeight /= 2) == 0) tempHeight = 1;
		} // end for
	} // end if
	else
	{
		DeletePointer( lImage );
		return -1;
	}

	DeletePointer( lImage );
	return lTextureId;
}

/****************************************************************************************************/
BIGINT TextureHandler::LoadTGA(GDevice* device, const CCHAR* textName, BBOOL wrap)
{
	UBIGINT lTextureId;

	TGALoader lLoader;
	GenericImage* lImage = lLoader.ReadTGA( textName );

	// Generate an ID for the texture.
	glGenTextures(1, &lTextureId);

	// Bind the texture as a 2D texture.
	glBindTexture(GL_TEXTURE_2D, lTextureId);

	// Load the image data into the texture unit.
	glTexImage2D( GL_TEXTURE_2D, 
				  0, 
				  lImage->Components() == 4 ? GL_RGBA8 : GL_RGB8, 
				  lImage->Width(), 
				  lImage->Height(), 
				  0, 
				  lImage->Components() == 4 ? GL_RGBA : GL_RGB, 
				  GL_UNSIGNED_BYTE, 
				  lImage->Pixels() );

	// Set the texture color to either wrap around or clamp to the edge.
	if(wrap)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	}

	// Set the texture filtering.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	// Generate mipmaps for the texture.
	device->glGenerateMipMap(GL_TEXTURE_2D);

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Random vector texture Failure !!! ");
	}

	// Release the targa image data.
	lImage->Release();
	DeletePointer( lImage );

	return lTextureId;
}

/****************************************************************************************************/
TextureHandler::TextureHandler(GDevice* pDevice) :
mDevice(pDevice)
{
	
}

/****************************************************************************************************/
TextureHandler::~TextureHandler()
{

}

/****************************************************************************************************/
BIGINT TextureHandler::Load( const CCHAR* textName, ExtType type, BBOOL wrap)
{
	BIGINT lId = -1;

	switch (type)
	{
	case BMP:
		{
			lId = LoadBMP(this->mDevice, textName, wrap);
		}
		break;
	case PNG:
		{
			lId = LoadPNG(this->mDevice, textName, wrap);
		}
		break;
	case JPG:
		{
			lId = LoadJPG(this->mDevice, textName, wrap);
		}
		break;
	case DDS:
		{
			lId = LoadDDS(this->mDevice, textName, wrap);
		}
		break;
	case TGA:
		{
			lId = LoadTGA(this->mDevice, textName, wrap);
		}
		break;
	default:
		return -1;
		break;
	}

	return lId;
}
