#ifndef DEF_TEXTUREEXTYPE_H
#define DEF_TEXTUREEXTYPE_H

#include "OSCheck.h"

enum ExtType
{
	BMP,
	PNG,
	JPG,
	DDS,
	TGA
};

struct RLEState
{
	UCCHAR* StateBuf;
	BIGINT  StateLen;
	BIGINT  LastState;
};

struct TGAHeader
{
	UCCHAR IdLength;
    UCCHAR ColorMapType;

    /* The image type. */
#define TGA_TYPE_MAPPED 1
#define TGA_TYPE_COLOR 2
#define TGA_TYPE_GRAY 3
#define TGA_TYPE_MAPPED_RLE 9
#define TGA_TYPE_COLOR_RLE 10
#define TGA_TYPE_GRAY_RLE 11
	UCCHAR ImageType;

	/* Color Map Specification. */
	/* We need to separately specify high and low bytes to avoid endianness
		and alignment problems. */
	UCCHAR ColorMapIndexLSB;
	UCCHAR ColorMapIndexMSB;
	UCCHAR ColorMapLengthLSB;
	UCCHAR ColorMapLengthMSB;
	UCCHAR ColorMapSize;

	/* Image Specification. */
	UCCHAR XOriginLSB;
	UCCHAR XOriginMSB;
	UCCHAR YOriginLSB;
	UCCHAR YOriginMSB;

	UCCHAR WidthLSB;
	UCCHAR WidthMSB;
	UCCHAR HeightLBS;
	UCCHAR HeightMSB;

	UCCHAR Bpp;

	/* Image descriptor.
		3-0: attribute bpp
		4:   left-to-right ordering
		5:   top-to-bottom ordering
		7-6: zero
     */
#define TGA_DESC_ABITS 0x0f
#define TGA_DESC_HORIZONTAL 0x10
#define TGA_DESC_VERTICAL 0x20
	UCCHAR Descriptor;
};

struct TGAFooter
{
	UBIGINT ExtensionAreaOffset;
	UBIGINT DeveloperDirectoryOffset;

#define TGA_SIGNATURE "TRUEVISION-XFILE"

	CCHAR Signature[16];
	CCHAR Dot;
	CCHAR Null;
};

#endif