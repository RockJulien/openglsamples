#ifndef DEF_BASICMODELINSTANCE_H
#define DEF_BASICMODELINSTANCE_H

// Includes
#include "BasicModel.h"
#include "..\Maths\Matrix4x4.h"

// Class definition
class BasicModelInstance
{
public:

	// Constructor
	BasicModelInstance();
	~BasicModelInstance();

	// Attributes
	Matrix4x4   World;
	BasicModel* Model;

};

#endif