#ifndef DEF_VECTOR3_H
#define DEF_VECTOR3_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   Vector3.h/.cpp
/
/ Description: This file provide a way to create a 3D vector with its
/              basic contents Dot, Cross products and extras such as
/              four interpolation function between vectors and product
/              with matrixes as well.
/              It will serve especially for 3D Games as point for position
/              or direction vector for lights for example.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "Utility.h"
#include "Matrix3x3.h"
#include "Matrix4x4.h"
#include "Vector4.h"

class Matrix3x3;
class Matrix4x4;
class Vector4;

class Vector3
{
private:

	// Attributes
	float m_fX;
	float m_fY;
	float m_fZ;

public:

	// Constructors & Destructor
	Vector3();								 // default cst
	Vector3(const Vector3&);                 // copy cst
	Vector3& operator = (const Vector3&);    // assignment cst
	Vector3(float xyz);                      // overload cst
	Vector3(double xyz);                     // overload cst
	Vector3(float  x, float  y, float  z);   // overload cst
	Vector3(double x, double y, double z);   // overload cst
	~Vector3();

	// Methods
	void	Clear();
	float   Vec3Length();
	float   Vec3LengthSquared();
	float   Vec3DistanceBetween(const Vector3& target);
	Vector3 Vec3Normalise();
	float   Vec3DotProduct(const Vector3& vec);
	Vector3 Vec3CrossProduct(const Vector3& adjVec2);
	Vector3 Vec3Addition(const Vector3& vecToAdd);
	Vector3 Vec3Subtraction(const Vector3& vecToSub);
	Vector3 Vec3LinearInterpolation(const Vector3& vecToJoin, float var);
	Vector3 Vec3HermiteSplineInterpolation(const Vector3& tangStartPoint, const Vector3& vecToJoin, const Vector3& tangVecToJoin, float var);
	Vector3 Vec3CatmullSplineInterpolation(const Vector3& vecControl1, const Vector3& vecToJoin, const Vector3& vecControl2, float var);
	Vector3 Vec3BarycentricInterpolation(const Vector3& vecAtBar, const Vector3& vecToJoin, float baryVar1, float baryVar2);
	Vector3 Vec3VecMatrixProduct(const Matrix3x3& mat);
	Vector4 Vec3VecMatrixProduct(const Matrix4x4& mat);
	Vector4 Vec3VecMatrixTransformNormal(const Matrix4x4& mat);

	// Accessors
	inline float getX() const { return m_fX; }
	inline float getY() const { return m_fY; }
	inline float getZ() const { return m_fZ; }
	inline void  x(const float X) { m_fX = X; }
	inline void  y(const float Y) { m_fY = Y; }
	inline void  z(const float Z) { m_fZ = Z; }
	inline void  set(const int index, const float value) { if(index == 0) { m_fX = value; } else if(index == 1) { m_fY = value; } else if(index == 2) { m_fZ = value; }  }
	// operators overload
	/*********************Cast Operators**************************/
	operator float* ();              // for being able to pass it through the shader as a (float*)
	operator const float* () const;  // constant version
	/*********************** unary operators **********************/
	Vector3  operator + () const;
	Vector3  operator - () const;
	/*********************** assgm operators **********************/
	Vector3& operator += (const Vector3&);
	Vector3& operator -= (const Vector3&);
	Vector3& operator *= (const float& val);
	Vector3& operator /= (const float& val);
	/*********************** binary operators *********************/
	Vector3  operator +  (const Vector3&) const;
	Vector3  operator -  (const Vector3&) const;
	Vector3  operator *  (const float& val) const;
	Vector3  operator /  (const float& val) const;
	bool     operator == (const Vector3&) const;
	bool     operator != (const Vector3&) const;
	float    operator [] (const int index);
	friend Vector3 operator * (float, const Vector3&);
};

#endif