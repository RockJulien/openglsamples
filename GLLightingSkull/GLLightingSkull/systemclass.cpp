////////////////////////////////////////////////////////////////////////////////
// Filename: SystemClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "systemclass.h"

#define GET_X_LPARAM(lp) ((int)(short)LOWORD(lp))
#define GET_Y_LPARAM(lp) ((int)(short)HIWORD(lp))

// Static variables initialization
FFLOAT SystemClass::sTotalTime = 0.f;

/****************************************************************************************************/
SystemClass::SystemClass()
{
	this->mLastMouseX = 0;
	this->mLastMouseY = 0;

	this->mOpenGL   = NULL;
	this->mGraphics = NULL;
}

/****************************************************************************************************/
SystemClass::~SystemClass()
{

}

/****************************************************************************************************/
BBOOL SystemClass::Initialize()
{
	BIGINT lScreenWidth, lScreenHeight;
	BBOOL lResult;

	// Initialize the width and height of the screen to zero.
	lScreenWidth  = 0;
	lScreenHeight = 0;

	GLenum lError = glGetError();

	// Create the OpenGL object.
	this->mOpenGL = new OpenGLClass;
	if( this->mOpenGL == NULL )
	{
		return false;
	}

	// Create the window the application will be using and also initialize OpenGL.
	lResult = this->InitializeWindows( this->mOpenGL, lScreenWidth, lScreenHeight );
	if( lResult == false )
	{
		MessageBox( this->mHwnd, L"Could not initialize the window.", L"Error", MB_OK );
		return false;
	}
	
	// Create the graphics object.  This object will handle rendering all the graphics for this application.
	this->mGraphics = new GraphicsClass;
	if( this->mGraphics == NULL )
	{
		return false;
	}

	// Initialize the graphics object.
	lResult = this->mGraphics->Initialize( this->mOpenGL, this->mClientWidth, this->mClientHeight );
	if( lResult == false )
	{
		return false;
	}

	return true;
}

/****************************************************************************************************/
VVOID SystemClass::OnResize()
{
	if( this->mGraphics != NULL )
	{
		this->mGraphics->OnResize( this->mClientWidth, this->mClientHeight );
	}
}

/****************************************************************************************************/
VVOID SystemClass::Shutdown()
{
	// Release the graphics object.
	if( this->mGraphics )
	{
		this->mGraphics->Shutdown();
		DeletePointer( this->mGraphics );
	}

	// Release the OpenGL object.
	if( this->mOpenGL )
	{
		this->mOpenGL->Release( this->mHwnd );
		DeletePointer( this->mOpenGL );
	}

	// Shutdown the window.
	this->ShutdownWindows();
	
	return;
}

/****************************************************************************************************/
VVOID SystemClass::Run()
{
	MSG lMsg;
	BBOOL lDone, lResult;

	this->mTimer.resetTimer();

	// Initialize the message structure.
	ZeroMemory( &lMsg, sizeof(MSG) );
	
	// Loop until there is a quit message from the window or the user.
	lDone = false;
	while( lDone == false )
	{
		// Handle the windows messages.
		if( PeekMessage( &lMsg, NULL, 0, 0, PM_REMOVE ) )
		{
			TranslateMessage(&lMsg);
			DispatchMessage(&lMsg);
		}

		// If windows signals to end the application then exit out.
		if( lMsg.message == WM_QUIT )
		{
			lDone = true;
		}
		else
		{
			this->mTimer.tick();
			sTotalTime = this->mTimer.getGameTime();

			if( this->mAppPaused == false )
			{
				// Otherwise do the frame processing.
				lResult = this->Update( this->mTimer.getDeltaTime() );
				if( lResult == false )
				{
					lDone = true;
				}
			}
			else
			{
				Sleep( 100 );
			}
		}

	}

	return;
}

/****************************************************************************************************/
BBOOL SystemClass::Update(FFLOAT pDeltaTime)
{
	if( this->mGraphics->Update( pDeltaTime ) )
	{
		return this->mGraphics->Render();
	}

	return false;
}

/****************************************************************************************************/
LRESULT CALLBACK SystemClass::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	switch(umsg)
	{
		case WM_ACTIVATE:
		{
			if( LOWORD(wparam) == WA_INACTIVE )
			{
				this->mAppPaused = true;
				this->mTimer.stopTimer();
			}
			else
			{
				this->mAppPaused = false;
				this->mTimer.startTimer();
			}
			return 0;
		}
		case WM_SIZE:
		{
			// Save the new client area dimensions.
			this->mClientWidth  = LOWORD(lparam);
			this->mClientHeight = HIWORD(lparam);
			if( this->mOpenGL )
			{
				if( wparam == SIZE_MINIMIZED )
				{
					this->mAppPaused = true;
					this->mMinimized = true;
					this->mMaximized = false;
				}
				else if( wparam == SIZE_MAXIMIZED )
				{
					this->mAppPaused = false;
					this->mMinimized = false;
					this->mMaximized = true;
					this->OnResize();
				}
				else if( wparam == SIZE_RESTORED )
				{
				
					// Restoring from minimized state?
					if( this->mMinimized )
					{
						this->mAppPaused = false;
						this->mMinimized = false;
						this->OnResize();
					}

					// Restoring from maximized state?
					else if( this->mMaximized )
					{
						this->mAppPaused = false;
						this->mMaximized = false;
						this->OnResize();
					}
					else if( this->mResizing )
					{
						// If user is dragging the resize bars, we do not resize 
						// the buffers here because as the user continuously 
						// drags the resize bars, a stream of WM_SIZE messages are
						// sent to the window, and it would be pointless (and slow)
						// to resize for each WM_SIZE message received from dragging
						// the resize bars.  So instead, we reset after the user is 
						// done resizing the window and releases the resize bars, which 
						// sends a WM_EXITSIZEMOVE message.
					}
					else // API call such as SetWindowPos or mSwapChain->SetFullscreenState.
					{
						this->OnResize();
					}
				}
			}
			return 0;
		}
		break;
		case WM_ENTERSIZEMOVE:
		{
			this->mAppPaused = true;
			this->mResizing  = true;
			this->mTimer.stopTimer();
			return 0;
		}
		break;
		case WM_EXITSIZEMOVE:
		{
			this->mAppPaused = false;
			this->mResizing  = false;
			this->mTimer.startTimer();
			this->OnResize();
			return 0;
		}
		break;
		/*case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}
		break;*/
		// Catch this message so to prevent the window from becoming too small.
		case WM_GETMINMAXINFO:
		{
			((MINMAXINFO*)lparam)->ptMinTrackSize.x = 200;
			((MINMAXINFO*)lparam)->ptMinTrackSize.y = 200; 
			return 0;
		}
		break;
		case WM_LBUTTONDOWN:
		{
			this->OnMouseDown( wparam, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
			return 0;
		}
		break;
		case WM_MBUTTONDOWN:
		{
			this->OnMouseDown( wparam, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
			return 0;
		}
		break;
		case WM_RBUTTONDOWN:
		{
			this->OnMouseDown( wparam, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
			return 0;
		}
		break;
		case WM_LBUTTONUP:
		{
			this->OnMouseUp( wparam, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam) );
			return 0;
		}
		break;
		case WM_MBUTTONUP:
		{
			this->OnMouseUp( wparam, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam) );
			return 0;
		}
		break;
		case WM_RBUTTONUP:
		{
			this->OnMouseUp( wparam, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam) );
			return 0;
		}
		break;
		case WM_MOUSEMOVE:
		{
			this->OnMouseMove( wparam, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam) );
			return 0;
		}
		break;
		// Check if a key has been released on the keyboard.
		case WM_KEYUP:
		{
			// If a key is released then send it to the input object so it can unset the state for that key.
			this->OnKeyUp( wparam );
			return 0;
		}
		break;
		// Check if a key has been pressed on the keyboard.
		case WM_KEYDOWN:
		{
			// If a key is pressed send it to the input object so it can record that state.
			this->OnKeyDown( wparam );
			return 0;
		}
		break;
	}

	// Any other messages send to the default message handler as our application won't make use of them.
	return DefWindowProc( hwnd, umsg, wparam, lparam );
}

/****************************************************************************************************/
VVOID SystemClass::OnMouseDown(WPARAM pButtonState, BIGINT pX, BIGINT pY)
{
	this->mLastMouseX = pX;
	this->mLastMouseY = pY;

	SetCapture( this->mHwnd );
}

/****************************************************************************************************/
VVOID SystemClass::OnMouseUp(WPARAM pButtonState, BIGINT pX, BIGINT pY)
{
	ReleaseCapture();
}

/****************************************************************************************************/
VVOID SystemClass::OnMouseMove(WPARAM pButtonState, BIGINT pX, BIGINT pY)
{
	if( (pButtonState & MK_LBUTTON) == MK_LBUTTON )
	{
		FFLOAT lToRad = PI / 180.0f;
		// Make each pixel correspond to a quarter of a degree.
		FFLOAT lDx = ( 0.25f * static_cast<FFLOAT>( pX - this->mLastMouseX ) ) * lToRad;
		FFLOAT lDy = ( 0.25f * static_cast<FFLOAT>( pY - this->mLastMouseY ) ) * lToRad;

		if( this->mGraphics != NULL &&
			this->mGraphics->Controller() != NULL )
		{
			this->mGraphics->Controller()->pitch( lDy );
			this->mGraphics->Controller()->yaw( lDx );
		}
	}

	this->mLastMouseX = pX;
	this->mLastMouseY = pY;
}

/****************************************************************************************************/
VVOID SystemClass::OnKeyDown(WPARAM pButtonState)
{
	if( (pButtonState & VK_ESCAPE) == VK_ESCAPE )
	{
		PostQuitMessage( 0 );
	}

	if( pButtonState == VK_UP )
	{
		this->mGraphics->Controller()->move( Direction::Forward );
	}

	if( pButtonState == VK_DOWN )
	{
		this->mGraphics->Controller()->move( Direction::Backward );
	}

	if( pButtonState == VK_LEFT )
	{
		this->mGraphics->Controller()->move( Direction::LeftSide );
	}

	if( pButtonState == VK_RIGHT )
	{
		this->mGraphics->Controller()->move( Direction::RightSide );
	}

	if( pButtonState == VK_NUMPAD0 )
	{
		this->mGraphics->SetLightCount( 0 );
	}

	if( pButtonState == VK_NUMPAD1 )
	{
		this->mGraphics->SetLightCount( 1 );
	}

	if( pButtonState == VK_NUMPAD2 )
	{
		this->mGraphics->SetLightCount( 2 );
	}

	if( pButtonState == VK_NUMPAD3 )
	{
		this->mGraphics->SetLightCount( 3 );
	}
}

/****************************************************************************************************/
VVOID SystemClass::OnKeyUp(WPARAM pButtonState)
{
	
}

/****************************************************************************************************/
BBOOL SystemClass::InitializeWindows(OpenGLClass* OpenGL, int& screenWidth, int& screenHeight)
{
	WNDCLASSEX lWc;
	DEVMODE lDmScreenSettings;
	BIGINT lPosX, lPosY;
	BBOOL lResult;

	this->mClientWidth  = screenWidth;
	this->mClientHeight = screenHeight;

	GLenum lError = glGetError();

	// Get an external pointer to this object.	
	ApplicationHandle = this;

	// Get the instance of this application.
	this->mHinstance = GetModuleHandle( NULL );

	// Give the application a name.
	this->mApplicationName = L"Engine";

	// Setup the windows class with default settings.
	lWc.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	lWc.lpfnWndProc   = WndProc;
	lWc.cbClsExtra    = 0;
	lWc.cbWndExtra    = 0;
	lWc.hInstance     = this->mHinstance;
	lWc.hIcon		  = LoadIcon(NULL, IDI_WINLOGO);
	lWc.hIconSm       = lWc.hIcon;
	lWc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	lWc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	lWc.lpszMenuName  = NULL;
	lWc.lpszClassName = this->mApplicationName;
	lWc.cbSize        = sizeof(WNDCLASSEX);
	
	// Register the window class.
	RegisterClassEx( &lWc );

	// Create a temporary window for the OpenGL extension setup.
	this->mHwnd = CreateWindowEx( WS_EX_APPWINDOW, this->mApplicationName, this->mApplicationName, WS_POPUP,
						          0, 0, 640, 480, NULL, NULL, this->mHinstance, NULL );
	if( this->mHwnd == NULL )
	{
		return false;
	}

	// Don't show the window.
	ShowWindow( this->mHwnd, SW_HIDE );

	// Initialize a temporary OpenGL window and load the OpenGL extensions.
	lResult = OpenGL->InitializeExtensions( this->mHwnd );
	if( lResult == false )
	{
		MessageBox( this->mHwnd, L"Could not initialize the OpenGL extensions.", L"Error", MB_OK );
		return false;
	}

	lError = glGetError();

	// Release the temporary window now that the extensions have been initialized.
	DestroyWindow( this->mHwnd );
	this->mHwnd = NULL;

	// Determine the resolution of the clients desktop screen.
	screenWidth  = GetSystemMetrics(SM_CXSCREEN);
	screenHeight = GetSystemMetrics(SM_CYSCREEN);

	// Setup the screen settings depending on whether it is running in full screen or in windowed mode.
	if(FULL_SCREEN)
	{
		// If full screen set the screen to maximum size of the users desktop and 32bit.
		memset(&lDmScreenSettings, 0, sizeof(lDmScreenSettings));
		lDmScreenSettings.dmSize       = sizeof(lDmScreenSettings);
		lDmScreenSettings.dmPelsWidth  = (unsigned long)screenWidth;
		lDmScreenSettings.dmPelsHeight = (unsigned long)screenHeight;
		lDmScreenSettings.dmBitsPerPel = 32;			
		lDmScreenSettings.dmFields     = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings( &lDmScreenSettings, CDS_FULLSCREEN );

		// Set the position of the window to the top left corner.
		lPosX = lPosY = 0;
	}
	else
	{
		// If windowed then set it to 800x600 resolution.
		screenWidth  = 800;
		screenHeight = 600;

		// Place the window in the middle of the screen.
		lPosX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth)  / 2;
		lPosY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
	}

	// Create the window with the screen settings and get the handle to it.
	this->mHwnd = CreateWindowEx( WS_EX_APPWINDOW, this->mApplicationName, this->mApplicationName, WS_POPUP,
						          lPosX, lPosY, screenWidth, screenHeight, NULL, NULL, this->mHinstance, NULL );
	if( this->mHwnd == NULL )
	{
		return false;
	}

	// Initialize OpenGL now that the window has been created.
	lResult = this->mOpenGL->InitializeDevice( this->mHwnd, screenWidth, screenHeight, SCREEN_DEPTH, SCREEN_NEAR, VSYNC_ENABLED );
	if( lResult == false )
	{
		MessageBox( this->mHwnd, L"Could not initialize OpenGL, check if video card supports OpenGL 4.0.", L"Error", MB_OK );
		return false;
	}

	// Bring the window up on the screen and set it as main focus.
	ShowWindow( this->mHwnd, SW_SHOW );
	SetForegroundWindow( this->mHwnd );
	SetFocus( this->mHwnd );

	// Hide the mouse cursor.
	//ShowCursor( false );

	return true;
}

/****************************************************************************************************/
VVOID SystemClass::ShutdownWindows()
{
	// Show the mouse cursor.
	ShowCursor(true);

	// Fix the display settings if leaving full screen mode.
	if( FULL_SCREEN )
	{
		ChangeDisplaySettings( NULL, 0 );
	}

	// Remove the window.
	DestroyWindow( this->mHwnd );
	this->mHwnd = NULL;

	// Remove the application instance.
	UnregisterClass( this->mApplicationName, this->mHinstance );
	this->mHinstance = NULL;

	// Release the pointer to this class.
	ApplicationHandle = NULL;

	return;
}

/****************************************************************************************************/
LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
{
	switch( umessage )
	{
		// Check if the window is being closed.
		case WM_CLOSE:
		{
			PostQuitMessage( 0 );		
			return 0;
		}

		// All other messages pass to the message handler in the system class.
		default:
		{
			return ApplicationHandle->MessageHandler( hwnd, umessage, wparam, lparam );
		}
	}
}

/****************************************************************************************************/
FFLOAT SystemClass::GetTotalTime()
{
	return sTotalTime;
}
