////////////////////////////////////////////////////////////////////////////////
// Filename: SsaoNormalDepth.vs
////////////////////////////////////////////////////////////////////////////////

/////////////////////
// INPUT VARIABLES //
/////////////////////
in vec3 PosL;		// Local position
in vec2 TexIn;		// Texture coordinates
in vec3 NormalL;    // Local normal

//////////////////////
// OUTPUT VARIABLES //
//////////////////////
out vec3 PosV;		 // View space position
out vec3 NormalV;    // View space normal
out vec2 TexOut;	 // Texture coordinates.

///////////////////////
// UNIFORM VARIABLES //
///////////////////////
uniform mat4 gWorldView;
uniform mat4 gWorldInvTransposeView;
uniform mat4 gWorldViewProj;
uniform mat4 gTexTransform;

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader Stage
////////////////////////////////////////////////////////////////////////////////
void main(void)
{
	// Transform to view space.
	PosV    = ( gWorldView * vec4( PosL, 1.0f ) ).xyz;
	NormalV = mat3( gWorldInvTransposeView ) * NormalL;
		
	// Transform to homogeneous clip space.
	gl_Position = gWorldViewProj * vec4( PosL, 1.0f );
	
	// Output vertex attributes for interpolation across triangle.
	TexOut = ( gTexTransform * vec4( TexIn, 0.0f, 1.0f ) ).xy;
}
