////////////////////////////////////////////////////////////////////////////////
// Filename: SkinnedNormalMap.vs
////////////////////////////////////////////////////////////////////////////////

/////////////////////
// INPUT VARIABLES //
/////////////////////
in vec3  PosL;		// Local position
in vec3  NormalL;	// Local normal
in vec4  TangentL;	// Local tangent
in vec2  TexIn;     // Texture coordinates.
in vec3  Weights;   // Bone weights
in uvec4 BoneIndices; // Bone indices.

//////////////////////
// OUTPUT VARIABLES //
//////////////////////
out vec3 PosW;		 // World position
out vec3 NormalW;    // World normal
out vec4 TangentW;   // World tangent
out vec2 TexOut;	 // Texture coordinates.
out vec4 ShadowPosH; // Shadow map homogeneous coordinates.
out vec4 SsaoPosH;   // Screen space ambient occlusion position homogeneous coordinates

///////////////////////
// UNIFORM VARIABLES //
///////////////////////
uniform mat4 gBoneTransforms[96];
uniform mat4 gWorld;
uniform mat4 gWorldInvTranspose;
uniform mat4 gWorldViewProj;
uniform mat4 gWorldViewProjTex;
uniform mat4 gTexTransform;
uniform mat4 gShadowTransform;

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader Stage
////////////////////////////////////////////////////////////////////////////////
void main(void)
{
	float lWeights[4] = { 0.0, 0.0, 0.0, 0.0 };
	lWeights[0] = Weights.x;
	lWeights[1] = Weights.y;
	lWeights[2] = Weights.z;
	lWeights[3] = 1.0 - lWeights[0] - lWeights[1] - lWeights[2];

	vec3 lPosL     = vec3( 0.0, 0.0, 0.0 );
	vec3 lNormalL  = vec3( 0.0, 0.0, 0.0 );
	vec3 lTangentL = vec3( 0.0, 0.0, 0.0 );
	for( int i = 0; i < 4; i++ )
	{
	    // Assume no nonuniform scaling when transforming normals, so 
		// that we do not have to use the inverse-transpose.

	    lPosL     += lWeights[ i ] * vec4( gBoneTransforms[ BoneIndices[ i ] ] * vec4( PosL, 1.0f ) ).xyz;
		lNormalL  += lWeights[ i ] * mat3( gBoneTransforms[ BoneIndices[ i ] ] ) * NormalL;
		lTangentL += lWeights[ i ] * mat3( gBoneTransforms[ BoneIndices[ i ] ] ) * TangentL.xyz;
	}
 
	// Transform to world space space.
	PosW     = vec4( gWorld * vec4( lPosL, 1.0 ) ).xyz;
	NormalW  = mat3( gWorldInvTranspose ) * lNormalL;
	TangentW = vec4( mat3( gWorld ) * lTangentL, TangentL.w );

	// Transform to homogeneous clip space.
	gl_Position = gWorldViewProj * vec4( lPosL, 1.0 );
	
	// Output vertex attributes for interpolation across triangle.
	TexOut = vec4( gTexTransform * vec4( TexIn, 0.0, 1.0 ) ).xy;

	// Generate projective tex-coords to project shadow map onto scene.
	ShadowPosH = gShadowTransform * vec4( lPosL, 1.0 );

	// Generate projective tex-coords to project SSAO map onto scene.
	SsaoPosH = gWorldViewProjTex * vec4( lPosL, 1.0 );
}
