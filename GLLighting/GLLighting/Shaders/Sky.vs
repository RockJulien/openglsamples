////////////////////////////////////////////////////////////////////////////////
// Filename: Sky.vs
////////////////////////////////////////////////////////////////////////////////

/////////////////////
// INPUT VARIABLES //
/////////////////////
in vec3 PosL;		// Local position

//////////////////////
// OUTPUT VARIABLES //
//////////////////////
out vec3 Tex;	   // Texture coordinates

///////////////////////
// UNIFORM VARIABLES //
///////////////////////
uniform mat4 gWorldViewProj;

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader Stage
////////////////////////////////////////////////////////////////////////////////
void main(void)
{
	// Set z = w so that z/w = 1 (i.e., skydome always on far plane).
	gl_Position = vec4( gWorldViewProj * vec4( PosL, 1.0 ) ).xyww;
	
	// Use local vertex position as cubemap lookup vector.
	Tex = PosL;
}
