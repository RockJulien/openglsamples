////////////////////////////////////////////////////////////////////////////////
// Filename: NormalMap.vs
////////////////////////////////////////////////////////////////////////////////

/////////////////////
// INPUT VARIABLES //
/////////////////////
in vec3 PosL;		// Local position
in vec3 NormalL;    // Local normal
in vec4 TangentL;   // Local tangent
in vec2 TexIn;		// Texture coordinates

//////////////////////
// OUTPUT VARIABLES //
//////////////////////
out vec3 PosW;		 // World position
out vec3 NormalW;    // World normal
out vec4 TangentW;   // World tangent
out vec2 TexOut;	 // Texture coordinates.
out vec4 ShadowPosH; // Shadow map homogeneous coordinates.
out vec4 SsaoPosH;   // Screen space ambient occlusion position homogeneous coordinates

///////////////////////
// UNIFORM VARIABLES //
///////////////////////
uniform mat4 gWorld;
uniform mat4 gWorldInvTranspose;
uniform mat4 gWorldViewProj;
uniform mat4 gWorldViewProjTex;
uniform mat4 gTexTransform;
uniform mat4 gShadowTransform;

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader Stage
////////////////////////////////////////////////////////////////////////////////
void main(void)
{
	// Transform to world space space.
	PosW     = vec4( gWorld * vec4( PosL, 1.0 ) ).xyz;
	NormalW  = mat3( gWorldInvTranspose ) * NormalL;
	TangentW = gWorld * TangentL;

	// Transform to homogeneous clip space.
	gl_Position = gWorldViewProj * vec4( PosL, 1.0 );
	
	// Output vertex attributes for interpolation across triangle.
	TexOut = vec4( gTexTransform * vec4( TexIn, 0.0, 1.0 ) ).xy;

	// Generate projective tex-coords to project shadow map onto scene.
	ShadowPosH = gShadowTransform * vec4( PosL, 1.0 );

	// Generate projective tex-coords to project SSAO map onto scene.
	SsaoPosH = gWorldViewProjTex * vec4( PosL, 1.0 );
}
