////////////////////////////////////////////////////////////////////////////////
// Filename: Basic.vs
////////////////////////////////////////////////////////////////////////////////

/////////////////////
// INPUT VARIABLES //
/////////////////////
in vec3 PosL;		// Local position
in vec2 TexIn;		// Texture coordinates
in vec3 NormalL;    // Local normal

//////////////////////
// OUTPUT VARIABLES //
//////////////////////
out vec3 PosW;		 // World position
out vec3 NormalW;    // World normal
out vec2 TexOut;	 // Texture coordinates.
out vec4 ShadowPosH; // Shadow map homogeneous coordinates.
out vec4 SsaoPosH;   // Screen space ambient occlusion position homogeneous coordinates

///////////////////////
// UNIFORM VARIABLES //
///////////////////////
uniform mat4 gWorld;
uniform mat4 gWorldInvTranspose;
uniform mat4 gWorldViewProj;
uniform mat4 gWorldViewProjTex;
uniform mat4 gTexTransform;
uniform mat4 gShadowTransform;

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader Stage
////////////////////////////////////////////////////////////////////////////////
void main(void)
{
	// Calculate the position of the vertex against the world, view, and projection matrices.
	PosW = vec4( gWorld * vec4( PosL, 1.0 ) ).xyz;
	gl_Position = gWorldViewProj * vec4( PosL, 1.0 );

	// Store the texture coordinates for the pixel shader.
	TexOut = vec4( gTexTransform * vec4( TexIn, 0.0, 1.0 ) ).xy;

	// Calculate the normal vector with the local one against the world matrix inverse transpose.
	NormalW = mat3( gWorldInvTranspose ) * NormalL;

	// Generate projective tex-coords to project shadow map onto scene.
	ShadowPosH = gShadowTransform * vec4( PosL, 1.0 );

	// Generate projective tex-coords to project SSAO map onto scene.
	SsaoPosH   = gWorldViewProjTex * vec4( PosL, 1.0 );
}
