////////////////////////////////////////////////////////////////////////////////
// Filename: TesselatedShadowMap.tes
////////////////////////////////////////////////////////////////////////////////

// When directX ask for partitioning and output topology
// for the Hull stage, OpenGL needs to be informed about it
// as input layout of the evaluation stage.
layout( triangles, fractional_odd_spacing, cw ) in;

/////////////////////
// INPUT VARIABLES //
/////////////////////
in vec3  tcPosW[];	   // World position
in vec3  tcNormalW[];  // World normal
in vec2  tcTexOut[];   // Texture coordinates

//////////////////////
// OUTPUT VARIABLES //
//////////////////////
out vec3  tePosW;	  // World position
out vec3  teNormalW;  // World normal
out vec2  teTexOut;   // Texture coordinates

///////////////////////
// UNIFORM VARIABLES //
///////////////////////
uniform mat4  gViewProj;
uniform vec3  gEyePosW;
uniform float gHeightScale;

// Texture uniform variables.
uniform sampler2D gNormalMap;

////////////////////////////////////////////////////////////////////////////////
// Tesselation Evaluation Shader Stage
////////////////////////////////////////////////////////////////////////////////
void main(void)
{
	// In the case of triangle topology, gl_TessCoord is a barycentric coordinate
	vec3 lBary = gl_TessCoord;

	// Interpolate patch attributes to generated vertices.
	vec3 lPosW    = lBary.x * tcPosW[0]     + lBary.y * tcPosW[1]     + lBary.z * tcPosW[2];
	vec3 lNormalW = lBary.x * tcNormalW[0]  + lBary.y * tcNormalW[1]  + lBary.z * tcNormalW[2];
	vec2 lTexOut  = lBary.x * tcTexOut[0]   + lBary.y * tcTexOut[1]   + lBary.z * tcTexOut[2];
	
	// Interpolating normal can unnormalize it, so normalize it.
	lNormalW  = normalize( lNormalW );
	
	//
	// Displacement mapping.
	//
	
	// Choose the mipmap level based on distance to the eye; specifically, choose
	// the next miplevel every MipInterval units, and clamp the miplevel in the range [ 0, 6 ].
	const float lMipInterval = 20.0f;
	float lMipLevel = clamp( ( distance( lPosW, gEyePosW ) - lMipInterval ) / lMipInterval, 0.0f, 6.0f );
	
	// Sample height map (stored in alpha channel).
	float lHeightMapFact = textureLod( gNormalMap, lTexOut, lMipLevel ).a;
	
	// Offset vertex along normal.
	lPosW += ( gHeightScale * ( lHeightMapFact - 1.0f ) ) * lNormalW;

	// Project to homogeneous clip space and return all results.
	gl_Position = gViewProj * vec4( lPosW, 1.0f );
	tePosW    = lPosW;
	teNormalW = lNormalW;
	teTexOut  = lTexOut;
}
