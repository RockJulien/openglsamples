////////////////////////////////////////////////////////////////////////////////
// Filename: SkinnedShadowMap.vs
////////////////////////////////////////////////////////////////////////////////

/////////////////////
// INPUT VARIABLES //
/////////////////////
in vec3  PosL;		// Local position
in vec3  NormalL;	// Local normal
in vec4  TangentL;	// Local tangent
in vec2  TexIn;     // Texture coordinates.
in vec3  Weights;   // Bone weights
in uvec4 BoneIndices; // Bone indices.

//////////////////////
// OUTPUT VARIABLES //
//////////////////////
out vec2 TexOut;	 // Texture coordinates.

///////////////////////
// UNIFORM VARIABLES //
///////////////////////
uniform mat4 gBoneTransforms[96];
uniform mat4 gWorldViewProj;
uniform mat4 gTexTransform;

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader Stage
////////////////////////////////////////////////////////////////////////////////
void main(void)
{
	float lWeights[4] = { 0.0, 0.0, 0.0, 0.0 };
	lWeights[0] = Weights.x;
	lWeights[1] = Weights.y;
	lWeights[2] = Weights.z;
	lWeights[3] = 1.0f - lWeights[0] - lWeights[1] - lWeights[2];

	vec3 lPosL = vec3( 0.0, 0.0, 0.0 );
	for( int i = 0; i < 4; i++ )
	{
	    // Assume no nonuniform scaling when transforming normals, so 
		// that we do not have to use the inverse-transpose.
	    lPosL += lWeights[ i ] * vec4( gBoneTransforms[ BoneIndices[ i ] ] * vec4( PosL, 1.0 ) ).xyz;
	}
 
	// Transform to homogeneous clip space.
	gl_Position = gWorldViewProj * vec4( lPosL, 1.0 );
	
	// Output vertex attributes for interpolation across triangle.
	TexOut = vec4( gTexTransform * vec4( TexIn, 0.0, 1.0 ) ).xy;
}
