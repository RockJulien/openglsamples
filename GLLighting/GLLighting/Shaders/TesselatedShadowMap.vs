////////////////////////////////////////////////////////////////////////////////
// Filename: TesselatedShadowMap.vs
////////////////////////////////////////////////////////////////////////////////

/////////////////////
// INPUT VARIABLES //
/////////////////////
in vec3 PosL;		// Local position
in vec2 TexIn;		// Texture coordinates
in vec3 NormalL;    // Local normal

//////////////////////
// OUTPUT VARIABLES //
//////////////////////
out vec3  vPosW;	   // World position
out vec3  vNormalW;    // World normal
out vec2  vTexOut;	   // Texture coordinates
out float vTessFactor; // Tesselation factor

///////////////////////
// UNIFORM VARIABLES //
///////////////////////
uniform mat4  gWorld;
uniform mat4  gWorldInvTranspose;
uniform mat4  gTexTransform;
uniform vec3  gEyePosW;
uniform float gMaxTessDistance;
uniform float gMinTessDistance;
uniform float gMinTessFactor;
uniform float gMaxTessFactor;

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader Stage
////////////////////////////////////////////////////////////////////////////////
void main(void)
{
	vec3 lPosW = ( gWorld * vec4( PosL, 1.0f ) ).xyz;
	PosW     = lPosW;
	NormalW  = mat3( gWorldInvTranspose ) * NormalL;
	TexOut   = ( gTexTransform * vec4( TexIn, 0.0f, 1.0f ) ).xy;

	float lDistance = distance( lPosW, gEyePosW );

	// Normalized tessellation factor. 
	// The tessellation is 
	//   0 if d >= gMinTessDistance and
	//   1 if d <= gMaxTessDistance.  
	float lTess = saturate( ( gMinTessDistance - lDistance ) / ( gMinTessDistance - gMaxTessDistance ) );
	
	// Rescale [0,1] --> [ gMinTessFactor, gMaxTessFactor ].
	TessFactor = gMinTessFactor + lTess * ( gMaxTessFactor - gMinTessFactor );
}
