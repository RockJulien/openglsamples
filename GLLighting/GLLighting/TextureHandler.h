#ifndef DEF_TEXTUREHANDLER_H
#define DEF_TEXTUREHANDLER_H

#include "GDevice.h"
#include "TextureEXType.h"
#include <stdio.h>

class TextureHandler
{
private:

	// Attributes
	GDevice* mDevice;

	// Private Methods
	BIGINT LoadBMP( GDevice* device, const CCHAR* textName, BBOOL wrap );
	BIGINT LoadPNG( GDevice* device, const CCHAR* textName, BBOOL wrap );
	BIGINT LoadJPG( GDevice* device, const CCHAR* textName, BBOOL wrap );
	BIGINT LoadDDS( GDevice* device, const CCHAR* textName, BBOOL wrap );
	BIGINT LoadTGA( GDevice* device, const CCHAR* textName, BBOOL wrap );

public:

	// Constructor & Destructor
	TextureHandler(GDevice* pDevice);
	~TextureHandler();

	// Methods
	BIGINT Load(const CCHAR* textName, ExtType type, BBOOL wrap);

};

#endif