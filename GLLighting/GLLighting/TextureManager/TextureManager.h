#ifndef DEF_TEXUREMANAGER_H
#define DEF_TEXUREMANAGER_H

// Includes
#include <map>
#include <string>
#include "..\GDevice.h"
#include "..\TextureEXType.h"

// Class definition
class TextureManager
{
private:

	// Attributes
	UBIGINT  mUnitCount;
	GDevice* mDevice;
	std::map<std::string, UBIGINT> mTextures;

	// Private Methods
	TextureManager(const TextureManager& pCopy);
	TextureManager& operator = (const TextureManager& pAssgmt);

public:

	// Constructor
	TextureManager();
	~TextureManager();

	// Methods
	VVOID   Initialize(GDevice* pDevice);
	BIGINT  CreateTexture(std::string pFilename, ExtType pType);

	UBIGINT UnitCount() const;
	VVOID   SetUnitCount(UBIGINT pCount);
};

#endif