#ifndef DEF_LIGHTINGEFFECT_H
#define DEF_LIGHTINGEFFECT_H

// Includes
#include "..\GDevice.h"
#include "EffectTechnique.h"
#include "..\Material.h"
#include "..\Maths\Color.h"
#include "..\Maths\Matrix4x4.h"
#include "..\OcelotLight.h"

// Namespaces


// Class definition
class LightingEffect
{
private:

	// Attributes
	GDevice*         mDevice;
	EffectTechnique* mCurrentTechnique;

public:

	// Constructor & Destructor
	LightingEffect(GDevice* pDevice);
	~LightingEffect();

	// Methods
	VVOID SetWorldViewProj(const Matrix4x4& pMatrix);
	VVOID SetWorld(const Matrix4x4& pMatrix);
	VVOID SetWorldInvTranspose(const Matrix4x4& pMatrix);
	VVOID SetEyePosW(const Vector3& pEyePos);
	VVOID SetDirLight(const Ocelot::OcelotParallelLight& pDirLight);
	VVOID SetPointLight(const Ocelot::OcelotPointLight& pPointLight);
	VVOID SetSpotLight(const Ocelot::OcelotSpotLight& pSpotLight);
	VVOID SetMaterial(const Material& pMaterial);

	VVOID MakeCurrent(EffectTechnique* pCurrent);

	// Technique getters
	EffectTechnique* LightingTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
			
};

#endif