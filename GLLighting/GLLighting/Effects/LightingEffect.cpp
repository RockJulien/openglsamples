#include "LightingEffect.h"
#include "Constants.h"

// Namespaces used
using namespace std;
using namespace Ocelot;

/********************************************************************************************************/
LightingEffect::LightingEffect(GDevice* pDevice) :
mCurrentTechnique(NULL), mDevice(pDevice)
{

}

/********************************************************************************************************/
LightingEffect::~LightingEffect()
{

}

/********************************************************************************************************/
VVOID LightingEffect::SetWorldViewProj(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gWorldViewProj");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/********************************************************************************************************/
VVOID LightingEffect::SetWorld(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gWorld");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/********************************************************************************************************/
VVOID LightingEffect::SetWorldInvTranspose(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gWorldInvTranspose");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/********************************************************************************************************/
VVOID LightingEffect::SetEyePosW(const Vector3& pEyePos)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gEyePosW");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform3fv(lShaderLocation, 1, pEyePos);
}

/********************************************************************************************************/
VVOID LightingEffect::SetDirLight(const Ocelot::OcelotParallelLight& pDirLight)
{
	UBIGINT lShaderLocation;

	lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), "gDirLight.Ambient" );
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pDirLight.Ambient());

	lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), "gDirLight.Diffuse" );
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pDirLight.Diffuse());

	lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), "gDirLight.Specular" );
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pDirLight.Specular());

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gDirLight.Direction");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform3fv(lShaderLocation, 1, pDirLight.Direction());

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Set Effect Light FAILURE !!! ");
	}
}

/********************************************************************************************************/
VVOID LightingEffect::SetPointLight(const Ocelot::OcelotPointLight& pPointLight)
{
	UBIGINT lShaderLocation;

	lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), "gPointLight.Ambient" );
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pPointLight.Ambient());

	lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), "gPointLight.Diffuse" );
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pPointLight.Diffuse());

	lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), "gPointLight.Specular" );
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pPointLight.Specular());

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gPointLight.Position");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform3fv(lShaderLocation, 1, pPointLight.Position());

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gPointLight.Att");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform3fv(lShaderLocation, 1, pPointLight.Attenuation());

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gPointLight.Range");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform1f(lShaderLocation, pPointLight.Range());

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Set Effect Light FAILURE !!! ");
	}
}

/********************************************************************************************************/
VVOID LightingEffect::SetSpotLight(const Ocelot::OcelotSpotLight& pSpotLight)
{
	UBIGINT lShaderLocation;

	lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), "gSpotLight.Ambient" );
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pSpotLight.Ambient());

	lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), "gSpotLight.Diffuse" );
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pSpotLight.Diffuse());

	lShaderLocation = this->mDevice->glGetUniformLocation( this->mCurrentTechnique->Handle(), "gSpotLight.Specular" );
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pSpotLight.Specular());

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gSpotLight.Position");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform3fv(lShaderLocation, 1, pSpotLight.Position());

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gSpotLight.Direction");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform3fv(lShaderLocation, 1, pSpotLight.Direction());

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gSpotLight.Att");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform3fv(lShaderLocation, 1, pSpotLight.Attenuation());

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gSpotLight.Range");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform1f(lShaderLocation, pSpotLight.Range());

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gSpotLight.Spot");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform1f(lShaderLocation, pSpotLight.SpotPower());

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Set Effect Light FAILURE !!! ");
	}
}

/********************************************************************************************************/
VVOID LightingEffect::SetMaterial(const Material& pMaterial)
{
	UBIGINT lShaderLocation;

	// Now, pass material object
	// Obviously opengl does not allow to pass an entire struct as DirectX does
	// so, we ll pass elements one by one. It could avoid padding issues that appear
	// in DirectX managing data by one or four. (e.g: vec3 need an extra float as padding).
	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Ambient");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pMaterial.Ambient);

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Diffuse");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pMaterial.Diffuse);

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Specular");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pMaterial.Specular);

	lShaderLocation = this->mDevice->glGetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Reflect");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->glUniform4fv(lShaderLocation, 1, pMaterial.Reflect);

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Set Material Failure !!! ");
	}
}

/********************************************************************************************************/
VVOID LightingEffect::MakeCurrent(EffectTechnique* pCurrent)
{
	if(pCurrent == NULL)
	{
		return;
	}

	pCurrent->MakeCurrent();
	this->mCurrentTechnique = pCurrent;
}

/********************************************************************************************************/
EffectTechnique* LightingEffect::LightingTech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLightingTechName, pStagePaths, pIncludes );
}
