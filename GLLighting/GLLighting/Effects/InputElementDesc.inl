
/*************************************************************************************************/
inline const std::string& InputElementDesc::SemanticName() const
{
	return this->mSemanticName;
}

/*************************************************************************************************/
inline const VVOID* InputElementDesc::Offset() const
{
	return this->mOffset;
}

/*************************************************************************************************/
inline UBIGINT		InputElementDesc::SemanticIndex() const
{
	return this->mSemanticIndex;
}

/*************************************************************************************************/
inline UBIGINT		InputElementDesc::Stride() const
{
	return this->mStride;
}

/*************************************************************************************************/
inline USMALLINT    InputElementDesc::DataCount() const
{
	return this->mDataCount;
}

/*************************************************************************************************/
inline USMALLINT    InputElementDesc::DataType() const
{
	return this->mDataType;
}

/*************************************************************************************************/
inline BBOOL		InputElementDesc::NeedNormalization() const
{
	return this->mNeedNormalization;
}

/*************************************************************************************************/
inline VVOID        InputElementDesc::SetSemanticName(const std::string& pSemanticName)
{
	this->mSemanticName = pSemanticName;
}

/*************************************************************************************************/
inline VVOID        InputElementDesc::SetOffset(const VVOID* pOffset)
{
	this->mOffset = pOffset;
}

/*************************************************************************************************/
inline VVOID		InputElementDesc::SetSemanticIndex(const UBIGINT& pSemanticIndex)
{
	this->mSemanticIndex = pSemanticIndex;
}

/*************************************************************************************************/
inline VVOID		InputElementDesc::SetStride(const UBIGINT& pStride)
{
	this->mStride = pStride;
}

/*************************************************************************************************/
inline VVOID		InputElementDesc::SetDataCount(const USMALLINT& pDataCount)
{
	this->mDataCount = pDataCount;
}

/*************************************************************************************************/
inline VVOID		InputElementDesc::SetDataType(const USMALLINT& pDataType)
{
	this->mDataType = pDataType;
}

/*************************************************************************************************/
inline VVOID		InputElementDesc::SetNeedNormalization(BBOOL pNormalize)
{
	this->mNeedNormalization = pNormalize;
}
