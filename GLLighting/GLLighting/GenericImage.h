#ifndef DEF_GENERICIMAGE_H
#define DEF_GENERICIMAGE_H

// Includes
#include "OSCheck.h"

// Class definition
class GenericImage
{
private:

	// Attributes
	UCCHAR* mPixels;
	UCCHAR* mMap;
	UBIGINT mWidth;
	UBIGINT mHeight;
	UBIGINT mMapEntries;
	UBIGINT	mFormat;
	UBIGINT mMapFormat;
	BIGINT  mComponents;

public:

	// Constructor
	GenericImage();
	~GenericImage();

	// Methods
	VVOID Release();

	// Accessors
	inline UCCHAR* Pixels() const;
	inline UCCHAR* Map() const;
	inline UBIGINT Width() const;
	inline UBIGINT Height() const;
	inline UBIGINT MapEntries() const;
	inline UBIGINT Format() const;
	inline UBIGINT MapFormat() const;
	inline BIGINT  Components() const;
	inline VVOID   SetPixels(UCCHAR* pPixels);
	inline VVOID   SetMap(UCCHAR* pMap);
	inline VVOID   SetWidth(UBIGINT pWidth);
	inline VVOID   SetHeight(UBIGINT pHeight);
	inline VVOID   SetMapEntries(UBIGINT pMapEntries);
	inline VVOID   SetFormat(UBIGINT pFormat);
	inline VVOID   SetMapFormat(UBIGINT pMapFormat);
	inline VVOID   SetComponents(BIGINT pComponents);
};

// Inlines
#include "GenericImage.inl"

#endif