#include "BoundingSphere.h"

// Namespaces used

/*************************************************************************************************/
BoundingSphere::BoundingSphere() :
mCenter(0.0), mRadius(0.0f)
{

}

/*************************************************************************************************/
BoundingSphere::~BoundingSphere()
{

}

/*************************************************************************************************/
Vector3 BoundingSphere::Center() const
{
	return this->mCenter;
}

/*************************************************************************************************/
FFLOAT  BoundingSphere::Radius() const
{
	return this->mRadius;
}

/*************************************************************************************************/
VVOID   BoundingSphere::SetCenter(const Vector3& pCenter)
{
	this->mCenter = pCenter;
}

/*************************************************************************************************/
VVOID   BoundingSphere::SetRadius(const FFLOAT& pRadius)
{
	this->mRadius = pRadius;
}
