#ifndef DEF_MATRIXVIEW_H
#define DEF_MATRIXVIEW_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   MatrixView.h/.cpp
/
/ Description: This file provide a way to create a 4 by 4 matrix view for
/              setting camera properties accordingly. A possibily to
/              construct left-hand or right-hand coordinates matrix view
/              has been provided as well.
/
/ Author:      Larbi Julien
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "Matrix4x4.h"

class MatrixView : public Matrix4x4
{
private:

	// Variables which will make the view matrix
	Vector3 m_vEye;
	Vector3 m_vAt;
	Vector3 m_vUp;

public:

	// Constructor & Destructor
	MatrixView();
	~MatrixView();

	// Methods
	MatrixView MatrixViewLookAtLH(const Vector3& eye, const Vector3& at, const Vector3& up);
	MatrixView MatrixViewLookAtRH(const Vector3& eye, const Vector3& at, const Vector3& up);

	// Accessors
	Vector3 getEye() const { return m_vEye; }
	Vector3 getAt() const { return m_vAt; }
	Vector3 getUp() const { return m_vUp; }
	void    setEye(const Vector3& eye) { m_vEye = eye; }
	void    setAt(const Vector3& at) { m_vAt = at; }
	void    setUp(const Vector3& up) { m_vUp = up; }
};

#endif