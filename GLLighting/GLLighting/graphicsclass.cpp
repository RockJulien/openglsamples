////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "graphicsclass.h"
#include "TGALoader.h"
#include <fstream>
#include "MathUtilities.h"
#include <time.h>
#include "systemclass.h"
#include "MeshData.h"

using namespace std;
using namespace Ocelot;

/*********************************************************************************************/
GraphicsClass::GraphicsClass() :
mOpenGL(NULL), mCamera(NULL), mController(NULL), mLandVAO(0), mLandVB(0), 
mLandIB(0), mWaveVAO(0), mWaveVB(0), mWaveIB(0), mEyePosW(0.0f), mWaves(),
mLandIndexCount(0), mLightingTech(NULL), mLayout(NULL), mTheta(1.5f * PI), 
mPhi(0.1f * PI), mRadius(80.0f), mLightingFX(NULL)
{
	srand((UBIGINT)time(NULL));

	// Initialize the camera.
	OcelotCameraInfo lCamInfo( Vector3( 0.0f, 2.0f, -15.0f ), 
							   Vector3( 0.0f, 0.0f, 1.0f ), 
							   (FFLOAT)(800 / 600), 
							   1000.0f );
	lCamInfo.SetNear(1.0f);
	this->mCamera = new OcelotCamera( lCamInfo );

	// And create the controller
	this->mController = ControlFact->createController( this->mCamera, 
													   CAMERA_CONTROLLER_FLYER );

	// Initialize meshes matrices
	this->mLandWorld.Matrix4x4Identity();
	this->mWaveWorld.Matrix4x4Identity();
	this->mLightView.Matrix4x4Identity();
	this->mLightProj.Matrix4x4Identity();

	Matrix4x4 lWaveOffset;
	lWaveOffset.Matrix4x4Translation( this->mWaveWorld, 0.0f, -3.0f, 0.0f );

	// Initialize the light object.
	this->mDirLight.SetAmbient( Color( 0.2f, 0.2f, 0.2f, 1.0f ) );
	this->mDirLight.SetDiffuse( Color( 0.5f, 0.5f, 0.5f, 1.0f ) );
	this->mDirLight.SetSpecular( Color( 0.5f, 0.5f, 0.5f, 1.0f ) );
	this->mDirLight.SetDirection( Vector3( 0.57735f, -0.57735f, 0.57735f ) );

	// Point light--position is changed every frame to animate in UpdateScene function.
	this->mPointLight.SetAmbient( Color( 0.3f, 0.3f, 0.3f, 1.0f ) );
	this->mPointLight.SetDiffuse( Color( 0.7f, 0.7f, 0.7f, 1.0f ) );
	this->mPointLight.SetSpecular( Color( 0.7f, 0.7f, 0.7f, 1.0f ) );
	this->mPointLight.SetAttenuation( Vector3( 0.0f, 0.1f, 0.0f ) );
	this->mPointLight.SetRange( 25.0f );

	// Spot light--position and direction changed every frame to animate in UpdateScene function.
	this->mSpotLight.SetAmbient( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mSpotLight.SetDiffuse( Color( 1.0f, 1.0f, 0.0f, 1.0f ) );
	this->mSpotLight.SetSpecular( Color( 1.0f, 1.0f, 1.0f, 1.0f ) );
	this->mSpotLight.SetAttenuation( Vector3( 1.0f, 0.0f, 0.0f ) );
	this->mSpotLight.SetSpotPower( 96.0f );
	this->mSpotLight.SetRange( 10000.0f );

	// Initialize materials
	this->mLandMat.Ambient  = Color( 0.48f, 0.77f, 0.46f, 1.0f );
	this->mLandMat.Diffuse  = Color( 0.48f, 0.77f, 0.46f, 1.0f );
	this->mLandMat.Specular = Color( 0.2f, 0.2f, 0.2f, 16.0f );
	this->mLandMat.Reflect  = Color( 0.0f, 0.0f, 0.0f, 1.0f );

	this->mWaveMat.Ambient  = Color( 0.137f, 0.42f, 0.556f, 1.0f );
	this->mWaveMat.Diffuse  = Color( 0.137f, 0.42f, 0.556f, 1.0f );
	this->mWaveMat.Specular = Color( 0.8f, 0.8f, 0.8f, 96.0f );
	this->mWaveMat.Reflect  = Color( 0.0f, 0.0f, 0.0f, 1.0f );
}

/*********************************************************************************************/
GraphicsClass::~GraphicsClass()
{
	
}

/*********************************************************************************************/
BBOOL GraphicsClass::Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight)
{
	BBOOL lResult = false;

	// Store a pointer to the OpenGL class object.
	this->mOpenGL = pGDevice;

	this->mWaves.Init( 160, 160, 1.0f, 0.03f, 3.25f, 0.4f );
	this->BuildVertexLayout();
	this->BuildLandGeometryBuffers();
	this->BuildWaveGeometryBuffers();
	this->BuildFX();

	return true;
}

/*********************************************************************************************/
VVOID GraphicsClass::OnResize(BIGINT pClientWidth, BIGINT pClientHeight)
{
	FFLOAT lAspect = (FFLOAT)pClientWidth / pClientHeight;

	// refresh the aspect of the camera if the
	// window is resized.
	OcelotCameraInfo lInfo = this->mCamera->Info();
	if( this->mCamera != NULL )
	{
		lInfo.SetAspect( lAspect );  // aspect ratio
		this->mCamera->SetInfo( lInfo );
	}

	// Reset the projection
	this->mLightProj.Matrix4x4Identity();
	this->mLightProj = this->mLightProj.MatrixProjectionPerspectiveFOVLH( 0.25f * PI, lAspect, 1.0f, 1000.0f );

	glViewport( 0, 0, pClientWidth, pClientHeight );
}

/*********************************************************************************************/
VVOID GraphicsClass::Shutdown()
{
	DeletePointer( this->mCamera );
	DeletePointer( this->mController );
	DeletePointer( this->mLightingTech );
	DeletePointer( this->mLayout );
	DeletePointer( this->mLightingFX );

	// Release VAOs, VBOs and IBOs
	// Release shapes buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mLandVB );
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mLandIB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mLandVAO );

	// Release Skull buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mWaveVB );
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mWaveIB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mWaveVAO );
}

/*********************************************************************************************/
BBOOL GraphicsClass::Update(FFLOAT pDeltaTime)
{
	// Update the camera.
	this->mController->update( pDeltaTime );

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Start Update Failure !!! ");
	}

	//
	// Every quarter second, generate a random wave.
	//
	static FFLOAT sBase = 0.0f;
	FFLOAT lTotalTime = SystemClass::GetTotalTime();
	if( (lTotalTime - sBase) >= 0.25f )
	{
		sBase += 0.25f;
 
		ULINT lRow = 5 + rand() % (this->mWaves.RowCount() - 10 );
		ULINT lCol = 5 + rand() % (this->mWaves.ColumnCount() - 10 );

		FFLOAT lMagnitude = RandBetween( 1.0f, 2.0f );

		this->mWaves.Disturb( lRow, lCol, lMagnitude );
	}

	this->mWaves.Update( pDeltaTime );

	//
	// Update the wave vertex buffer with the new solution.
	//
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mWaveVB );
	VoxelVertex* lVBO = reinterpret_cast<VoxelVertex*>( this->mOpenGL->glMapBuffer( GL_ARRAY_BUFFER, GL_WRITE_ONLY ) );
	if 
		( lVBO )
	{
		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Map Waves VBO Failure !!! ");
		}

		// Update waves VBO
		for( UBIGINT lCurrVertex = 0; lCurrVertex < mWaves.VertexCount(); lCurrVertex++ )
		{
			lVBO[ lCurrVertex ].m_vPosition = this->mWaves[ lCurrVertex ];
			lVBO[ lCurrVertex ].m_vNormal   = this->mWaves.Normal( lCurrVertex );
		}

		this->mOpenGL->glUnmapBuffer( GL_ARRAY_BUFFER );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Unmap Waves VBO Failure !!! ");
		}
	}

	//
	// Animate the lights.
	//
	OcelotCameraInfo lInfo = this->mCamera->Info();
	// Circle light over the land surface.
	Vector3 lPosition;
	lPosition.x( 70.0f * cosf( 0.2f * lTotalTime ) );
	lPosition.z( 70.0f * sinf( 0.2f * lTotalTime ) );
	lPosition.y( Higher( this->GetHillHeight( this->mPointLight.Position().getX(), 
											  this->mPointLight.Position().getZ() ), 
						 -3.0f ) + 10.0f );
	this->mPointLight.SetPosition( lPosition );

	// The spotlight takes on the camera position and is aimed in the
	// same direction the camera is looking.  In this way, it looks
	// like we are holding a flashlight.
	Vector3 lTarget(0.0f);
	this->mSpotLight.SetPosition( lInfo.Eye() );
	this->mSpotLight.SetDirection( (lTarget - lInfo.Eye()).Vec3Normalise() );

	return true;
}

/*********************************************************************************************/
BBOOL GraphicsClass::Render()
{
	// Clear the buffers to begin the scene.
	this->mOpenGL->StartRender( true, true );

	GLenum lError = glGetError();
	if( 
		lError != GL_NO_ERROR )
	{
		throw exception(" Before rendering FAILURE !!! ");
	}

	GLuint lStatus = this->mOpenGL->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	Matrix4x4 lView  = this->mCamera->View();
	Matrix4x4 lProj  = this->mCamera->Proj();
	Matrix4x4 lViewProj = lView * lProj;
	OcelotCameraInfo lInfo = this->mCamera->Info();

	if
		( this->mLightingFX != NULL )
	{
		// Make the technique current
		this->mLightingFX->MakeCurrent( this->mLightingTech );

		this->mLightingFX->SetDirLight( this->mDirLight );
		this->mLightingFX->SetPointLight( this->mPointLight );
		this->mLightingFX->SetSpotLight( this->mSpotLight );
		this->mLightingFX->SetEyePosW( lInfo.Eye() );

		//
		// Draw the hills.
		//
		Matrix4x4 lWorld = this->mLandWorld;
		Matrix4x4 lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		Matrix4x4 lWorldViewProj = lWorld * lViewProj;

		this->mLightingFX->SetWorld( lWorld );
		this->mLightingFX->SetWorldInvTranspose( lWorldInvTranspose );
		this->mLightingFX->SetWorldViewProj( lWorldViewProj );
		this->mLightingFX->SetMaterial( this->mLandMat );

		this->mOpenGL->glBindVertArray( this->mLandVAO );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Land Failure !!! ");
		}

		glDrawElements( GL_TRIANGLES, this->mLandIndexCount, GL_UNSIGNED_INT, NULL );

		// Unbind Land VAO
		this->mOpenGL->glBindVertArray( 0 );

		//
		// Draw the waves.
		//
		lWorld = this->mWaveWorld;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * lViewProj;

		this->mLightingFX->SetWorld( lWorld );
		this->mLightingFX->SetWorldInvTranspose( lWorldInvTranspose );
		this->mLightingFX->SetWorldViewProj( lWorldViewProj );
		this->mLightingFX->SetMaterial( this->mWaveMat );

		this->mOpenGL->glBindVertArray( this->mWaveVAO );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Waves Failure !!! ");
		}

		glDrawElements( GL_TRIANGLES, 3 * this->mWaves.TriangleCount(), GL_UNSIGNED_INT, NULL );

		// Unbind Waves VAO
		this->mOpenGL->glBindVertArray( 0 );
	}

	lStatus = this->mOpenGL->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	// Present the rendered scene to the screen.
	this->mOpenGL->StopRender();

	return true;
}

/*********************************************************************************************/
FFLOAT  GraphicsClass::GetHillHeight( FFLOAT pX, FFLOAT pZ ) const
{
	return 0.3f * ( pZ * sinf( 0.1f * pX ) + pX * cosf( 0.1f * pZ ) );
}

/*********************************************************************************************/
Vector3 GraphicsClass::GetHillNormal( FFLOAT pX, FFLOAT pZ ) const
{
	// Normal = (-df / dx, 1, -df / dz)
	Vector3 lNormal( -0.03f * pZ * cosf( 0.1f * pX ) - 0.3f * cosf( 0.1f * pZ ),
					 1.0f,
					 -0.3f * sinf( 0.1f * pX ) + 0.03f * pX * sinf( 0.1f * pZ ) );
	
	Vector3 lUnitNormal = lNormal.Vec3Normalise();

	return lUnitNormal;
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildLandGeometryBuffers()
{
	MeshData lGrid;

	GeometryHelper::CreateGrid( 160.0f, 160.0f, 50, 50, lGrid );

	this->mLandIndexCount = (UBIGINT)lGrid.Indices.size();

	//
	// Extract the vertex elements we are interested and apply the height function to
	// each vertex.  
	//
	std::vector<VoxelVertex> lVertices( lGrid.Vertices.size() );
	for( UBIGINT lCurrVert = 0; lCurrVert < (UBIGINT)lGrid.Vertices.size(); lCurrVert++ )
	{
		Vector3 lPosition = lGrid.Vertices[ lCurrVert ].Position;

		lPosition.y( this->GetHillHeight( lPosition.getX(), lPosition.getZ() ) );
		
		lVertices[ lCurrVert ].m_vPosition = lPosition;
		lVertices[ lCurrVert ].m_vNormal   = this->GetHillNormal( lPosition.getX(), lPosition.getZ() );
	}

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Land Geometry Failure !!! ");
	}

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mLandVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mLandVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mLandVB );

	// Bind the vertex buffer and load the vertex (position, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mLandVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, lVertices.size() * sizeof(VoxelVertex), &lVertices[0], GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	this->mLayout->SetVertexLayout( this->mLandVB );

	// Generate an Index buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mLandIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mLandIB );
	this->mOpenGL->glBufferData( GL_ELEMENT_ARRAY_BUFFER, this->mLandIndexCount * sizeof(UBIGINT), &lGrid.Indices[0], GL_STATIC_DRAW );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Land Geometry Failure !!! ");
	}

	this->mOpenGL->glBindVertArray( 0 );
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildWaveGeometryBuffers()
{
	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Wave Geometry Failure !!! ");
	}

	// Create the vertex buffer.  Note that we allocate space only, as
	// we will be updating the data every time step of the simulation.

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mWaveVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mWaveVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mWaveVB );

	// Bind the vertex buffer and load the vertex (position, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mWaveVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, this->mWaves.VertexCount() * sizeof(VoxelVertex), NULL, GL_DYNAMIC_DRAW );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Wave Geometry Failure !!! ");
	}

	// Set the vertex stage layout.
	this->mLayout->SetVertexLayout( this->mWaveVB );

	// Create the index buffer.  The index buffer is fixed, so we only 
	// need to create and set once.

	vector<UBIGINT> lIndices( 3 * this->mWaves.TriangleCount() ); // 3 indices per face

	// Iterate over each quad.
	UBIGINT lRowCount = this->mWaves.RowCount();
	UBIGINT lColCount = this->mWaves.ColumnCount();
	BIGINT lCounter = 0;
	for( UBIGINT lCurrRow = 0; lCurrRow < lRowCount - 1; lCurrRow++ )
	{
		for( UBIGINT lCurrCol = 0; lCurrCol < lColCount - 1; lCurrCol++ )
		{
			lIndices[ lCounter ]   = lCurrRow * lColCount + lCurrCol;
			lIndices[ lCounter + 1 ] = lCurrRow * lColCount + lCurrCol + 1;
			lIndices[ lCounter + 2 ] = (lCurrRow + 1) * lColCount + lCurrCol;

			lIndices[ lCounter + 3 ] = (lCurrRow + 1) * lColCount + lCurrCol;
			lIndices[ lCounter + 4 ] = lCurrRow * lColCount + lCurrCol + 1;
			lIndices[ lCounter + 5 ] = (lCurrRow + 1) * lColCount + lCurrCol + 1;

			lCounter += 6; // next quad
		}
	}

	// Generate a Index buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mWaveIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mWaveIB );
	this->mOpenGL->glBufferData( GL_ELEMENT_ARRAY_BUFFER, lIndices.size() * sizeof(UBIGINT), &lIndices[0], GL_STATIC_DRAW );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Wave Geometry Failure !!! ");
	}

	// Unbind the VAO
	this->mOpenGL->glBindVertArray( 0 );
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildFX()
{
	// Initialize effects and get the techniques
	this->mLightingFX = new LightingEffect( this->mOpenGL );
	vector<string> lIncludes;
	lIncludes.push_back("Shaders/LightHelper.helper");
	StageDescription lDescription;
	lDescription.SetVertexStagePath("Shaders/Lighting.vs");
	lDescription.SetPixelStagePath("Shaders/Lighting.ps");
	this->mLightingTech = this->mLightingFX->LightingTech( lDescription, lIncludes );
	this->mLightingTech->SetLayout( this->mLayout );
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildVertexLayout()
{
	UBIGINT lStride = sizeof(VoxelVertex);
	this->mLayout   = new InputLayout( this->mOpenGL );
	this->mLayout->AddElement( InputElementDesc("PosL",    0, lStride, 3, GL_FLOAT));
	this->mLayout->AddElement( InputElementDesc("NormalL", 1, lStride, 3, GL_FLOAT, (UCCHAR*)NULL + (3 * sizeof(FFLOAT))));
}

/*********************************************************************************************/
IOcelotCameraController* GraphicsClass::Controller()
{
	return this->mController;
}
