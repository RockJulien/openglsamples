#ifndef DEF_SKINNEDMODEL_H
#define DEF_SKINNEDMODEL_H

// Includes
#include <string>
#include <vector>
#include "..\Material.h"
#include "..\SkinnedData.h"
#include "..\VertexTypes.h"
#include "..\MeshGeometry.h"
#include "..\TextureManager\TextureManager.h"

// Class definition
class SkinnedModel
{
private:

	// Attributes
	std::vector<Material>			    mMaterial;
	std::vector<UBIGINT>			    mDiffuseMap;
	std::vector<UBIGINT>			    mNormalMap;
	std::vector<Ocelot::SkinnedVertex>  mVertices;
	std::vector<UBIGINT>			    mIndices;
	std::vector<Subset>				    mSubsets;
	MeshGeometry<Ocelot::SkinnedVertex> mMesh;
	SkinnedData						    mSkinnedData;
	UBIGINT							    mSubsetCount;

public:

	// Constructor
	SkinnedModel(GDevice* pDevice, TextureManager& pTextureService, const std::string& pModelFilename, const std::string& pTexturePath, ExtType pTextType);
	~SkinnedModel();

	// Methods


	// Accessors
	std::vector<Material>&			     Material();
	std::vector<UBIGINT>&			     DiffuseMap();
	std::vector<UBIGINT>&			     NormalMap();
	std::vector<Ocelot::SkinnedVertex>&  Vertices();
	std::vector<UBIGINT>&			     Indices();
	std::vector<Subset>&			     Subsets();
	MeshGeometry<Ocelot::SkinnedVertex>& Mesh();
	SkinnedData&					     SkinnedData();
	UBIGINT							     SubsetCount() const;

};

#endif