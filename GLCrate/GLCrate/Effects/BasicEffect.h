#ifndef DEF_BASICEFFECT_H
#define DEF_BASICEFFECT_H

// Includes
#include <map>
#include <string>
#include "..\GDevice.h"
#include "..\Material.h"
#include "..\Maths\Color.h"
#include "..\Maths\Matrix4x4.h"
#include "..\OcelotLight.h"
#include "EffectTechnique.h"

// Namespaces

// Class definition
class BasicEffect
{
private:

	std::map<std::string, BBOOL> mFlagMap;

	// Attributes
	EffectTechnique* mCurrentTechnique;
	GDevice*		 mDevice;

public:

	// Constructor & destructor
	BasicEffect(GDevice* pDevice);
	~BasicEffect();

	// Methods
	VVOID SetWorldViewProj(const Matrix4x4& pMatrix);
	VVOID SetWorld(const Matrix4x4& pMatrix);
	VVOID SetWorldInvTranspose(const Matrix4x4& pMatrix);
	VVOID SetTexTransform(const Matrix4x4& pMatrix);
	VVOID SetEyePosW(const Vector3& pEyePos);
	VVOID SetDirLights(const Ocelot::OcelotParallelLight* pLights, UBIGINT pCount);
	VVOID SetMaterial(const Material& pMaterial);
	VVOID SetDiffuseMap(const UBIGINT& pUnit);

	VVOID MakeCurrent(EffectTechnique* pCurrent);

	// Technique getters
	EffectTechnique* Light1Tech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
	EffectTechnique* Light2Tech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
	EffectTechnique* Light3Tech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);

	EffectTechnique* Light0TexTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
	EffectTechnique* Light1TexTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
	EffectTechnique* Light2TexTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
	EffectTechnique* Light3TexTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
};

#endif