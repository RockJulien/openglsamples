////////////////////////////////////////////////////////////////////////////////
// Filename: Basic.vs
////////////////////////////////////////////////////////////////////////////////

/////////////////////
// INPUT VARIABLES //
/////////////////////
in vec3 PosL;		// Local position
in vec3 NormalL;    // Local normal
in vec2 TexIn;      // Texture coordinates

//////////////////////
// OUTPUT VARIABLES //
//////////////////////
out vec3 PosW;		 // World position
out vec3 NormalW;    // World normal
out vec2 TexOut;     // Texture coordinates

///////////////////////
// UNIFORM VARIABLES //
///////////////////////
uniform mat4 gWorld;
uniform mat4 gWorldInvTranspose;
uniform mat4 gWorldViewProj;
uniform mat4 gTexTransform;

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader Stage
////////////////////////////////////////////////////////////////////////////////
void main(void)
{
	// Transform to world space space.
	PosW    = vec4( gWorld * vec4( PosL, 1.0f ) ).xyz;
	NormalW = mat3( gWorldInvTranspose ) * NormalL;
		
	// Transform to homogeneous clip space.
	gl_Position = gWorldViewProj * vec4( PosL, 1.0 );

	// Output vertex attributes for interpolation across triangle.
	TexOut = vec4( gTexTransform * vec4( TexIn, 0.0, 1.0 ) ).xy;
}
