////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "graphicsclass.h"
#include "TGALoader.h"
#include <fstream>
#include "MathUtilities.h"
#include <time.h>
#include "systemclass.h"
#include "MeshData.h"

using namespace std;
using namespace Ocelot;

/*********************************************************************************************/
GraphicsClass::GraphicsClass() :
mOpenGL(NULL), mCamera(NULL), mController(NULL), 
mBoxVAO(0), mBoxVB(0), mBoxIB(0), mLight2TexTech(NULL)
{
	srand((UBIGINT)time(NULL));

	// Initialize the camera.
	OcelotCameraInfo lCamInfo( Vector3( 0.0f, 2.0f, -15.0f ), 
							   Vector3( 0.0f, 0.0f, 1.0f ), 
							   (FFLOAT)(800 / 600), 
							   1000.0f );
	lCamInfo.SetNear(1.0f);
	this->mCamera = new OcelotCamera( lCamInfo );

	// And create the controller
	this->mController = ControlFact->createController( this->mCamera, 
													   CAMERA_CONTROLLER_FLYER );

	// Initialize the light object(s).
	this->mDirLights[0].SetAmbient( Color( 0.3f, 0.3f, 0.3f, 1.0f ) );
	this->mDirLights[0].SetDiffuse( Color( 0.8f, 0.8f, 0.8f, 1.0f ) );
	this->mDirLights[0].SetSpecular( Color( 0.6f, 0.6f, 0.6f, 16.0f ) );
	this->mDirLights[0].SetDirection( Vector3( 0.707f, -0.707f, 0.0f ) );

	this->mDirLights[1].SetAmbient( Color( 0.2f, 0.2f, 0.2f, 1.0f ) );
	this->mDirLights[1].SetDiffuse( Color( 1.4f, 1.4f, 1.4f, 1.0f ) );
	this->mDirLights[1].SetSpecular( Color( 0.3f, 0.3f, 0.3f, 16.0f ) );
	this->mDirLights[1].SetDirection( Vector3( -0.707f, 0.0f, 0.707f ) );

	// Initialize materials
	this->mBoxMat.Ambient  = Color( 0.5f, 0.5f, 0.5f, 1.0f );
	this->mBoxMat.Diffuse  = Color( 1.0f, 1.0f, 1.0f, 1.0f );
	this->mBoxMat.Specular = Color( 0.6f, 0.6f, 0.6f, 16.0f );
	this->mBoxMat.Reflect  = Color( 0.0f, 0.0f, 0.0f, 1.0f );

	this->mDiffuseTexUnit = 1;
}

/*********************************************************************************************/
GraphicsClass::~GraphicsClass()
{
	
}

/*********************************************************************************************/
BBOOL GraphicsClass::Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight)
{
	BBOOL lResult = false;

	// Store a pointer to the OpenGL class object.
	this->mOpenGL = pGDevice;

	Effects::InitializeAll( this->mOpenGL );
	InputLayouts::InitializeAll( this->mOpenGL );

	// Initialize the texture manager
	this->mTexManager.Initialize( this->mOpenGL );

	// Init the diffuse map to apply to the mesh.
	ExtType lType = ExtType::eTGA;
	if
		( lType == ExtType::eDDS )
	{
		this->mTexTransform.Matrix4x4Scaling( this->mTexTransform, 1.0f, -1.0f, 1.0f ); // Flip the texture bottom/top to top/bottom.
	}

	this->mDiffuseMapRV = this->mTexManager.CreateTexture( "Textures/bricks.tga", lType );

	// Initialize the technique.
	vector<string> lIncludes;
	lIncludes.push_back("Shaders/LightHelper.helper"); // Need light include
	StageDescription lDescription;
	lDescription.SetVertexStagePath("Shaders/Basic.vs");
	lDescription.SetPixelStagePath("Shaders/Basic.ps");
	this->mLight2TexTech = Effects::BasicFX()->Light2TexTech( lDescription, lIncludes );
	this->mLight2TexTech->SetLayout( InputLayouts::PosNormalTexLayout() );

	this->BuildGeometryBuffers();

	return true;
}

/*********************************************************************************************/
VVOID GraphicsClass::OnResize(BIGINT pClientWidth, BIGINT pClientHeight)
{
	FFLOAT lAspect = (FFLOAT)pClientWidth / pClientHeight;

	// refresh the aspect of the camera if the
	// window is resized.
	OcelotCameraInfo lInfo = this->mCamera->Info();
	if( this->mCamera != NULL )
	{
		lInfo.SetAspect( lAspect );  // aspect ratio
		this->mCamera->SetInfo( lInfo );
	}

	glViewport( 0, 0, pClientWidth, pClientHeight );
}

/*********************************************************************************************/
VVOID GraphicsClass::Shutdown()
{
	DeletePointer( this->mCamera );
	DeletePointer( this->mController );
	DeletePointer( this->mLight2TexTech );

	glDeleteTextures( 1, &this->mDiffuseMapRV );

	// Release VAOs, VBOs and IBOs
	// Release box buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mBoxVB );
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mBoxIB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mBoxVAO );

	Effects::DestroyAll();
	InputLayouts::DestroyAll();
}

/*********************************************************************************************/
BBOOL GraphicsClass::Update(FFLOAT pDeltaTime)
{
	// Update the camera.
	this->mController->update( pDeltaTime );

	return true;
}

/*********************************************************************************************/
BBOOL GraphicsClass::Render()
{
	// Clear the buffers to begin the scene.
	this->mOpenGL->StartRender( true, true );

	glClearColor( 0.69f, 0.77f, 0.87f, 1.0f );
	glClearDepth( 1.0f );
	glClearStencil( 0 );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

	GLenum lError = glGetError();
	if( 
		lError != GL_NO_ERROR )
	{
		throw exception(" Before rendering FAILURE !!! ");
	}

	GLuint lStatus = this->mOpenGL->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	Matrix4x4 lView  = this->mCamera->View();
	Matrix4x4 lProj  = this->mCamera->Proj();
	Matrix4x4 lViewProj = lView * lProj;
	OcelotCameraInfo lInfo = this->mCamera->Info();

	if
		( Effects::BasicFX() != NULL )
	{
		// Make the technique current
		Effects::BasicFX()->MakeCurrent( this->mLight2TexTech );

		Effects::BasicFX()->SetDirLights( this->mDirLights, 2 );
		Effects::BasicFX()->SetEyePosW( lInfo.Eye() );

		// Active the first texture unit
		this->mOpenGL->glActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mDiffuseMapRV );

		//
		// Draw the box
		//
		Matrix4x4 lWorld = this->mBoxWorld;
		Matrix4x4 lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		Matrix4x4 lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetTexTransform( this->mTexTransform );
		Effects::BasicFX()->SetMaterial( this->mBoxMat );
		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		this->mOpenGL->glBindVertArray( this->mBoxVAO );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Land Failure !!! ");
		}

		this->mOpenGL->glDrawElementsBaseVertex( GL_TRIANGLES, this->mBoxIndexCount, GL_UNSIGNED_INT, (VVOID*)(this->mBoxIndexOffset * sizeof(UBIGINT)), this->mBoxVertexOffset );
		
		// Unbind Waves VAO
		this->mOpenGL->glBindVertArray( 0 );
	}

	lStatus = this->mOpenGL->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	// Present the rendered scene to the screen.
	this->mOpenGL->StopRender();

	return true;
}

/*********************************************************************************************/
VVOID GraphicsClass::BuildGeometryBuffers()
{
	MeshData lBox;

	GeometryHelper::CreateBox( 1.0f, 1.0f, 1.0f, lBox );

	this->mBoxVertexOffset = 0;
	this->mBoxIndexCount   = (UBIGINT)lBox.Indices.size();
	this->mBoxIndexOffset  = 0;

	UBIGINT lTotalVertexCount = (UBIGINT)lBox.Vertices.size();

	UBIGINT lTotalIndexCount = this->mBoxIndexCount;

	//
	// Extract the vertex elements we are interested in and pack the
	// vertices of all the meshes into one vertex buffer.
	//
	vector<SimpleVertex> lVertices( lTotalVertexCount );

	UBIGINT lCounter = 0;
	for( UBIGINT lCurr = 0; lCurr < (UBIGINT)lBox.Vertices.size(); lCurr++, lCounter++ )
	{
		lVertices[ lCounter ].Position = lBox.Vertices[ lCurr ].Position;
		lVertices[ lCounter ].Normal   = lBox.Vertices[ lCurr ].Normal;
		lVertices[ lCounter ].TexCoord = lBox.Vertices[ lCurr ].TexCoord;
	}

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mBoxVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mBoxVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mBoxVB );

	// Bind the vertex buffer and load the vertex (position, texture, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mBoxVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, lTotalVertexCount * sizeof(SimpleVertex), &lVertices[0], GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::PosNormalTexLayout()->SetVertexLayout( this->mBoxVB );

	vector<UBIGINT> lIndices;
	lIndices.insert( lIndices.end(), lBox.Indices.begin(), lBox.Indices.end() );

	// Generate a index buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mBoxIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mBoxIB );
	this->mOpenGL->glBufferData( GL_ELEMENT_ARRAY_BUFFER, lTotalIndexCount * sizeof(UBIGINT), &lIndices[0], GL_STATIC_DRAW );

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Box geometry Failed !!! ");
	}

	this->mOpenGL->glBindVertArray( 0 );
}

/*********************************************************************************************/
IOcelotCameraController* GraphicsClass::Controller()
{
	return this->mController;
}
