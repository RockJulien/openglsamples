////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _GRAPHICSCLASS_H_
#define _GRAPHICSCLASS_H_

///////////////////////
// INCLUDES //
///////////////////////
#include "GDevice.h"
#include "Camera\OcelotCamera.h"
#include "Camera\OcelotCameraFactory.h"
#include "GeometryHelper.h"
#include "OcelotLight.h"
#include "Material.h"
#include "Effects\Effects.h"
#include "Effects\InputLayouts.h"
#include "TextureManager\TextureManager.h"

using namespace Ocelot;

/////////////
// GLOBALS //
/////////////
const BBOOL  FULL_SCREEN   = false;
const BBOOL  VSYNC_ENABLED = true;
const FFLOAT SCREEN_DEPTH  = 1.0f;
const FFLOAT SCREEN_NEAR   = 0.1f;

////////////////////////////////////////////////////////////////////////////////
// Class name: GraphicsClass
////////////////////////////////////////////////////////////////////////////////
class GraphicsClass
{
public:

	// Constructor & Destructor
	GraphicsClass();
	~GraphicsClass();

	// Methods
	BBOOL Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight);
	VVOID OnResize(BIGINT pClientWidth, BIGINT pClientHeight);
	VVOID Shutdown();
	BBOOL Update(FFLOAT pDeltaTime);
	BBOOL Render();

	// Accessors
	IOcelotCameraController* Controller();

private:

	// Forbidden
	GraphicsClass(const GraphicsClass&);

	// Private Methods
	VVOID BuildGeometryBuffers();

private:

	// Attributes
	// Graphic device
	OpenGLClass*             mOpenGL;

	// Camera
	OcelotCamera*            mCamera;
	IOcelotCameraController* mController;
	
	// Buffers
	UBIGINT					 mBoxVAO;
	UBIGINT					 mBoxVB;
	UBIGINT					 mBoxIB;

	// Lights
	Ocelot::OcelotParallelLight mDirLights[3];
	Material				    mBoxMat;

	// Transforms
	// Define transformations from local spaces to world space.
	Matrix4x4		 mBoxWorld;
	Matrix4x4		 mTexTransform;

	// Index and Offset Count
	BIGINT			 mBoxVertexOffset;
	UBIGINT			 mBoxIndexOffset;
	UBIGINT			 mBoxIndexCount;

	// Texture Manager
	TextureManager   mTexManager;
	UBIGINT			 mDiffuseMapRV;
	UBIGINT			 mDiffuseTexUnit;

	// Effects
	EffectTechnique* mLight2TexTech;
};

#endif