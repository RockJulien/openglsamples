#include "TextureHandler.h"
#include "TGALoader.h"
#include "DDSLoader.h"
#include <string>

// Namespaces used
using namespace std;

#define MakeFourCC(ch0, ch1, ch2, ch3)                              \
				  ((ULINT)(UCCHAR)(ch0) | ((ULINT)(UCCHAR)(ch1) << 8) |   \
                  ((ULINT)(UCCHAR)(ch2) << 16) | ((ULINT)(UCCHAR)(ch3) << 24 ))

#define FOURCC_DXT1	MakeFourCC('D', 'X', 'T', '1')
#define FOURCC_DXT3	MakeFourCC('D', 'X', 'T', '3')
#define FOURCC_DXT5	MakeFourCC('D', 'X', 'T', '5')

/****************************************************************************************************/
BIGINT TextureHandler::LoadBMP(GDevice* device, const CCHAR* textName, BBOOL wrap)
{
	return -1;
}

/****************************************************************************************************/
BIGINT TextureHandler::LoadPNG(GDevice* device, const CCHAR* textName, BBOOL wrap)
{
	return -1;
}

/****************************************************************************************************/
BIGINT TextureHandler::LoadJPG(GDevice* device, const CCHAR* textName, BBOOL wrap)
{
	return -1;
}

/****************************************************************************************************/
BIGINT TextureHandler::LoadDDS(GDevice* device, const CCHAR* pFilename, BBOOL wrap)
{
	UBIGINT lTextureId;

	// Check if a name is passed.
	if(pFilename == NULL)
	{
		return -1;
	}

	DDSLoader lLoader;
	DDSImage* lImage = lLoader.LoadDDS( pFilename );

	if( lImage == NULL )
	{
		return -1;
	}

	// Generate an ID for the texture.
	glGenTextures(1, &lTextureId);

	// Bind the texture as a 2D texture.
	glBindTexture( GL_TEXTURE_2D, lTextureId );

	// Set the texture color to either wrap around or clamp to the edge.
	if(wrap)
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}
	else
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	}

	// Set the texture filtering.
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	BIGINT lMipMapLevels = lImage->GetMipMapLevels();
	BIGINT lWidth  = lImage->GetWidth();
	BIGINT lHeight = lImage->GetHeight();

	if
		( lMipMapLevels > 0 )
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0 );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, lMipMapLevels - 1 );
	}

	glPixelStorei( GL_UNPACK_ROW_LENGTH, 0 );
	glPixelStorei( GL_UNPACK_SKIP_ROWS, 0 );
	glPixelStorei( GL_UNPACK_SKIP_PIXELS, 0 );

	/* load the mipmaps */
	for( BIGINT lCurrLevel = 0; lCurrLevel < lMipMapLevels; lCurrLevel++ )
	{
		// Keep size at a minimum
		if( lWidth == 0 ) 
		{
			lWidth = 1;
		}
		if( lHeight == 0 ) 
		{
			lHeight = 1;
		}

		if
			( lImage->GetPixels( lCurrLevel ) )
		{
			if
				( lImage->GetFormat() == DDS_FORMAT_RGBA8 )
			{
				glTexImage2D( GL_TEXTURE_2D, 
							  lCurrLevel, 
							  GL_RGBA, 
							  lWidth, 
							  lHeight, 
							  0, 
							  GL_RGBA, 
							  GL_UNSIGNED_BYTE, 
							  lImage->GetPixels( lCurrLevel ) );
			} 
			else
			{
				// Compressed formats
				if
					( lImage->GetFormat() == DDS_FORMAT_YCOCG )
				{
					// DXT1/3/5
					this->mDevice->glCompressTextImg2D( GL_TEXTURE_2D, 
														lCurrLevel, 
														lImage->GetInternalFormat(), 
														lWidth, 
														lHeight, 
														0, 
														lImage->GetBytes( lCurrLevel ), 
														lImage->GetPixels( lCurrLevel ) );
				} 
				else
				{
					printf_s( "DDS:CreateTexture; not DDS_FORMAT_YCOCG for mipmap level %d", lCurrLevel );
				}

				// Verify that compression took place
				GLenum lError = glGetError();
				if( lError != GL_NO_ERROR )
				{
					throw exception(" Create compressed texture Failure !!! ");
				}

				GLint lParam = 0;
				glGetTexLevelParameteriv( GL_TEXTURE_2D, lCurrLevel, GL_TEXTURE_COMPRESSED, &lParam );
				if
					( lParam == 0 )
				{
					printf_s( "DDS:LoadDDS(); mipmap level %d indicated compression failed", lCurrLevel );
				}
			}

			GLint param=0;
			glGetTexLevelParameteriv( GL_TEXTURE_2D, lCurrLevel, GL_TEXTURE_INTERNAL_FORMAT, &param);
			
		} 
		else
		{
			printf_s( "DDS:LoadDDS(); missing pixels for mipmap level %d", lCurrLevel );
		}

		// Next level uses less levels
		lWidth /= 2;
		lHeight /= 2;
	}

	// Release the dds image data.
	DeletePointer( lImage );

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Create compressed texture Failure !!! ");
	}

	return lTextureId;
}

/****************************************************************************************************/
BIGINT TextureHandler::LoadTGA(GDevice* device, const CCHAR* textName, BBOOL wrap)
{
	UBIGINT lTextureId;

	TGALoader lLoader;
	GenericImage* lImage = lLoader.ReadTGA( textName );

	// Generate an ID for the texture.
	glGenTextures(1, &lTextureId);

	// Bind the texture as a 2D texture.
	glBindTexture(GL_TEXTURE_2D, lTextureId);

	// Load the image data into the texture unit.
	glTexImage2D( GL_TEXTURE_2D, 
				  0, 
				  lImage->Format(), 
				  lImage->Width(), 
				  lImage->Height(), 
				  0, 
				  lImage->Format(), 
				  GL_UNSIGNED_BYTE, 
				  lImage->Pixels() );

	// Set the texture color to either wrap around or clamp to the edge.
	if(wrap)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	}

	// Set the texture filtering.
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY, 4.0f);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	// Generate mipmaps for the texture.
	device->glGenerateMipMap(GL_TEXTURE_2D);

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Random vector texture Failure !!! ");
	}

	// Release the targa image data.
	lImage->Release();
	DeletePointer( lImage );

	return lTextureId;
}

/****************************************************************************************************/
TextureHandler::TextureHandler(GDevice* pDevice) :
mDevice(pDevice)
{
	
}

/****************************************************************************************************/
TextureHandler::~TextureHandler()
{

}

/****************************************************************************************************/
BIGINT TextureHandler::Load( const CCHAR* textName, ExtType type, BBOOL wrap)
{
	BIGINT lId = -1;

	switch (type)
	{
	case eBMP:
		{
			lId = LoadBMP(this->mDevice, textName, wrap);
		}
		break;
	case ePNG:
		{
			lId = LoadPNG(this->mDevice, textName, wrap);
		}
		break;
	case eJPG:
		{
			lId = LoadJPG(this->mDevice, textName, wrap);
		}
		break;
	case eDDS:
		{
			lId = LoadDDS(this->mDevice, textName, wrap);
		}
		break;
	case eTGA:
		{
			lId = LoadTGA(this->mDevice, textName, wrap);
		}
		break;
	default:
		return -1;
		break;
	}

	return lId;
}
