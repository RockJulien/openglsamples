////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _GRAPHICSCLASS_H_
#define _GRAPHICSCLASS_H_

///////////////////////
// INCLUDES //
///////////////////////
#include "GDevice.h"
#include "Camera\OcelotCamera.h"
#include "Camera\OcelotCameraFactory.h"
#include "GeometryHelper.h"
#include "OcelotLight.h"
#include "Material.h"
#include "Effects\Effects.h"
#include "Effects\InputLayouts.h"
#include "TextureManager\TextureManager.h"

using namespace Ocelot;

/////////////
// GLOBALS //
/////////////
const BBOOL  FULL_SCREEN   = false;
const BBOOL  VSYNC_ENABLED = true;
const FFLOAT SCREEN_DEPTH  = 1.0f;
const FFLOAT SCREEN_NEAR   = 0.1f;

////////////////////////////////////////////////////////////////////////////////
// Class name: GraphicsClass
////////////////////////////////////////////////////////////////////////////////
class GraphicsClass
{
public:

	// Constructor & Destructor
	GraphicsClass();
	~GraphicsClass();

	// Methods
	BBOOL Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight);
	VVOID OnResize(BIGINT pClientWidth, BIGINT pClientHeight);
	VVOID Shutdown();
	BBOOL Update(FFLOAT pDeltaTime);
	BBOOL Render();

	// Accessors
	IOcelotCameraController* Controller();
	VVOID                    SetLightCount(UBIGINT pCount);

private:

	// Forbidden
	GraphicsClass(const GraphicsClass&);

	// Private Methods
	VVOID BuildShapeGeometryBuffers();
	VVOID BuildSkullGeometryBuffers();

private:

	// Attributes
	// Graphic device
	OpenGLClass*				mOpenGL;

	// Camera
	OcelotCamera*				mCamera;
	IOcelotCameraController*	mController;

	// Screen bounds
	BIGINT						mClientWidth;
	BIGINT						mClientHeight;
	
	// Buffers
	UBIGINT						mShapesVAO;
	UBIGINT						mShapesVB;
	UBIGINT						mShapesIB;
	UBIGINT						mSkullVAO;
	UBIGINT						mSkullVB;
	UBIGINT						mSkullIB;

	// Lights
	Ocelot::OcelotParallelLight mDirLights[3];
	Material				    mGridMat;
	Material					mBoxMat;
	Material					mCylinderMat;
	Material					mSphereMat;
	Material					mSkullMat;

	// Transforms
	// Define transformations from local spaces to world space.
	Matrix4x4					mSphereWorld[10];
	Matrix4x4					mCylWorld[10];
	Matrix4x4					mBoxWorld;
	Matrix4x4					mGridWorld;
	Matrix4x4					mSkullWorld;

	// Index and Offset Count
	BIGINT						mBoxVertexOffset;
	BIGINT						mGridVertexOffset;
	BIGINT						mSphereVertexOffset;
	BIGINT						mCylinderVertexOffset;

	UBIGINT						mBoxIndexOffset;
	UBIGINT						mGridIndexOffset;
	UBIGINT						mSphereIndexOffset;
	UBIGINT						mCylinderIndexOffset;

	UBIGINT						mBoxIndexCount;
	UBIGINT						mGridIndexCount;
	UBIGINT						mSphereIndexCount;
	UBIGINT						mCylinderIndexCount;

	UBIGINT						mSkullIndexCount;

	UBIGINT						mLightCount;

	// Texture Manager
	TextureManager				mTexManager;
	UBIGINT						mFloorTexRV;
	UBIGINT						mStoneTexRV;
	UBIGINT						mBrickTexRV;

	UBIGINT						mDiffuseTexUnit;

	// Effects
	EffectTechnique* mLight1Tech;
	EffectTechnique* mLight1TexTech;
	EffectTechnique* mLight2Tech;
	EffectTechnique* mLight2TexTech;
	EffectTechnique* mLight3Tech;
	EffectTechnique* mLight3TexTech;

	EffectTechnique* mActiveTexTech;
	EffectTechnique* mActiveSkullTech;
};

#endif