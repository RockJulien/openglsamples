#include "Wave.h"
#include <algorithm>

// Namespaces used

/********************************************************************************************/
Wave::Wave() :
mNumRows(0), mNumCols(0), mVertexCount(0), mTriangleCount(0), 
mK1(0.0f), mK2(0.0f), mK3(0.0f), mTimeStep(0.0f), mSpatialStep(0.0f),
mPrevSolution(NULL), mCurrSolution(NULL), mNormals(NULL), mTangentX(NULL)
{

}

/********************************************************************************************/
Wave::~Wave()
{
	delete[] mPrevSolution;
	delete[] mCurrSolution;
	delete[] mNormals;
	delete[] mTangentX;
}

/********************************************************************************************/
VVOID Wave::Init(UBIGINT pRowCount, UBIGINT pColCount, FFLOAT pDx, FFLOAT pDeltaTime, FFLOAT pSpeed, FFLOAT pDamping)
{
	this->mNumRows  = pRowCount;
	this->mNumCols  = pColCount;

	this->mVertexCount   = pRowCount * pColCount;
	this->mTriangleCount = (pRowCount - 1) * (pColCount - 1) * 2;

	this->mTimeStep    = pDeltaTime;
	this->mSpatialStep = pDx;

	FFLOAT d = pDamping * pDeltaTime + 2.0f;
	FFLOAT e = (pSpeed * pSpeed) * (pDeltaTime * pDeltaTime) / (pDx * pDx);
	this->mK1 = (pDamping * pDeltaTime - 2.0f) / d;
	this->mK2 = (4.0f - 8.0f * e) / d;
	this->mK3 = (2.0f * e) / d;

	// In case Init() called again.
	delete[] mPrevSolution;
	delete[] mCurrSolution;
	delete[] mNormals;
	delete[] mTangentX;

	this->mPrevSolution = new Vector3[ pRowCount * pColCount ];
	this->mCurrSolution = new Vector3[ pRowCount * pColCount ];
	this->mNormals      = new Vector3[ pRowCount * pColCount ];
	this->mTangentX     = new Vector3[ pRowCount * pColCount ];

	// Generate grid vertices in system memory.
	FFLOAT lHalfWidth = (pColCount - 1) * pDx * 0.5f;
	FFLOAT lHalfDepth = (pRowCount - 1) * pDx * 0.5f;
	for( UBIGINT lCurrRow = 0; lCurrRow < pRowCount; lCurrRow++ )
	{
		FFLOAT lZ = lHalfDepth - lCurrRow * pDx;
		for( UBIGINT lCurrCol = 0; lCurrCol < pColCount; lCurrCol++ )
		{
			FFLOAT lX = -lHalfWidth + lCurrCol * pDx;

			this->mPrevSolution[ lCurrRow * pColCount + lCurrCol ] = Vector3( lX, 0.0f, lZ );
			this->mCurrSolution[ lCurrRow * pColCount + lCurrCol ] = Vector3( lX, 0.0f, lZ );
			this->mNormals[ lCurrRow * pColCount + lCurrCol ]      = Vector3( 0.0f, 1.0f, 0.0f );
			this->mTangentX[ lCurrRow * pColCount + lCurrCol ]     = Vector3( 1.0f, 0.0f, 0.0f );
		}
	}
}

/********************************************************************************************/
VVOID Wave::Update(FFLOAT pDeltaTime)
{
	static FFLOAT sTime = 0;

	// Accumulate time.
	sTime += pDeltaTime;

	// Only update the simulation at the specified time step.
	if( sTime >= this->mTimeStep )
	{
		// Only update interior points; we use zero boundary conditions.
		for( UBIGINT lCurrRow = 1; lCurrRow < this->mNumRows - 1; lCurrRow++ )
		{
			for( UBIGINT lCurrCol = 1; lCurrCol < this->mNumCols - 1; lCurrCol++ )
			{
				// After this update we will be discarding the old previous
				// buffer, so overwrite that buffer with the new update.
				// Note how we can do this inplace (read/write to same element) 
				// because we won'sTime need prev_ij again and the assignment happens last.

				// Note lCurrCol indexes x and lCurrRow indexes z: h(x_j, z_i, t_k)
				// Moreover, our +z axis goes "down"; this is just to 
				// keep consistent with our row indices going down.

				this->mPrevSolution[ lCurrRow * this->mNumCols + lCurrCol ].y( this->mK1 * this->mPrevSolution[ lCurrRow * this->mNumCols + lCurrCol ].getY() +
																			   this->mK2 * this->mCurrSolution[ lCurrRow * this->mNumCols + lCurrCol ].getY() +
																			   this->mK3 * ( this->mCurrSolution[ (lCurrRow + 1) * this->mNumCols + lCurrCol ].getY() + 
																							 this->mCurrSolution[ (lCurrRow - 1) * this->mNumCols + lCurrCol ].getY() + 
																							 this->mCurrSolution[ lCurrRow * this->mNumCols + lCurrCol + 1 ].getY() + 
																							 this->mCurrSolution[ lCurrRow * this->mNumCols + lCurrCol - 1 ].getY() ) );
			}
		}

		// We just overwrote the previous buffer with the new data, so
		// this data needs to become the current solution and the old
		// current solution becomes the new previous solution.
		std::swap( this->mPrevSolution, this->mCurrSolution);

		sTime = 0.0f; // reset time

		//
		// Compute normals using finite difference scheme.
		//
		for( UBIGINT lCurrRow = 1; lCurrRow < this->mNumRows - 1; lCurrRow++ )
		{
			for( UBIGINT lCurrCol = 1; lCurrCol < this->mNumCols - 1; lCurrCol++ )
			{
				FFLOAT l = this->mCurrSolution[ lCurrRow * this->mNumCols + lCurrCol - 1 ].getY();
				FFLOAT r = this->mCurrSolution[ lCurrRow * this->mNumCols + lCurrCol + 1 ].getY();
				FFLOAT sTime = this->mCurrSolution[ (lCurrRow - 1 ) * this->mNumCols + lCurrCol ].getY();
				FFLOAT b = this->mCurrSolution[ (lCurrRow + 1 ) * this->mNumCols + lCurrCol ].getY();
				this->mNormals[ lCurrRow * this->mNumCols + lCurrCol ].x( -r + l );
				this->mNormals[ lCurrRow * this->mNumCols + lCurrCol ].y( 2.0f * this->mSpatialStep );
				this->mNormals[ lCurrRow * this->mNumCols + lCurrCol ].z( b - sTime );

				Vector3 lNormal = this->mNormals[ lCurrRow * this->mNumCols + lCurrCol ];
				this->mNormals[ lCurrRow * this->mNumCols + lCurrCol ] = lNormal.Vec3Normalise();

				this->mTangentX[ lCurrRow * this->mNumCols + lCurrCol] = Vector3( 2.0f * this->mSpatialStep, r - l, 0.0f );
				Vector3 lTangentX = this->mTangentX[ lCurrRow * this->mNumCols + lCurrCol ];
				this->mTangentX[ lCurrRow * this->mNumCols + lCurrCol ] = lTangentX.Vec3Normalise();
			}
		}
	}
}

/********************************************************************************************/
VVOID Wave::Disturb(UBIGINT pI, UBIGINT pJ, FFLOAT pMagnitude)
{
	if( pI > 1 && 
		pI < this->mNumRows - 2 || 
		pJ > 1 && 
		pJ < this->mNumCols - 2 )
	{
		FFLOAT lHalfMag = 0.5f * pMagnitude;

		// Disturb the ijth vertex height and its neighbors.
		this->mCurrSolution[ pI * this->mNumCols + pJ ].y( this->mCurrSolution[ pI * this->mNumCols + pJ ].getY() + pMagnitude );
		this->mCurrSolution[ pI * this->mNumCols + pJ + 1 ].y( this->mCurrSolution[ pI * this->mNumCols + pJ + 1 ].getY() + lHalfMag );
		this->mCurrSolution[ pI * this->mNumCols + pJ - 1 ].y( this->mCurrSolution[ pI * this->mNumCols + pJ - 1 ].getY() + lHalfMag );
		this->mCurrSolution[ (pI + 1) * this->mNumCols + pJ ].y( this->mCurrSolution[ (pI + 1) * this->mNumCols + pJ ].getY() + lHalfMag );
		this->mCurrSolution[ (pI - 1) * this->mNumCols + pJ ].y( this->mCurrSolution[ (pI - 1) * this->mNumCols + pJ ].getY() + lHalfMag );
	}
}

/********************************************************************************************/
const Vector3& Wave::operator[] (BIGINT pIndex) const
{
	return this->mCurrSolution[ pIndex ];
}

/********************************************************************************************/
const Vector3& Wave::Normal(BIGINT pIndex) const
{
	return this->mNormals[ pIndex ];
}

/********************************************************************************************/
const Vector3& Wave::TangentX(BIGINT pIndex) const
{
	return this->mTangentX[ pIndex ];
}

/********************************************************************************************/
UBIGINT Wave::RowCount() const
{
	return this->mNumRows;
}

/********************************************************************************************/
UBIGINT Wave::ColumnCount() const
{
	return this->mNumCols;
}

/********************************************************************************************/
UBIGINT Wave::VertexCount() const
{
	return this->mVertexCount;
}

/********************************************************************************************/
UBIGINT Wave::TriangleCount() const
{
	return this->mTriangleCount;
}

/********************************************************************************************/
FFLOAT  Wave::Width() const
{
	return this->mNumCols * this->mSpatialStep;
}

/********************************************************************************************/
FFLOAT  Wave::Depth() const
{
	return this->mNumRows * this->mSpatialStep;
}
