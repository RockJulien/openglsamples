////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "graphicsclass.h"
#include "TGALoader.h"
#include <fstream>
#include "MathUtilities.h"
#include <time.h>
#include "systemclass.h"
#include "MeshData.h"

using namespace std;
using namespace Ocelot;

/*********************************************************************************************/
GraphicsClass::GraphicsClass() :
mOpenGL(NULL), mCamera(NULL), mController(NULL), 
mShapesVAO(0), mShapesVB(0), mShapesIB(0), mSkullVAO(0), 
mSkullVB(0), mSkullIB(0), mBoxVertexOffset(0), mGridVertexOffset(0), 
mSphereVertexOffset(0), mCylinderVertexOffset(0), mBoxIndexOffset(0), 
mGridIndexOffset(0), mSphereIndexOffset(0), mCylinderIndexOffset(0), 
mBoxIndexCount(0), mGridIndexCount(0), mSphereIndexCount(0), 
mCylinderIndexCount(0), mSkullIndexCount(0), mLightCount(3),
mLight1Tech(NULL), mLight1TexTech(NULL), mLight2Tech(NULL), 
mLight2TexTech(NULL), mLight3Tech(NULL), mLight3TexTech(NULL), 
mActiveTexTech(NULL), mActiveSkullTech(NULL)
{
	srand((UBIGINT)time(NULL));

	// Initialize the camera.
	OcelotCameraInfo lCamInfo( Vector3( 0.0f, 2.0f, -15.0f ), 
							   Vector3( 0.0f, 0.0f, 1.0f ), 
							   (FFLOAT)(800 / 600), 
							   1000.0f );
	lCamInfo.SetNear(1.0f);
	this->mCamera = new OcelotCamera( lCamInfo );

	// And create the controller
	this->mController = ControlFact->createController( this->mCamera, 
													   CAMERA_CONTROLLER_FLYER );
	// Initialize meshes matrices
	Matrix4x4 lBoxScale;
	Matrix4x4 lBoxOffset;
	lBoxScale.Matrix4x4Scaling( lBoxScale, 3.0f, 1.0f, 3.0f );
	lBoxOffset.Matrix4x4Translation( lBoxOffset, 0.0f, 0.5f, 0.0f );
	this->mBoxWorld.Matrix4x4Mutliply( this->mBoxWorld, lBoxScale, lBoxOffset );

	Matrix4x4 lSkullScale;
	Matrix4x4 lSkullOffset;
	lSkullScale.Matrix4x4Scaling( lSkullScale, 0.5f, 0.5f, 0.5f );
	lSkullOffset.Matrix4x4Translation( lSkullOffset, 0.0f, 1.0f, 0.0f );
	this->mSkullWorld.Matrix4x4Mutliply( this->mSkullWorld, lSkullScale, lSkullOffset );

	for
		( BIGINT lCurrMatrix = 0; lCurrMatrix < 5; lCurrMatrix++ )
	{
		Matrix4x4 lHandle;
		lHandle.Matrix4x4Translation( this->mCylWorld[ lCurrMatrix * 2 + 0 ], -5.0f, 1.5f, -10.0f + lCurrMatrix * 5.0f );
		lHandle.Matrix4x4Translation( this->mCylWorld[ lCurrMatrix * 2 + 1 ], +5.0f, 1.5f, -10.0f + lCurrMatrix * 5.0f );

		lHandle.Matrix4x4Translation( this->mSphereWorld[ lCurrMatrix * 2 + 0 ], -5.0f, 3.5f, -10.0f + lCurrMatrix * 5.0f );
		lHandle.Matrix4x4Translation( this->mSphereWorld[ lCurrMatrix * 2 + 1 ], +5.0f, 3.5f, -10.0f + lCurrMatrix * 5.0f );
	}

	// Initialize the light object(s).
	this->mDirLights[0].SetAmbient( Color( 0.2f, 0.2f, 0.2f, 1.0f ) );
	this->mDirLights[0].SetDiffuse( Color( 0.5f, 0.5f, 0.5f, 1.0f ) );
	this->mDirLights[0].SetSpecular( Color( 0.5f, 0.5f, 0.5f, 1.0f ) );
	this->mDirLights[0].SetDirection( Vector3( 0.57735f, -0.57735f, 0.57735f ) );

	this->mDirLights[1].SetAmbient( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[1].SetDiffuse( Color( 0.20f, 0.20f, 0.20f, 1.0f ) );
	this->mDirLights[1].SetSpecular( Color( 0.25f, 0.25f, 0.25f, 1.0f ) );
	this->mDirLights[1].SetDirection( Vector3( -0.57735f, -0.57735f, 0.57735f ) );

	this->mDirLights[2].SetAmbient( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[2].SetDiffuse( Color( 0.2f, 0.2f, 0.2f, 1.0f ) );
	this->mDirLights[2].SetSpecular( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[2].SetDirection( Vector3( 0.0f, -0.707f, -0.707f ) );

	// Initialize materials
	this->mGridMat.Ambient  = Color( 0.8f, 0.8f, 0.8f, 1.0f );
	this->mGridMat.Diffuse  = Color( 0.8f, 0.8f, 0.8f, 1.0f );
	this->mGridMat.Specular = Color( 0.8f, 0.8f, 0.8f, 16.0f );

	this->mCylinderMat.Ambient  = Color( 1.0f, 1.0f, 1.0f, 1.0f );
	this->mCylinderMat.Diffuse  = Color( 1.0f, 1.0f, 1.0f, 1.0f );
	this->mCylinderMat.Specular = Color( 0.8f, 0.8f, 0.8f, 16.0f );

	this->mSphereMat.Ambient  = Color( 0.6f, 0.8f, 0.9f, 1.0f );
	this->mSphereMat.Diffuse  = Color( 0.6f, 0.8f, 0.9f, 1.0f );
	this->mSphereMat.Specular = Color( 0.9f, 0.9f, 0.9f, 16.0f );

	this->mBoxMat.Ambient  = Color( 1.0f, 1.0f, 1.0f, 1.0f );
	this->mBoxMat.Diffuse  = Color( 1.0f, 1.0f, 1.0f, 1.0f );
	this->mBoxMat.Specular = Color( 0.8f, 0.8f, 0.8f, 16.0f );

	this->mSkullMat.Ambient  = Color( 0.4f, 0.4f, 0.4f, 1.0f );
	this->mSkullMat.Diffuse  = Color( 0.8f, 0.8f, 0.8f, 1.0f );
	this->mSkullMat.Specular = Color( 0.8f, 0.8f, 0.8f, 16.0f );

	this->mDiffuseTexUnit = 1;
}

/*********************************************************************************************/
GraphicsClass::~GraphicsClass()
{
	
}

/*********************************************************************************************/
BBOOL GraphicsClass::Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight)
{
	BBOOL lResult = false;

	// Store a pointer to the OpenGL class object.
	this->mOpenGL = pGDevice;
	this->mClientWidth  = pClientWidth;
	this->mClientHeight = pClientHeight;
	
	Effects::InitializeAll( this->mOpenGL );
	InputLayouts::InitializeAll( this->mOpenGL );

	// Initialize the texture manager
	this->mTexManager.Initialize( this->mOpenGL );

	// Init the diffuse map to apply to the mesh.
	ExtType lType = ExtType::eDDS;

	this->mFloorTexRV = this->mTexManager.CreateTexture( "Textures/floor.dds", lType );
	this->mStoneTexRV = this->mTexManager.CreateTexture( "Textures/stone.dds", lType );
	this->mBrickTexRV = this->mTexManager.CreateTexture( "Textures/bricks.dds", lType );

	// Initialize the technique(s).
	vector<string> lIncludes;
	lIncludes.push_back("Shaders/LightHelper.helper"); // Need light include
	StageDescription lDescription;
	lDescription.SetVertexStagePath("Shaders/Basic.vs");
	lDescription.SetPixelStagePath("Shaders/Basic.ps");
	this->mLight1Tech = Effects::BasicFX()->Light1Tech( lDescription, lIncludes );
	this->mLight1Tech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight1TexTech = Effects::BasicFX()->Light1TexTech( lDescription, lIncludes );
	this->mLight1TexTech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight2Tech = Effects::BasicFX()->Light2Tech( lDescription, lIncludes );
	this->mLight2Tech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight2TexTech = Effects::BasicFX()->Light2TexTech( lDescription, lIncludes );
	this->mLight2TexTech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight3Tech = Effects::BasicFX()->Light3Tech( lDescription, lIncludes );
	this->mLight3Tech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight3TexTech = Effects::BasicFX()->Light3TexTech( lDescription, lIncludes );
	this->mLight3TexTech->SetLayout( InputLayouts::PosNormalTexLayout() );

	this->BuildShapeGeometryBuffers();
	this->BuildSkullGeometryBuffers();

	return true;
}

/*********************************************************************************************/
VVOID GraphicsClass::OnResize(BIGINT pClientWidth, BIGINT pClientHeight)
{
	this->mClientWidth  = pClientWidth;
	this->mClientHeight = pClientHeight;

	FFLOAT lAspect = (FFLOAT)pClientWidth / pClientHeight;

	// refresh the aspect of the camera if the
	// window is resized.
	OcelotCameraInfo lInfo = this->mCamera->Info();
	if
		( this->mCamera != NULL )
	{
		lInfo.SetAspect( lAspect );  // aspect ratio
		this->mCamera->SetInfo( lInfo );
	}

	glViewport( 0, 0, pClientWidth, pClientHeight );
}

/*********************************************************************************************/
VVOID GraphicsClass::Shutdown()
{
	DeletePointer( this->mCamera );
	DeletePointer( this->mController );
	DeletePointer( this->mLight1Tech );
	DeletePointer( this->mLight1TexTech );
	DeletePointer( this->mLight2Tech );
	DeletePointer( this->mLight2TexTech );
	DeletePointer( this->mLight3Tech );
	DeletePointer( this->mLight3TexTech );

	glDeleteTextures( 1, &this->mFloorTexRV );
	glDeleteTextures( 1, &this->mStoneTexRV );
	glDeleteTextures( 1, &this->mBrickTexRV );

	// Release VAOs, VBOs and IBOs
	// Release Land buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mShapesVB );
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mShapesIB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mShapesVAO );

	// Release Wave buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mSkullVB );
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mSkullIB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mSkullVAO );

	Effects::DestroyAll();
	InputLayouts::DestroyAll();
}

/*********************************************************************************************/
BBOOL GraphicsClass::Update(FFLOAT pDeltaTime)
{
	// Update the camera.
	this->mController->update( pDeltaTime );

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Start Update Failure !!! ");
	}

	return true;
}

/*********************************************************************************************/
BBOOL GraphicsClass::Render()
{
	// Clear the buffers to begin the scene.
	this->mOpenGL->StartRender( true, true );

	glClearColor( 0.75f, 0.75f, 0.75f, 1.0f ); // Silver
	glClearDepth( 1.0f );
	glClearStencil( 0 );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

	Matrix4x4 lView     = mCamera->View();
	Matrix4x4 lProj     = mCamera->Proj();
	Matrix4x4 lViewProj = lView * lProj;

	OcelotCameraInfo lInfo = this->mCamera->Info();

	GLenum lStatus = this->mOpenGL->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	// Figure out which technique to use.  Skull does not have texture coordinates,
	// so we need a separate technique for it.
	switch
		( this->mLightCount )
	{
	case 1:
		{
			this->mActiveTexTech   = this->mLight1TexTech;
			this->mActiveSkullTech = this->mLight1Tech;
		}
		break;
	case 2:
		{
			this->mActiveTexTech   = this->mLight2TexTech;
			this->mActiveSkullTech = this->mLight2Tech;
		}
		break;
	case 3:
		{
			this->mActiveTexTech   = this->mLight3TexTech;
			this->mActiveSkullTech = this->mLight3Tech;
		}
		break;
	default:
		{
			this->mActiveTexTech   = this->mLight1TexTech;
			this->mActiveSkullTech = this->mLight1Tech;
		}
	}

	if
		( Effects::BasicFX() != NULL )
	{
		Effects::BasicFX()->MakeCurrent( this->mActiveTexTech );

		Effects::BasicFX()->SetEyePosW( lInfo.Eye() );
		Effects::BasicFX()->SetDirLights( this->mDirLights, this->mLightCount );

		// Draw the grid.
		Matrix4x4 lWorld = this->mGridWorld;
		Matrix4x4 lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		Matrix4x4 lWorldViewProj = lWorld * lViewProj;
		Matrix4x4 lScalingGrid;
		lScalingGrid.Matrix4x4Scaling( lScalingGrid, 6.0f, 8.0f, 1.0f );

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetTexTransform( lScalingGrid );
		Effects::BasicFX()->SetMaterial( this->mGridMat );

		this->mOpenGL->glActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		glBindTexture( GL_TEXTURE_2D, this->mFloorTexRV );

		GLenum lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Graphic Rendering Failure !!! ");
		}

		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		// Bind the vertex array object containing the geometry info.
		this->mOpenGL->glBindVertArray( this->mShapesVAO );

		this->mOpenGL->glDrawElementsBaseVertex( GL_TRIANGLES, this->mGridIndexCount, GL_UNSIGNED_INT, (VVOID*)(this->mGridIndexOffset * sizeof(UBIGINT)), this->mGridVertexOffset );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Graphic Rendering Failure !!! ");
		}

		// Draw the box.
		lWorld = this->mBoxWorld;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetTexTransform( Matrix4x4() );
		Effects::BasicFX()->SetMaterial( this->mBoxMat );

		this->mOpenGL->glActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		glBindTexture( GL_TEXTURE_2D, this->mStoneTexRV );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Graphic Rendering Failure !!! ");
		}

		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		// Bind the vertex array object containing the geometry info.
		this->mOpenGL->glBindVertArray( this->mShapesVAO );

		this->mOpenGL->glDrawElementsBaseVertex( GL_TRIANGLES, this->mBoxIndexCount, GL_UNSIGNED_INT, (VVOID*)(this->mBoxIndexOffset * sizeof(UBIGINT)), this->mBoxVertexOffset );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Graphic Rendering Failure !!! ");
		}

		// Draw the Cylinders.
		for
			( BIGINT lCurrCyl = 0; lCurrCyl < 10; lCurrCyl++ )
		{
			lWorld = this->mCylWorld[ lCurrCyl ];
			lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
			lWorldViewProj = lWorld * lViewProj;

			Effects::BasicFX()->SetWorld( lWorld );
			Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
			Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
			Effects::BasicFX()->SetTexTransform( Matrix4x4() );
			Effects::BasicFX()->SetMaterial( this->mCylinderMat );

			this->mOpenGL->glActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
			glBindTexture( GL_TEXTURE_2D, this->mBrickTexRV );

			lError = glGetError();
			if
				( lError != GL_NO_ERROR )
			{
				throw exception(" Graphic Rendering Failure !!! ");
			}

			Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

			// Bind the vertex array object containing the geometry info.
			this->mOpenGL->glBindVertArray( this->mShapesVAO );

			this->mOpenGL->glDrawElementsBaseVertex( GL_TRIANGLES, this->mCylinderIndexCount, GL_UNSIGNED_INT, (VVOID*)(this->mCylinderIndexOffset * sizeof(UBIGINT)), this->mCylinderVertexOffset );

			lError = glGetError();
			if
				( lError != GL_NO_ERROR )
			{
				throw exception(" Graphic Rendering Failure !!! ");
			}
		}

		// Draw the spheres.
		for
			( BIGINT lCurrSphere = 0; lCurrSphere < 10; lCurrSphere++ )
		{
			Matrix4x4 lWorld = this->mSphereWorld[ lCurrSphere ];
			Matrix4x4 lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
			Matrix4x4 lWorldViewProj = lWorld * lViewProj;

			Effects::BasicFX()->SetWorld( lWorld );
			Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
			Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
			Effects::BasicFX()->SetTexTransform( Matrix4x4() );
			Effects::BasicFX()->SetMaterial( this->mSphereMat );

			this->mOpenGL->glActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
			glBindTexture( GL_TEXTURE_2D, this->mStoneTexRV );

			lError = glGetError();
			if
				( lError != GL_NO_ERROR )
			{
				throw exception(" Graphic Rendering Failure !!! ");
			}

			Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

			// Bind the vertex array object containing the geometry info.
			this->mOpenGL->glBindVertArray( this->mShapesVAO );

			this->mOpenGL->glDrawElementsBaseVertex( GL_TRIANGLES, this->mSphereIndexCount, GL_UNSIGNED_INT, (VVOID*)(this->mSphereIndexOffset * sizeof(UBIGINT)), this->mSphereVertexOffset );

			lError = glGetError();
			if
				( lError != GL_NO_ERROR )
			{
				throw exception(" Graphic Rendering Failure !!! ");
			}
		}

		// Make the Skull technique current
		Effects::BasicFX()->MakeCurrent( this->mActiveSkullTech );

		Effects::BasicFX()->SetEyePosW( lInfo.Eye() );
		Effects::BasicFX()->SetDirLights( this->mDirLights, this->mLightCount );

		// Draw the skull.
		lWorld = this->mSkullWorld;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetMaterial( this->mSkullMat );

		// Bind the vertex array object containing the geometry info.
		this->mOpenGL->glBindVertArray( this->mSkullVAO );

		glDrawElements( GL_TRIANGLES, this->mSkullIndexCount, GL_UNSIGNED_INT, NULL );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Graphic Rendering Failure !!! ");
		}
	}

	// Present the rendered scene to the screen.
	this->mOpenGL->StopRender();

	return true;
}

/*********************************************************************************************/
VVOID GraphicsClass::BuildShapeGeometryBuffers()
{
	MeshData lBox;
	MeshData lGrid;
	MeshData lSphere;
	MeshData lCylinder;

	GeometryHelper::CreateBox( 1.0f, 1.0f, 1.0f, lBox );
	GeometryHelper::CreateGrid( 20.0f, 30.0f, 60, 40, lGrid );
	GeometryHelper::CreateSphere( 0.5f, 20, 20, lSphere );
	GeometryHelper::CreateCylinder( 0.5f, 0.3f, 3.0f, 20, 20, lCylinder );

	// Cache the vertex offsets to each object in the concatenated vertex buffer.
	this->mBoxVertexOffset      = 0;
	this->mGridVertexOffset     = (BIGINT)lBox.Vertices.size();
	this->mSphereVertexOffset   = this->mGridVertexOffset + (BIGINT)lGrid.Vertices.size();
	this->mCylinderVertexOffset = this->mSphereVertexOffset + (BIGINT)lSphere.Vertices.size();

	// Cache the index count of each object.
	this->mBoxIndexCount      = (UBIGINT)lBox.Indices.size();
	this->mGridIndexCount     = (UBIGINT)lGrid.Indices.size();
	this->mSphereIndexCount   = (UBIGINT)lSphere.Indices.size();
	this->mCylinderIndexCount = (UBIGINT)lCylinder.Indices.size();

	// Cache the starting index for each object in the concatenated index buffer.
	this->mBoxIndexOffset      = 0;
	this->mGridIndexOffset     = this->mBoxIndexCount;
	this->mSphereIndexOffset   = this->mGridIndexOffset + this->mGridIndexCount;
	this->mCylinderIndexOffset = this->mSphereIndexOffset + this->mSphereIndexCount;
	
	UBIGINT lTotalVertexCount = (UBIGINT)lBox.Vertices.size() + (UBIGINT)lGrid.Vertices.size() + (UBIGINT)lSphere.Vertices.size() + (UBIGINT)lCylinder.Vertices.size();

	UBIGINT lTotalIndexCount = this->mBoxIndexCount + this->mGridIndexCount + this->mSphereIndexCount + this->mCylinderIndexCount;

	//
	// Extract the vertex elements we are interested in and pack the
	// vertices of all the meshes into one vertex buffer.
	//
	std::vector<SimpleVertex> lVertices( lTotalVertexCount );

	UBIGINT lCounter = 0;
	for( UBIGINT lCurrVertex = 0; lCurrVertex < (UBIGINT)lBox.Vertices.size(); lCurrVertex++, lCounter++ )
	{
		lVertices[lCounter].Position = lBox.Vertices[lCurrVertex].Position;
		lVertices[lCounter].Normal   = lBox.Vertices[lCurrVertex].Normal;
		lVertices[lCounter].TexCoord = lBox.Vertices[lCurrVertex].TexCoord;
	}

	for( UBIGINT lCurrVertex = 0; lCurrVertex < (UBIGINT)lGrid.Vertices.size(); lCurrVertex++, lCounter++ )
	{
		lVertices[lCounter].Position = lGrid.Vertices[lCurrVertex].Position;
		lVertices[lCounter].Normal   = lGrid.Vertices[lCurrVertex].Normal;
		lVertices[lCounter].TexCoord = lGrid.Vertices[lCurrVertex].TexCoord;
	}

	for( UBIGINT lCurrVertex = 0; lCurrVertex < (UBIGINT)lSphere.Vertices.size(); lCurrVertex++, lCounter++ )
	{
		lVertices[lCounter].Position = lSphere.Vertices[lCurrVertex].Position;
		lVertices[lCounter].Normal   = lSphere.Vertices[lCurrVertex].Normal;
		lVertices[lCounter].TexCoord = lSphere.Vertices[lCurrVertex].TexCoord;
	}

	for( UBIGINT lCurrVertex = 0; lCurrVertex < (UBIGINT)lCylinder.Vertices.size(); lCurrVertex++, lCounter++ )
	{
		lVertices[lCounter].Position = lCylinder.Vertices[lCurrVertex].Position;
		lVertices[lCounter].Normal   = lCylinder.Vertices[lCurrVertex].Normal;
		lVertices[lCounter].TexCoord = lCylinder.Vertices[lCurrVertex].TexCoord;
	}

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mShapesVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mShapesVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mShapesVB );

	// Bind the vertex buffer and load the vertex (position, texture, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mShapesVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, lTotalVertexCount * sizeof(SimpleVertex), &lVertices[0], GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::PosNormalTexLayout()->SetVertexLayout( this->mShapesVB );

	//
	// Pack the indices of all the meshes into one index buffer.
	//
	vector<UBIGINT> lIndices;
	lIndices.insert( lIndices.end(), lBox.Indices.begin(), lBox.Indices.end() );
	lIndices.insert( lIndices.end(), lGrid.Indices.begin(), lGrid.Indices.end() );
	lIndices.insert( lIndices.end(), lSphere.Indices.begin(), lSphere.Indices.end() );
	lIndices.insert( lIndices.end(), lCylinder.Indices.begin(), lCylinder.Indices.end() );

	// Generate a index buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mShapesIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mShapesIB );
	this->mOpenGL->glBufferData( GL_ELEMENT_ARRAY_BUFFER, lTotalIndexCount * sizeof(UBIGINT), &lIndices[0], GL_STATIC_DRAW );

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Shapes geometries Failed !!! ");
	}

	this->mOpenGL->glBindVertArray( 0 );
}

/*********************************************************************************************/
VVOID GraphicsClass::BuildSkullGeometryBuffers()
{
	ifstream lSkullFile("Models/skull.txt");
	if
		( lSkullFile.is_open() == false )
	{
		MessageBox( 0, L"Models/skull.txt not found.", 0, 0 );
		return;
	}

	UBIGINT lVCount = 0;
	UBIGINT lTCount = 0;
	string lIgnore;

	lSkullFile >> lIgnore >> lVCount;
	lSkullFile >> lIgnore >> lTCount;
	lSkullFile >> lIgnore >> lIgnore >> lIgnore >> lIgnore;
	
	vector<SimpleVertex> lVertices( lVCount );
	for
		( UBIGINT lCurrVertex = 0; lCurrVertex < lVCount; lCurrVertex++ )
	{
		FFLOAT lX, lY, lZ;
		lSkullFile >> lX >> lY >> lZ;
		lVertices[lCurrVertex].Position = Vector3( lX, lY, lZ );
		lSkullFile >> lX >> lY >> lZ;
		lVertices[lCurrVertex].Normal   = Vector3( lX, lY, lZ );
	}

	lSkullFile >> lIgnore;
	lSkullFile >> lIgnore;
	lSkullFile >> lIgnore;

	this->mSkullIndexCount = 3 * lTCount;
	vector<UBIGINT> lIndices( this->mSkullIndexCount );
	for
		( UBIGINT lCurrIndex = 0; lCurrIndex < lTCount; lCurrIndex++ )
	{
		lSkullFile >> lIndices[lCurrIndex * 3 + 0] >> lIndices[lCurrIndex * 3 + 1] >> lIndices[lCurrIndex * 3 + 2];
	}

	lSkullFile.close();

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mSkullVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mSkullVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mSkullVB );

	// Bind the vertex buffer and load the vertex (position, texture, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mSkullVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, lVCount * sizeof(SimpleVertex), &lVertices[0], GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::PosNormalTexLayout()->SetVertexLayout( this->mSkullVB );

	// Generate a index buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mSkullIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mSkullIB );
	this->mOpenGL->glBufferData( GL_ELEMENT_ARRAY_BUFFER, this->mSkullIndexCount * sizeof(UBIGINT), &lIndices[0], GL_STATIC_DRAW );

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Skull geometry Failed !!! ");
	}

	this->mOpenGL->glBindVertArray( 0 );
}

/*********************************************************************************************/
IOcelotCameraController* GraphicsClass::Controller()
{
	return this->mController;
}

/*********************************************************************************************/
VVOID                    GraphicsClass::SetLightCount(UBIGINT pCount)
{
	this->mLightCount = pCount;
}
