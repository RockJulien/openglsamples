#ifndef DEF_OCELOTFLYERCONTROLLER_H
#define DEF_OCELOTFLYERCONTROLLER_H

#include "OcelotOrbiterController.h"

namespace Ocelot
{
	class OcelotFlyerController : public OcelotOrbiterController
	{
	protected:

			// Attributes
			FFLOAT m_fSpeed;     // fly or walk speed.
			FFLOAT m_fDeltaTime; // keep a track of the elapsed time between two frames.

	public:

			// Constructor & Destructor
			OcelotFlyerController();
			OcelotFlyerController(OcelotCamera* camera, FFLOAT speed = 20.0f);
	virtual ~OcelotFlyerController();

			// Methods
	virtual VVOID update(FFLOAT deltaTime);
	virtual VVOID move(Direction dir);
	virtual VVOID pitch(FFLOAT angle);
	virtual VVOID yaw(FFLOAT angle);
	virtual VVOID roll(FFLOAT angle);

			// Accessors
	inline  FFLOAT Speed() const { return m_fSpeed;}
	inline  VVOID  SetSpeed(FFLOAT speed) { m_fSpeed = speed;}
	};
}

#endif