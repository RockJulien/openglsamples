////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _GRAPHICSCLASS_H_
#define _GRAPHICSCLASS_H_

///////////////////////
// INCLUDES //
///////////////////////
#include "GDevice.h"
#include "Camera\OcelotCamera.h"
#include "Camera\OcelotCameraFactory.h"
#include "GeometryHelper.h"
#include "OcelotLight.h"
#include "Material.h"
#include "Effects\Effects.h"
#include "Effects\InputLayouts.h"
#include "TextureManager\TextureManager.h"

using namespace Ocelot;

/////////////
// GLOBALS //
/////////////
const BBOOL  FULL_SCREEN   = false;
const BBOOL  VSYNC_ENABLED = true;
const FFLOAT SCREEN_DEPTH  = 1.0f;
const FFLOAT SCREEN_NEAR   = 0.1f;

///////////////////////
// Rendering Options //
///////////////////////
enum RenderOptions
{
	eLighting = 0,
	eTextures = 1,
	eTexturesAndFog = 2
};

enum SkullMoves
{
	eUp    = 0,
	eDown  = 1,
	eLeft  = 2,
	eRight = 3
};

////////////////////////////////////////////////////////////////////////////////
// Class name: GraphicsClass
////////////////////////////////////////////////////////////////////////////////
class GraphicsClass
{
public:

	// Constructor & Destructor
	GraphicsClass();
	~GraphicsClass();

	// Methods
	BBOOL Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight);
	VVOID OnResize(BIGINT pClientWidth, BIGINT pClientHeight);
	VVOID Shutdown();
	BBOOL Update(FFLOAT pDeltaTime);
	BBOOL Render();

	// Accessors
	IOcelotCameraController* Controller();
	VVOID SetOption(RenderOptions pOption);
	VVOID MoveSkull(SkullMoves pMove);

private:

	// Forbidden
	GraphicsClass(const GraphicsClass&);

	// Private Methods
	VVOID BuildRoomGeometryBuffers();
	VVOID BuildSkullGeometryBuffers();

private:

	// Attributes
	// Graphic device
	OpenGLClass*             mOpenGL;

	// Camera
	OcelotCamera*            mCamera;
	IOcelotCameraController* mController;
	
	// Buffers
	UBIGINT					 mRoomVAO;
	UBIGINT					 mRoomVB;
	UBIGINT					 mSkullVAO;
	UBIGINT					 mSkullVB;
	UBIGINT					 mSkullIB;

	// Lights
	Ocelot::OcelotParallelLight mDirLights[3];
	Material				    mRoomMat;
	Material				    mSkullMat;
	Material					mMirrorMat;
	Material					mShadowMat;

	// Transforms
	// Define transformations from local spaces to world space.
	Matrix4x4		 mRoomWorld;
	Matrix4x4		 mSkullWorld;

	// Index and Offset Count
	UBIGINT			 mSkullIndexCount;
	Vector3			 mSkullTranslation;

	// Texture Manager
	TextureManager   mTexManager;
	UBIGINT			 mFloorMapRV;
	UBIGINT			 mWallMapRV;
	UBIGINT			 mMirrorMapRV;

	UBIGINT			 mDiffuseTexUnit;

	// Effects
	EffectTechnique* mLight3Tech;
	EffectTechnique* mLight3FogTech;
	EffectTechnique* mLight3TexTech;
	EffectTechnique* mLight3TexFogTech;

	EffectTechnique* mTech;
	EffectTechnique* mSkullTech;

	// Options
	RenderOptions    mRenderOptions;

	FFLOAT			 mDeltaTime;
};

#endif