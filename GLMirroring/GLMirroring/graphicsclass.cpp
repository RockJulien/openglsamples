////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "graphicsclass.h"
#include "TGALoader.h"
#include <fstream>
#include "MathUtilities.h"
#include <time.h>
#include "systemclass.h"
#include "MeshData.h"

using namespace std;
using namespace Ocelot;

/*********************************************************************************************/
GraphicsClass::GraphicsClass() :
mOpenGL(NULL), mCamera(NULL), mController(NULL), 
mRoomVAO(0), mRoomVB(0), mSkullVAO(0),
mSkullVB(0), mSkullIB(0), mLight3Tech(NULL), mLight3TexTech(NULL),
mLight3TexFogTech(NULL), mTech(NULL), mSkullTech(NULL), 
mLight3FogTech(NULL), mRenderOptions(RenderOptions::eTexturesAndFog),
mFloorMapRV(0), mWallMapRV(0), mMirrorMapRV(0), mDeltaTime(0.0f),
mSkullTranslation( 0.0f, 1.0f, -5.0f )
{
	srand((UBIGINT)time(NULL));

	// Initialize the camera.
	OcelotCameraInfo lCamInfo( Vector3( 0.0f, 3.0f, -15.0f ), 
							   Vector3( 0.0f, 0.0f, 1.0f ), 
							   (FFLOAT)(800 / 600), 
							   1000.0f );
	lCamInfo.SetNear(1.0f);
	this->mCamera = new OcelotCamera( lCamInfo );

	// And create the controller
	this->mController = ControlFact->createController( this->mCamera, 
													   CAMERA_CONTROLLER_FLYER );

	// Initialize the light object(s).
	this->mDirLights[0].SetAmbient( Color( 0.2f, 0.2f, 0.2f, 1.0f ) );
	this->mDirLights[0].SetDiffuse( Color( 0.5f, 0.5f, 0.5f, 1.0f ) );
	this->mDirLights[0].SetSpecular( Color( 0.5f, 0.5f, 0.5f, 1.0f ) );
	this->mDirLights[0].SetDirection( Vector3( 0.57735f, -0.57735f, 0.57735f ) );

	this->mDirLights[1].SetAmbient( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[1].SetDiffuse( Color( 0.20f, 0.20f, 0.20f, 1.0f ) );
	this->mDirLights[1].SetSpecular( Color( 0.25f, 0.25f, 0.25f, 1.0f ) );
	this->mDirLights[1].SetDirection( Vector3( -0.57735f, -0.57735f, 0.57735f ) );

	this->mDirLights[2].SetAmbient( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[2].SetDiffuse( Color( 0.2f, 0.2f, 0.2f, 1.0f ) );
	this->mDirLights[2].SetSpecular( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[2].SetDirection( Vector3( 0.0f, -0.707f, -0.707f ) );

	// Initialize materials
	this->mRoomMat.Ambient    = Color( 0.5f, 0.5f, 0.5f, 1.0f );
	this->mRoomMat.Diffuse    = Color( 1.0f, 1.0f, 1.0f, 1.0f );
	this->mRoomMat.Specular   = Color( 0.4f, 0.4f, 0.4f, 16.0f );

	this->mSkullMat.Ambient   = Color( 0.5f, 0.5f, 0.5f, 1.0f );
	this->mSkullMat.Diffuse   = Color( 1.0f, 1.0f, 1.0f, 1.0f );
	this->mSkullMat.Specular  = Color( 0.4f, 0.4f, 0.4f, 16.0f );

	// Reflected material is transparent so it blends into mirror.
	this->mMirrorMat.Ambient  = Color( 0.5f, 0.5f, 0.5f, 1.0f );
	this->mMirrorMat.Diffuse  = Color( 1.0f, 1.0f, 1.0f, 0.5f );
	this->mMirrorMat.Specular = Color( 0.4f, 0.4f, 0.4f, 16.0f );

	this->mShadowMat.Ambient  = Color( 0.0f, 0.0f, 0.0f, 1.0f );
	this->mShadowMat.Diffuse  = Color( 0.0f, 0.0f, 0.0f, 0.5f );
	this->mShadowMat.Specular = Color( 0.0f, 0.0f, 0.0f, 16.0f );

	this->mDiffuseTexUnit = 1;
}

/*********************************************************************************************/
GraphicsClass::~GraphicsClass()
{
	
}

/*********************************************************************************************/
BBOOL GraphicsClass::Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight)
{
	BBOOL lResult = false;

	// Store a pointer to the OpenGL class object.
	this->mOpenGL = pGDevice;

	Effects::InitializeAll( this->mOpenGL );
	InputLayouts::InitializeAll( this->mOpenGL );

	// Initialize the texture manager
	this->mTexManager.Initialize( this->mOpenGL );

	// Init the diffuse map to apply to the mesh.
	ExtType lType = ExtType::eDDS;

	this->mFloorMapRV  = this->mTexManager.CreateTexture( "Textures/checkboard.dds", lType );
	this->mWallMapRV   = this->mTexManager.CreateTexture( "Textures/brick01.dds", lType );
	this->mMirrorMapRV = this->mTexManager.CreateTexture( "Textures/ice.dds", lType );

	// Initialize the technique(s).
	vector<string> lIncludes;
	lIncludes.push_back("Shaders/LightHelper.helper"); // Need light include
	StageDescription lDescription;
	lDescription.SetVertexStagePath("Shaders/Basic.vs");
	lDescription.SetPixelStagePath("Shaders/Basic.ps");
	this->mLight3Tech = Effects::BasicFX()->Light3Tech( lDescription, lIncludes );
	this->mLight3Tech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight3FogTech = Effects::BasicFX()->Light3FogTech( lDescription, lIncludes );
	this->mLight3FogTech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight3TexTech = Effects::BasicFX()->Light3TexTech( lDescription, lIncludes );
	this->mLight3TexTech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight3TexFogTech = Effects::BasicFX()->Light3TexFogTech( lDescription, lIncludes );
	this->mLight3TexFogTech->SetLayout( InputLayouts::PosNormalTexLayout() );

	this->BuildRoomGeometryBuffers();
	this->BuildSkullGeometryBuffers();

	return true;
}

/*********************************************************************************************/
VVOID GraphicsClass::OnResize(BIGINT pClientWidth, BIGINT pClientHeight)
{
	FFLOAT lAspect = (FFLOAT)pClientWidth / pClientHeight;

	// refresh the aspect of the camera if the
	// window is resized.
	OcelotCameraInfo lInfo = this->mCamera->Info();
	if( this->mCamera != NULL )
	{
		lInfo.SetAspect( lAspect );  // aspect ratio
		this->mCamera->SetInfo( lInfo );
	}

	glViewport( 0, 0, pClientWidth, pClientHeight );
}

/*********************************************************************************************/
VVOID GraphicsClass::Shutdown()
{
	DeletePointer( this->mCamera );
	DeletePointer( this->mController );
	DeletePointer( mLight3Tech );
	DeletePointer( mLight3FogTech );
	DeletePointer( mLight3TexTech );
	DeletePointer( mLight3TexFogTech );

	glDeleteTextures( 1, &this->mFloorMapRV );
	glDeleteTextures( 1, &this->mWallMapRV );
	glDeleteTextures( 1, &this->mMirrorMapRV );

	// Release VAOs, VBOs and IBOs
	// Release Room buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mRoomVB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mRoomVAO );

	// Release Skull buffers
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mSkullVB );
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	this->mOpenGL->glDeleteBuffers( 1, &this->mSkullIB );
	this->mOpenGL->glBindVertArray( 0 );
	this->mOpenGL->glDeleteVertArrays( 1, &this->mSkullVAO );

	Effects::DestroyAll();
	InputLayouts::DestroyAll();
}

/*********************************************************************************************/
BBOOL GraphicsClass::Update(FFLOAT pDeltaTime)
{
	// Update the camera.
	this->mController->update( pDeltaTime );

	this->mDeltaTime = pDeltaTime;

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Start Update Failure !!! ");
	}

	// Don't let user move below ground plane.
	this->mSkullTranslation.y( Higher( this->mSkullTranslation.getY(), 0.0f ) );

	// Update the new world matrix.
	Matrix4x4 lSkullTranslation;
	Matrix4x4 lSkullRotate;
	Matrix4x4 lSkullScale;
	lSkullTranslation.Matrix4x4Translation( lSkullTranslation, this->mSkullTranslation.getX(), this->mSkullTranslation.getY(), this->mSkullTranslation.getZ() );
	lSkullRotate.Matrix4x4RotationY( lSkullRotate, 0.5f * PI );
	lSkullScale.Matrix4x4Scaling( lSkullScale, 0.45f, 0.45f, 0.45f );
	this->mSkullWorld = lSkullRotate * lSkullScale * lSkullTranslation;

	return true;
}

/*********************************************************************************************/
BBOOL GraphicsClass::Render()
{
	// Clear the buffers to begin the scene.
	this->mOpenGL->StartRender( true, true );

	glClearDepth( 1.0f );
	glClearStencil( 0 );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

	FFLOAT lBlendFactor[] = { 0.0f, 0.0f, 0.0f, 0.0f };

	GLenum lError = glGetError();
	if( 
		lError != GL_NO_ERROR )
	{
		throw exception(" Before rendering FAILURE !!! ");
	}

	GLuint lStatus = this->mOpenGL->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	Matrix4x4 lView  = this->mCamera->View();
	Matrix4x4 lProj  = this->mCamera->Proj();
	Matrix4x4 lViewProj = lView * lProj;
	OcelotCameraInfo lInfo = this->mCamera->Info();

	if
		( Effects::BasicFX() != NULL )
	{
		switch 
			( this->mRenderOptions )
		{
		case eLighting:
			this->mTech = this->mLight3Tech;
			this->mSkullTech = this->mLight3Tech;
			break;
		case eTextures:
			this->mTech = this->mLight3TexTech;
			this->mSkullTech = this->mLight3Tech;
			break;
		case eTexturesAndFog:
			this->mTech = this->mLight3TexFogTech;
			this->mSkullTech = this->mLight3FogTech;
			break;
		}

		//
		// Draw the floor and walls to the back buffer as normal.
		//

		// Make the main technique current
		Effects::BasicFX()->MakeCurrent( this->mTech );

		Effects::BasicFX()->SetDirLights( this->mDirLights, 3 );
		Effects::BasicFX()->SetEyePosW( lInfo.Eye() );
		Effects::BasicFX()->SetFogColor( Color( 0.f, 0.f, 0.f, 1.0f ) );
		Effects::BasicFX()->SetFogStart( 2.0f );
		Effects::BasicFX()->SetFogRange( 40.0f );

		Matrix4x4 lWorld = this->mRoomWorld;
		Matrix4x4 lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		Matrix4x4 lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetTexTransform( Matrix4x4() );
		Effects::BasicFX()->SetMaterial( this->mRoomMat );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw box Failure !!! ");
		}

		//
		// Draw the floor
		// 

		this->mOpenGL->glBindVertArray( this->mRoomVAO );

		// Active the first texture unit
		this->mOpenGL->glActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mFloorMapRV );

		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		glDrawArrays( GL_TRIANGLES, 0, 6 );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Room Failure !!! ");
		}

		//
		// Draw the wall
		// 

		// Active the first texture unit
		this->mOpenGL->glActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mWallMapRV );

		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		glDrawArrays( GL_TRIANGLES, 6, 18 );

		// Unbind Room VAO
		this->mOpenGL->glBindVertArray( 0 );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Room Failure !!! ");
		}

		//
		// Draw the skull to the back buffer as normal.
		//

		// Active Skull technique
		Effects::BasicFX()->MakeCurrent( this->mSkullTech );

		Effects::BasicFX()->SetDirLights( this->mDirLights, 3 );
		Effects::BasicFX()->SetEyePosW( lInfo.Eye() );
		Effects::BasicFX()->SetFogColor( Color( 0.f, 0.f, 0.f, 1.0f ) );
		Effects::BasicFX()->SetFogStart( 2.0f );
		Effects::BasicFX()->SetFogRange( 40.0f );

		lWorld = this->mSkullWorld;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetMaterial( this->mSkullMat );

		this->mOpenGL->glBindVertArray( this->mSkullVAO );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Skull Failure !!! ");
		}

		glDrawElements( GL_TRIANGLES, this->mSkullIndexCount, GL_UNSIGNED_INT, NULL );
		
		// Unbind Skull VAO
		this->mOpenGL->glBindVertArray( 0 );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Skull Failure !!! ");
		}

		//
		// Draw the mirror to stencil buffer only.
		//

		// Active the main technique back.
		Effects::BasicFX()->MakeCurrent( this->mTech );

		Effects::BasicFX()->SetDirLights( this->mDirLights, 3 );
		Effects::BasicFX()->SetEyePosW( lInfo.Eye() );
		Effects::BasicFX()->SetFogColor( Color( 0.f, 0.f, 0.f, 1.0f ) );
		Effects::BasicFX()->SetFogStart( 2.0f );
		Effects::BasicFX()->SetFogRange( 40.0f );

		lWorld = this->mRoomWorld;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetTexTransform( Matrix4x4() );

		this->mOpenGL->glBindVertArray( this->mRoomVAO );

		// Disable blending
		glDisable( GL_BLEND );
		this->mOpenGL->glBlendColor( lBlendFactor[0], lBlendFactor[1], lBlendFactor[2], lBlendFactor[3] );
		this->mOpenGL->glBlendEquationSeparate( GL_FUNC_ADD, GL_FUNC_ADD );
		this->mOpenGL->glBlendFuncSeparate( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO );
		
		// MarkMirrorDSS
		//glDepthFunc( GL_LESS );
		glColorMask( GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE );
		glDepthMask( GL_FALSE );
		glEnable( GL_STENCIL_TEST );
		glStencilOp( GL_REPLACE, GL_KEEP, GL_KEEP );
		glStencilFunc( GL_ALWAYS, 1, 0xFF );
		glStencilMask( 0xFF );
		glClear( GL_STENCIL_BUFFER_BIT ); 

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Mirror Failure !!! ");
		}

		glDrawArrays( GL_TRIANGLES, 24, 6 );

		//glDepthFunc( GL_LEQUAL );
		glColorMask( GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE );
		glDepthMask( GL_TRUE );
		glDisable( GL_STENCIL_TEST );

		// Unbind Room VAO
		this->mOpenGL->glBindVertArray( 0 );
		
		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Waves Failure !!! ");
		}

		//
		// Draw the skull reflection.
		// 

		// Active Skull technique
		Effects::BasicFX()->MakeCurrent( this->mSkullTech );

		Effects::BasicFX()->SetEyePosW( lInfo.Eye() );
		Effects::BasicFX()->SetFogColor( Color( 0.f, 0.f, 0.f, 1.0f ) );
		Effects::BasicFX()->SetFogStart( 2.0f );
		Effects::BasicFX()->SetFogRange( 40.0f );

		Plane lMirrorPlane( 0.0f, 0.0f, 1.0f, 0.0f ); // xy plane
		Matrix4x4 lReflect;
		Matrix4x4::Matrix4x4Reflect( lReflect, lMirrorPlane );
		lWorld = this->mSkullWorld * lReflect;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetMaterial( this->mSkullMat );

		// Cache the old light directions, and reflect the light directions.
		Vector3 lOldLightDirections[3];
		for( BIGINT lCurrLight = 0; lCurrLight < 3; lCurrLight++ )
		{
			lOldLightDirections[ lCurrLight ] = this->mDirLights[ lCurrLight ].Direction();

			Vector3 lLightDir3 = this->mDirLights[ lCurrLight ].Direction();
			Vector4 lReflectedLightDir = lLightDir3.Vec3VecMatrixTransformNormal( lReflect );
			this->mDirLights[ lCurrLight ].SetDirection( Vector3( lReflectedLightDir.getX(), lReflectedLightDir.getY(), lReflectedLightDir.getZ() ) );
		}

		Effects::BasicFX()->SetDirLights( this->mDirLights, 3 );

		this->mOpenGL->glBindVertArray( this->mSkullVAO );

		// Cull clockwise triangles for reflection by setting front faces counter clockwise.
		glFrontFace( GL_CCW );
		glEnable( GL_STENCIL_TEST );

		// Only draw reflection into visible mirror pixels as marked by the stencil buffer. 

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Skull Failure !!! ");
		}

		glDrawElements( GL_TRIANGLES, this->mSkullIndexCount, GL_UNSIGNED_INT, NULL );
		
		// Restore default states.
		glFrontFace( GL_CW ); // Back to clockwise
		glDisable( GL_STENCIL_TEST );

		// Unbind Skull VAO
		this->mOpenGL->glBindVertArray( 0 );

		// Restore light directions.
		for( BIGINT lCurrLight = 0; lCurrLight < 3; lCurrLight++ )
		{
			this->mDirLights[ lCurrLight ].SetDirection( lOldLightDirections[ lCurrLight ] );
		}

		Effects::BasicFX()->SetDirLights( this->mDirLights, 3 );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Skull Failure !!! ");
		}

		//
		// Draw the mirror to the back buffer as usual but with transparency
		// blending so the reflection shows through.
		// 

		// Active the main technique back.
		Effects::BasicFX()->MakeCurrent( this->mTech );

		Effects::BasicFX()->SetDirLights( this->mDirLights, 3 );
		Effects::BasicFX()->SetEyePosW( lInfo.Eye() );
		Effects::BasicFX()->SetFogColor( Color( 0.f, 0.f, 0.f, 1.0f ) );
		Effects::BasicFX()->SetFogStart( 2.0f );
		Effects::BasicFX()->SetFogRange( 40.0f );

		lWorld = this->mRoomWorld;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetTexTransform( Matrix4x4() );
		Effects::BasicFX()->SetMaterial( this->mMirrorMat );

		// Active the first texture unit
		this->mOpenGL->glActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mMirrorMapRV );

		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Mirror Failure !!! ");
		}

		this->mOpenGL->glBindVertArray( this->mRoomVAO );

		glEnable( GL_BLEND );

		glDrawArrays( GL_TRIANGLES, 24, 6 );

		glDisable( GL_BLEND );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Mirror Failure !!! ");
		}

		//
		// Draw the skull shadow.
		// 

		// Active Skull technique
		Effects::BasicFX()->MakeCurrent( this->mSkullTech );

		Effects::BasicFX()->SetDirLights( this->mDirLights, 3 );
		Effects::BasicFX()->SetEyePosW( lInfo.Eye() );
		Effects::BasicFX()->SetFogColor( Color( 0.f, 0.f, 0.f, 1.0f ) );
		Effects::BasicFX()->SetFogStart( 2.0f );
		Effects::BasicFX()->SetFogRange( 40.0f );

		Plane lShadowPlane( 0.0f, 1.0f, 0.0f, 0.0f ); // xz plane
		Vector3 lToMainLight = -this->mDirLights[0].Direction();
		Matrix4x4 lShadow;
		Matrix4x4::Matrix4x4Shadow( lShadow, lShadowPlane, Vector4( lToMainLight.getX(), lToMainLight.getY(), lToMainLight.getZ(), 0.0f ) );
		Matrix4x4 lShadowOffsetY;
		lShadowOffsetY.Matrix4x4Translation( lShadowOffsetY, 0.0f, 0.001f, 0.0f );

		lWorld = this->mSkullWorld * lShadow * lShadowOffsetY;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * lViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetMaterial( this->mShadowMat );

		glEnable( GL_BLEND ); // Blending needed for transparency of the shadow.
		glEnable( GL_STENCIL_TEST );
		glStencilFunc( GL_EQUAL, 0, 0xff );
		this->mOpenGL->glStencilOpSeparate( GL_BACK, GL_KEEP, GL_KEEP, GL_INCR );
		this->mOpenGL->glStencilOpSeparate( GL_FRONT, GL_KEEP, GL_KEEP, GL_INCR );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Shadow Failure !!! ");
		}

		this->mOpenGL->glBindVertArray( this->mSkullVAO );

		glDrawElements( GL_TRIANGLES, this->mSkullIndexCount, GL_UNSIGNED_INT, NULL );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Shadow Failure !!! ");
		}

		this->mOpenGL->glBindVertArray( 0 );

		glDisable( GL_STENCIL_TEST );
		glDisable( GL_BLEND );
	}

	lStatus = this->mOpenGL->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	// Present the rendered scene to the screen.
	this->mOpenGL->StopRender();

	return true;
}

/*********************************************************************************************/
VVOID GraphicsClass::BuildRoomGeometryBuffers()
{
	// Create and specify geometry.  For this sample we draw a floor
	// and a wall with a mirror on it.  We put the floor, wall, and
	// mirror geometry in one vertex buffer.
	//
	//   |--------------|
	//   |              |
    //   |----|----|----|
    //   |Wall|Mirr|Wall|
	//   |    | or |    |
    //   /--------------/
    //  /   Floor      /
	// /--------------/

	SimpleVertex lVertices[30];

	// Floor: Observe we tile texture coordinates.
	lVertices[0] = SimpleVertex(-3.5f, 0.0f, -10.0f, 0.0f, 1.0f, 0.0f, 0.0f, 4.0f);
	lVertices[1] = SimpleVertex(-3.5f, 0.0f,   0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f);
	lVertices[2] = SimpleVertex( 7.5f, 0.0f,   0.0f, 0.0f, 1.0f, 0.0f, 4.0f, 0.0f);
	
	lVertices[3] = SimpleVertex(-3.5f, 0.0f, -10.0f, 0.0f, 1.0f, 0.0f, 0.0f, 4.0f);
	lVertices[4] = SimpleVertex( 7.5f, 0.0f,   0.0f, 0.0f, 1.0f, 0.0f, 4.0f, 0.0f);
	lVertices[5] = SimpleVertex( 7.5f, 0.0f, -10.0f, 0.0f, 1.0f, 0.0f, 4.0f, 4.0f);

	// Wall: Observe we tile texture coordinates, and that we
	// leave a gap in the middle for the mirror.
	lVertices[6]  = SimpleVertex(-3.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 2.0f);
	lVertices[7]  = SimpleVertex(-3.5f, 4.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
	lVertices[8]  = SimpleVertex(-2.5f, 4.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.5f, 0.0f);
	
	lVertices[9]  = SimpleVertex(-3.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 2.0f);
	lVertices[10] = SimpleVertex(-2.5f, 4.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.5f, 0.0f);
	lVertices[11] = SimpleVertex(-2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.5f, 2.0f);

	lVertices[12] = SimpleVertex(2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 2.0f);
	lVertices[13] = SimpleVertex(2.5f, 4.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
	lVertices[14] = SimpleVertex(7.5f, 4.0f, 0.0f, 0.0f, 0.0f, -1.0f, 2.0f, 0.0f);
	
	lVertices[15] = SimpleVertex(2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 2.0f);
	lVertices[16] = SimpleVertex(7.5f, 4.0f, 0.0f, 0.0f, 0.0f, -1.0f, 2.0f, 0.0f);
	lVertices[17] = SimpleVertex(7.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 2.0f, 2.0f);

	lVertices[18] = SimpleVertex(-3.5f, 4.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
	lVertices[19] = SimpleVertex(-3.5f, 6.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
	lVertices[20] = SimpleVertex( 7.5f, 6.0f, 0.0f, 0.0f, 0.0f, -1.0f, 6.0f, 0.0f);
	
	lVertices[21] = SimpleVertex(-3.5f, 4.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
	lVertices[22] = SimpleVertex( 7.5f, 6.0f, 0.0f, 0.0f, 0.0f, -1.0f, 6.0f, 0.0f);
	lVertices[23] = SimpleVertex( 7.5f, 4.0f, 0.0f, 0.0f, 0.0f, -1.0f, 6.0f, 1.0f);

	// Mirror
	lVertices[24] = SimpleVertex(-2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
	lVertices[25] = SimpleVertex(-2.5f, 4.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f);
	lVertices[26] = SimpleVertex( 2.5f, 4.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);
	
	lVertices[27] = SimpleVertex(-2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f);
	lVertices[28] = SimpleVertex( 2.5f, 4.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f);
	lVertices[29] = SimpleVertex( 2.5f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f);

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Room Geometry Failure !!! ");
	}

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mRoomVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mRoomVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mRoomVB );

	// Bind the vertex buffer and load the vertex (position, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mRoomVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, 30 * sizeof(SimpleVertex), lVertices, GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::PosNormalTexLayout()->SetVertexLayout( this->mRoomVB );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Room Geometry Failure !!! ");
	}

	this->mOpenGL->glBindVertArray( 0 );
}

/*********************************************************************************************/
VVOID GraphicsClass::BuildSkullGeometryBuffers()
{
	ifstream lSkullFile("Models/skull.txt");
	if
		( lSkullFile.is_open() == false )
	{
		MessageBox( 0, L"Models/skull.txt not found.", 0, 0 );
		return;
	}

	UBIGINT lVCount = 0;
	UBIGINT lTCount = 0;
	string lIgnore;

	lSkullFile >> lIgnore >> lVCount;
	lSkullFile >> lIgnore >> lTCount;
	lSkullFile >> lIgnore >> lIgnore >> lIgnore >> lIgnore;
	
	vector<SimpleVertex> lVertices( lVCount );
	for( UBIGINT lCurrVertex = 0; lCurrVertex < lVCount; lCurrVertex++ )
	{
		FFLOAT lX, lY, lZ;
		lSkullFile >> lX >> lY >> lZ;
		lVertices[lCurrVertex].Position = Vector3( lX, lY, lZ );
		lSkullFile >> lX >> lY >> lZ;
		lVertices[lCurrVertex].Normal   = Vector3( lX, lY, lZ );
	}

	lSkullFile >> lIgnore;
	lSkullFile >> lIgnore;
	lSkullFile >> lIgnore;

	this->mSkullIndexCount = 3 * lTCount;
	vector<UBIGINT> lIndices( this->mSkullIndexCount );
	for( UBIGINT lCurrIndex = 0; lCurrIndex < lTCount; lCurrIndex++ )
	{
		lSkullFile >> lIndices[lCurrIndex * 3 + 0] >> lIndices[lCurrIndex * 3 + 1] >> lIndices[lCurrIndex * 3 + 2];
	}

	lSkullFile.close();

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->glGenVertArrays( 1, &this->mSkullVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->glBindVertArray( this->mSkullVAO );

	// Generate a vertex buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mSkullVB );

	// Bind the vertex buffer and load the vertex (position, texture, and normal) data into the vertex buffer.
	this->mOpenGL->glBindBuffer( GL_ARRAY_BUFFER, this->mSkullVB );
	this->mOpenGL->glBufferData( GL_ARRAY_BUFFER, lVCount * sizeof(SimpleVertex), &lVertices[0], GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::PosNormalTexLayout()->SetVertexLayout( this->mSkullVB );

	// Generate a index buffer object
	this->mOpenGL->glGenBuffers( 1, &this->mSkullIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mSkullIB );
	this->mOpenGL->glBufferData( GL_ELEMENT_ARRAY_BUFFER, this->mSkullIndexCount * sizeof(UBIGINT), &lIndices[0], GL_STATIC_DRAW );

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Skull geometry Failed !!! ");
	}

	this->mOpenGL->glBindVertArray( 0 );
}

/*********************************************************************************************/
IOcelotCameraController* GraphicsClass::Controller()
{
	return this->mController;
}

/*********************************************************************************************/
VVOID GraphicsClass::SetOption(RenderOptions pOption)
{
	this->mRenderOptions = pOption;
}

/*********************************************************************************************/
VVOID GraphicsClass::MoveSkull(SkullMoves pMove)
{
	if
		( pMove == SkullMoves::eRight )
	{
		this->mSkullTranslation.x( this->mSkullTranslation.getX() - 1.0f * this->mDeltaTime );
	}

	if
		( pMove == SkullMoves::eLeft )
	{
		this->mSkullTranslation.x( this->mSkullTranslation.getX() + 1.0f * this->mDeltaTime );
	}

	if
		( pMove == SkullMoves::eUp )
	{
		this->mSkullTranslation.y( this->mSkullTranslation.getY() + 1.0f * this->mDeltaTime );
	}

	if
		( pMove == SkullMoves::eDown )
	{
		this->mSkullTranslation.y( this->mSkullTranslation.getY() - 1.0f * this->mDeltaTime );
	}
}
