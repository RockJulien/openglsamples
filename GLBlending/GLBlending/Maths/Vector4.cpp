#include "Vector4.h"

Vector4::Vector4()
{
	m_fX = 0.0f;
	m_fY = 0.0f;
	m_fZ = 0.0f;
	m_fW = 0.0f;  // normally either 1 or 0 for a point or a vector direction for allowing or not (only allows in case of a point) a translation.
}

Vector4::Vector4(const Vector4& vec)
{
	m_fX = vec.getX();
	m_fY = vec.getY();
	m_fZ = vec.getZ();
	m_fW = vec.getW();
}

Vector4& Vector4::operator = (const Vector4& vec)
{
	m_fX = vec.getX();
	m_fY = vec.getY();
	m_fZ = vec.getZ();
	m_fW = vec.getW();

	return *this;
}

Vector4::Vector4(float xyz, float w) :
m_fX(xyz), m_fY(xyz), m_fZ(xyz), m_fW(w)
{

}

Vector4::Vector4(double xyz, double w) :
m_fX((float)xyz), m_fY((float)xyz), m_fZ((float)xyz), m_fW((float)w)
{

}

Vector4::Vector4(float  x, float  y, float  z, float  w) :
m_fX(x), m_fY(y), m_fZ(z), m_fW(w)
{

}

Vector4::Vector4(double x, double y, double z, double w) :
m_fX((float)x), m_fY((float)y), m_fZ((float)z), m_fW((float)w)
{

}

Vector4::~Vector4()
{

}

float   Vector4::Vec4Length()
{
	return sqrt((SQUARE(m_fX)) + (SQUARE(m_fY)) + (SQUARE(m_fZ))/* + (SQUARE(m_fW))*/);
}

float   Vector4::Vec4LengthSqrt()
{
	return (SQUARE(m_fX)) + (SQUARE(m_fY)) + (SQUARE(m_fZ)) /*+ (SQUARE(m_fW))*/;
}

float   Vector4::Vec4DistanceBetween(const Vector4& target)
{
	float newX = target.getX() - m_fX;
	float newY = target.getY() - m_fY;
	float newZ = target.getZ() - m_fZ;
	//float newW = target.getW() - m_fW;

	return sqrt((SQUARE(newX)) + (SQUARE(newY)) + (SQUARE(newZ))/* + (SQUARE(newW))*/);
}

Vector4 Vector4::Vec4Normalise()
{
	float magnitude = Vec4Length();

	if(fabsf(magnitude) > FLT_EPSILON) // prevent from dividing by 0
	{
		this->m_fX /= magnitude;
		this->m_fY /= magnitude;
		this->m_fZ /= magnitude;
		//this->m_fW /= magnitude;
	}

	return Vector4(m_fX, m_fY, m_fZ, m_fW);
}

float   Vector4::Vec4DotProduct(const Vector4& vec)
{
	return ((m_fX * vec.getX()) + (m_fY * vec.getY()) + (m_fZ * vec.getZ()) + (m_fW * vec.getW()));
}

Vector4 Vector4::Vec4CrossProduct(const Vector4& adjVec1, const Vector4& adjVec2)
{
	Vector4 rien(0.0f, 0.0f);
	// doesn't exist or became a simple 3D cross product by setting last vector for
	// homogeneous coordinates to w(0, 0, 0, 1);
	return rien;
}

Vector4 Vector4::Vec4Addition(const Vector4& vecToAdd)
{
	return Vector4(m_fX + vecToAdd.getX(), m_fY + vecToAdd.getY(), m_fZ + vecToAdd.getZ(), m_fW);
}

Vector4 Vector4::Vec4Subtraction(const Vector4& vecToSub)
{
	return Vector4(m_fX - vecToSub.getX(), m_fY - vecToSub.getY(), m_fZ - vecToSub.getZ(), m_fW);
}

Vector4 Vector4::Vec4LinearInterpolation(const Vector4& vecToJoin, float var)
{
	// just interpolate a straight line thanks to the origine point and its target.
	// if "var" equal 0, result is V1 and if "var" equal 1, result is V2.
	Vector4 origine(*this);
	Vector4 result = origine + (var * (vecToJoin - origine));
	
	return result;
}

Vector4 Vector4::Vec4HermiteSplineInterpolation(const Vector4& tangStartPoint, const Vector4& vecToJoin, const Vector4& tangVecToJoin, float var)
{
	// for finding the tangent to a vector, we have to differenciate(d�river) each component. If we want the unit tangent vecteur
	// we have just to divide each component of this new tangent vector by its length.
	// but doesn't matter, you have to pre compute tangents and put them as parameter, hahahahha.
	float coef0 = (2.0f * pow(var, 3)) - (3.0f * pow(var, 2)) + 1;
	float coef1 = (pow(var, 3)) - (2.0f * pow(var, 2)) + var;
	float coef2 = -(2.0f * pow(var, 3)) + (3.0f * pow(var, 2));
	float coef3 = pow(var, 3) - pow(var, 2);
	Vector4 origine(*this);

	Vector4 result = (coef0 * origine) + (coef1 * tangStartPoint) + (coef2 * vecToJoin) + (coef3 * tangVecToJoin);
	return  result;
}

Vector4 Vector4::Vec4CatmullSplineInterpolation(const Vector4& vecControl1, const Vector4& vecToJoin, const Vector4& vecControl2, float var)
{
	// spline interpolation between vecOrigine and vecToJoin thanks to "var". Two vecControls are usd as control 
	// points on each side of the two vectors for controlling the shape of the interpolation. 
	// If "var" equal 0, result is vecOrigine, and if "var" equal 1, result is vecToJoin.
	float coef0 = -(pow(var, 3)) + (2.0f * pow(var, 2)) - var;
	float coef1 = (3.0f * pow(var, 3)) - (5.0f * pow(var, 2)) + 2.0f;
	float coef2 = -(3.0f * pow(var, 3)) + (4.0f * pow(var, 2)) + var;
	float coef3 = pow(var, 3) - pow(var, 2);
	Vector4 origine(*this);

	Vector4 result = 0.5f * ((coef0 * vecControl1) + (coef1 * origine) + (coef2 * vecToJoin) + (coef3 * vecControl2));
	return  result;
}

Vector4 Vector4::Vec4BarycentricInterpolation(const Vector4& vecAtBar, const Vector4& vecToJoin, float baryVar1, float baryVar2)
{
	// this is an interpolation between three vectors by using two barycentric coordinates "baryVar1" and "baryVar2".
	// If "baryVar1" and "baryVar2" are both equal to 0, result is vecOrigine, if "baryVar1" equal 1 and "baryVar2" equal 0, result is vecAtBar,
	// and if opposite, result is vecToJoin.
	Vector4 origine(*this);

	Vector4 result = origine + (baryVar1 * (vecAtBar - origine)) + (baryVar2 * (vecToJoin - origine));

	return result;
}

Vector4 Vector4::Vec4VecMatrixProduct(const Matrix4x4& mat)
{
	Vector4 temp;

	temp.m_fX = (m_fX * mat.get_11()) + (m_fY * mat.get_21()) + (m_fZ * mat.get_31()) + (m_fW * mat.get_41());
	temp.m_fY = (m_fX * mat.get_12()) + (m_fY * mat.get_22()) + (m_fZ * mat.get_32()) + (m_fW * mat.get_42());
	temp.m_fZ = (m_fX * mat.get_13()) + (m_fY * mat.get_23()) + (m_fZ * mat.get_33()) + (m_fW * mat.get_43());
	temp.m_fW = m_fW;

	return temp;
}

Vector4::operator float* ()
{
	float* matVec4[4];

	matVec4[0] = &m_fX;
	matVec4[1] = &m_fY;
	matVec4[2] = &m_fZ;
	matVec4[3] = &m_fW;

	return matVec4[0];
}

Vector4::operator const float* () const
{
	const float* matVec4[4];

	matVec4[0] = &m_fX;
	matVec4[1] = &m_fY;
	matVec4[2] = &m_fZ;
	matVec4[3] = &m_fW;

	return matVec4[0];
}

Vector4  Vector4::operator + () const
{
	return Vector4(+this->getX(), +this->getY(), +this->getZ(), this->getW());
}

Vector4  Vector4::operator - () const
{
	return Vector4(-this->getX(), -this->getY(), -this->getZ(), this->getW());
}

Vector4& Vector4::operator += (const Vector4& vec)
{
	m_fX += vec.getX();
	m_fY += vec.getY();
	m_fZ += vec.getZ();
	//m_fW += vec.getW();

	return *this;
}

Vector4& Vector4::operator -= (const Vector4& vec)
{
	m_fX -= vec.getX();
	m_fY -= vec.getY();
	m_fZ -= vec.getZ();
	//m_fW -= vec.getW();

	return *this;
}

Vector4& Vector4::operator *= (const float& val)
{
	m_fX *= val;
	m_fY *= val;
	m_fZ *= val;
	//m_fW *= val;

	return *this;
}

Vector4& Vector4::operator /= (const float& val)
{
	m_fX /= val;
	m_fY /= val;
	m_fZ /= val;
	//m_fW  = val;

	return *this;
}

Vector4  Vector4::operator +  (const Vector4& vec) const
{
	Vector4 temp(*this);
	temp += vec;

	return temp;
}

Vector4  Vector4::operator -  (const Vector4& vec) const
{
	Vector4 temp(*this);
	temp -= vec;

	return temp;
}

Vector4  Vector4::operator *  (const float& val) const
{
	Vector4 temp(*this);
	temp *= val;

	return temp;
}

Vector4  Vector4::operator /  (const float& val) const
{
	Vector4 temp(*this);
	temp /= val;

	return temp;
}

bool     Vector4::operator == (const Vector4& vec) const
{
	return (Equal<float>(m_fX, vec.getX()) && Equal<float>(m_fY, vec.getY()) && Equal<float>(m_fZ, vec.getZ()) && Equal<float>(m_fW, vec.getW()));
}

bool     Vector4::operator != (const Vector4& vec) const
{
	return (Different<float>(m_fX, vec.getX()) || Different<float>(m_fY, vec.getY()) || Different<float>(m_fZ, vec.getZ()) || Different<float>(m_fW, vec.getW()));
}

Vector4  operator * (float val, const Vector4& vec)
{
	Vector4 temp(vec);
	temp *= val;

	return temp;
}
