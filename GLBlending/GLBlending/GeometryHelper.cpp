#include "GeometryHelper.h"

// Namespaces used
using namespace std;
using namespace Ocelot;

/*************************************************************************************************/
GeometryHelper::GeometryHelper()
{

}

/*************************************************************************************************/
VVOID GeometryHelper::Subdivide(MeshData& pMeshData)
{
	// Save a copy of the input geometry.
	MeshData lInputCopy = pMeshData;
	
	pMeshData.Vertices.resize(0);
	pMeshData.Indices.resize(0);

	//       v1
	//       *
	//      / \
	//     /   \
	//  m0*-----*m1
	//   / \   / \
	//  /   \ /   \
	// *-----*-----*
	// v0    m2     v2

	UBIGINT lTriangleCount = (UBIGINT)lInputCopy.Indices.size()/3;
	for(UBIGINT lCurrTriangle = 0; lCurrTriangle < lTriangleCount; lCurrTriangle++)
	{
		EnhancedVertex lVertex0 = lInputCopy.Vertices[ lInputCopy.Indices[ lCurrTriangle * 3 + 0 ] ];
		EnhancedVertex lVertex1 = lInputCopy.Vertices[ lInputCopy.Indices[ lCurrTriangle * 3 + 1 ] ];
		EnhancedVertex lVertex2 = lInputCopy.Vertices[ lInputCopy.Indices[ lCurrTriangle * 3 + 2 ] ];

		//
		// Generate the midpoints.
		//
		EnhancedVertex lMidpoint0, lMidpoint1, lMidpoint2;

		// For subdivision, we just care about the position component.  We derive the other
		// lCurrVertex components in CreateGeosphere.
		lMidpoint0.Position = Vector3( 0.5f * (lVertex0.Position.getX() + lVertex1.Position.getX()),
									   0.5f * (lVertex0.Position.getY() + lVertex1.Position.getY()),
									   0.5f * (lVertex0.Position.getZ() + lVertex1.Position.getZ()) );

		lMidpoint1.Position = Vector3( 0.5f * (lVertex1.Position.getX() + lVertex2.Position.getX()),
									   0.5f * (lVertex1.Position.getY() + lVertex2.Position.getY()),
									   0.5f * (lVertex1.Position.getZ() + lVertex2.Position.getZ()) );

		lMidpoint2.Position = Vector3( 0.5f * (lVertex0.Position.getX() + lVertex2.Position.getX()),
									   0.5f * (lVertex0.Position.getY() + lVertex2.Position.getY()),
									   0.5f * (lVertex0.Position.getZ() + lVertex2.Position.getZ()) );

		//
		// Add new geometry.
		//
		pMeshData.Vertices.push_back( lVertex0 );   // 0
		pMeshData.Vertices.push_back( lVertex1 );   // 1
		pMeshData.Vertices.push_back( lVertex2 );   // 2
		pMeshData.Vertices.push_back( lMidpoint0 ); // 3
		pMeshData.Vertices.push_back( lMidpoint1 ); // 4
		pMeshData.Vertices.push_back( lMidpoint2 ); // 5
 
		pMeshData.Indices.push_back( lCurrTriangle * 6 + 0 );
		pMeshData.Indices.push_back( lCurrTriangle * 6 + 3 );
		pMeshData.Indices.push_back( lCurrTriangle * 6 + 5 );

		pMeshData.Indices.push_back( lCurrTriangle * 6 + 3 );
		pMeshData.Indices.push_back( lCurrTriangle * 6 + 4 );
		pMeshData.Indices.push_back( lCurrTriangle * 6 + 5 );

		pMeshData.Indices.push_back( lCurrTriangle * 6 + 5 );
		pMeshData.Indices.push_back( lCurrTriangle * 6 + 4 );
		pMeshData.Indices.push_back( lCurrTriangle * 6 + 2 );

		pMeshData.Indices.push_back( lCurrTriangle * 6 + 3 );
		pMeshData.Indices.push_back( lCurrTriangle * 6 + 1 );
		pMeshData.Indices.push_back( lCurrTriangle * 6 + 4 );
	}
}

/*************************************************************************************************/
VVOID GeometryHelper::BuildCylinderTopCap(FFLOAT pBottomRadius, FFLOAT pTopRadius, FFLOAT pHeight, UBIGINT pSliceCount, UBIGINT pStackCount, MeshData& pMeshData)
{
	UBIGINT lBaseIndex = (UBIGINT)pMeshData.Vertices.size();

	FFLOAT lY = 0.5f * pHeight;
	FFLOAT lDTheta = 2.0f * PI / pSliceCount;

	// Duplicate cap ring vertices because the texture coordinates and normals differ.
	for(UBIGINT lCurrSlice = 0; lCurrSlice <= pSliceCount; lCurrSlice++)
	{
		FFLOAT lX = pTopRadius * cosf( lCurrSlice * lDTheta );
		FFLOAT lZ = pTopRadius * sinf( lCurrSlice * lDTheta );

		// Scale down by the pHeight to try and make top cap texture coord area
		// proportional to base.
		FFLOAT lU = lX / pHeight + 0.5f;
		FFLOAT lV = lZ / pHeight + 0.5f;

		pMeshData.Vertices.push_back( EnhancedVertex( lX, lY, lZ, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, lU, lV ) );
	}

	// Cap center lCurrVertex.
	pMeshData.Vertices.push_back( EnhancedVertex( 0.0f, lY, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.5f, 0.5f ) );

	// Index of center lCurrVertex.
	UBIGINT lCenterIndex = (UBIGINT)pMeshData.Vertices.size() - 1;
	for(UBIGINT lCurrSlice = 0; lCurrSlice < pSliceCount; lCurrSlice++)
	{
		pMeshData.Indices.push_back( lCenterIndex );
		pMeshData.Indices.push_back( lBaseIndex + lCurrSlice + 1 );
		pMeshData.Indices.push_back( lBaseIndex + lCurrSlice );
	}
}

/*************************************************************************************************/
VVOID GeometryHelper::BuildCylinderBottomCap(FFLOAT pBottomRadius, FFLOAT pTopRadius, FFLOAT pHeight, UBIGINT pSliceCount, UBIGINT pStackCount, MeshData& pMeshData)
{
	// 
	// Build bottom cap.
	//
	UBIGINT lBaseIndex = (UBIGINT)pMeshData.Vertices.size();
	FFLOAT lY = -0.5f * pHeight;

	// vertices of ring
	FFLOAT lDTheta = 2.0f * PI / pSliceCount;
	for(UBIGINT lCurrSlice = 0; lCurrSlice <= pSliceCount; lCurrSlice++)
	{
		FFLOAT lX = pBottomRadius * cosf( lCurrSlice * lDTheta );
		FFLOAT lZ = pBottomRadius * sinf( lCurrSlice * lDTheta );

		// Scale down by the Height to try and make top cap texture coord area
		// proportional to base.
		FFLOAT lU = lX / pHeight + 0.5f;
		FFLOAT lV = lZ / pHeight + 0.5f;

		pMeshData.Vertices.push_back( EnhancedVertex( lX, lY, lZ, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, lU, lV ) );
	}

	// Cap center lCurrVertex.
	pMeshData.Vertices.push_back( EnhancedVertex( 0.0f, lY, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.5f, 0.5f ) );

	// Cache the index of center lCurrVertex.
	UBIGINT lCenterIndex = (UBIGINT)pMeshData.Vertices.size() - 1;
	for(UBIGINT lCurrSlice = 0; lCurrSlice < pSliceCount; lCurrSlice++)
	{
		pMeshData.Indices.push_back( lCenterIndex );
		pMeshData.Indices.push_back( lBaseIndex + lCurrSlice );
		pMeshData.Indices.push_back( lBaseIndex + lCurrSlice + 1 );
	}
}

/*************************************************************************************************/
VVOID GeometryHelper::CreateBox(FFLOAT pWidth, FFLOAT pHeight, FFLOAT pDepth, MeshData& pMeshData)
{
	//
	// Create the vertices.
	//
	EnhancedVertex lVertices[24];

	FFLOAT lHalfWidth  = 0.5f * pWidth;
	FFLOAT lHalfHeight = 0.5f * pHeight;
	FFLOAT lHalfDepth  = 0.5f * pDepth;
    
	// Fill in the front face lCurrVertex data.
	lVertices[0] = EnhancedVertex( -lHalfWidth, -lHalfHeight, -lHalfDepth, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f );
	lVertices[1] = EnhancedVertex( -lHalfWidth, +lHalfHeight, -lHalfDepth, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f );
	lVertices[2] = EnhancedVertex( +lHalfWidth, +lHalfHeight, -lHalfDepth, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f );
	lVertices[3] = EnhancedVertex( +lHalfWidth, -lHalfHeight, -lHalfDepth, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f );

	// Fill in the back face lCurrVertex data.
	lVertices[4] = EnhancedVertex( -lHalfWidth, -lHalfHeight, +lHalfDepth, 0.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f );
	lVertices[5] = EnhancedVertex( +lHalfWidth, -lHalfHeight, +lHalfDepth, 0.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f );
	lVertices[6] = EnhancedVertex( +lHalfWidth, +lHalfHeight, +lHalfDepth, 0.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f );
	lVertices[7] = EnhancedVertex( -lHalfWidth, +lHalfHeight, +lHalfDepth, 0.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f );

	// Fill in the top face lCurrVertex data.
	lVertices[8]  = EnhancedVertex( -lHalfWidth, +lHalfHeight, -lHalfDepth, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f );
	lVertices[9]  = EnhancedVertex( -lHalfWidth, +lHalfHeight, +lHalfDepth, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f );
	lVertices[10] = EnhancedVertex( +lHalfWidth, +lHalfHeight, +lHalfDepth, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f );
	lVertices[11] = EnhancedVertex( +lHalfWidth, +lHalfHeight, -lHalfDepth, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f );

	// Fill in the bottom face lCurrVertex data.
	lVertices[12] = EnhancedVertex( -lHalfWidth, -lHalfHeight, -lHalfDepth, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f );
	lVertices[13] = EnhancedVertex( +lHalfWidth, -lHalfHeight, -lHalfDepth, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f );
	lVertices[14] = EnhancedVertex( +lHalfWidth, -lHalfHeight, +lHalfDepth, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f );
	lVertices[15] = EnhancedVertex( -lHalfWidth, -lHalfHeight, +lHalfDepth, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f );

	// Fill in the left face lCurrVertex data.
	lVertices[16] = EnhancedVertex( -lHalfWidth, -lHalfHeight, +lHalfDepth, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f );
	lVertices[17] = EnhancedVertex( -lHalfWidth, +lHalfHeight, +lHalfDepth, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f );
	lVertices[18] = EnhancedVertex( -lHalfWidth, +lHalfHeight, -lHalfDepth, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f );
	lVertices[19] = EnhancedVertex( -lHalfWidth, -lHalfHeight, -lHalfDepth, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f );

	// Fill in the right face lCurrVertex data.
	lVertices[20] = EnhancedVertex( +lHalfWidth, -lHalfHeight, -lHalfDepth, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f );
	lVertices[21] = EnhancedVertex( +lHalfWidth, +lHalfHeight, -lHalfDepth, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f );
	lVertices[22] = EnhancedVertex( +lHalfWidth, +lHalfHeight, +lHalfDepth, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f );
	lVertices[23] = EnhancedVertex( +lHalfWidth, -lHalfHeight, +lHalfDepth, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f );

	pMeshData.Vertices.assign( &lVertices[0], &lVertices[24] );
 
	//
	// Create the indices.
	//
	UBIGINT lIndices[36];

	// Fill in the front face index data
	lIndices[0] = 0; lIndices[1] = 1; lIndices[2] = 2;
	lIndices[3] = 0; lIndices[4] = 2; lIndices[5] = 3;

	// Fill in the back face index data
	lIndices[6] = 4; lIndices[7]  = 5; lIndices[8]  = 6;
	lIndices[9] = 4; lIndices[10] = 6; lIndices[11] = 7;

	// Fill in the top face index data
	lIndices[12] = 8; lIndices[13] =  9; lIndices[14] = 10;
	lIndices[15] = 8; lIndices[16] = 10; lIndices[17] = 11;

	// Fill in the bottom face index data
	lIndices[18] = 12; lIndices[19] = 13; lIndices[20] = 14;
	lIndices[21] = 12; lIndices[22] = 14; lIndices[23] = 15;

	// Fill in the left face index data
	lIndices[24] = 16; lIndices[25] = 17; lIndices[26] = 18;
	lIndices[27] = 16; lIndices[28] = 18; lIndices[29] = 19;

	// Fill in the right face index data
	lIndices[30] = 20; lIndices[31] = 21; lIndices[32] = 22;
	lIndices[33] = 20; lIndices[34] = 22; lIndices[35] = 23;

	pMeshData.Indices.assign( &lIndices[0], &lIndices[36] );
}

/*************************************************************************************************/
VVOID GeometryHelper::CreateSphere(FFLOAT pRadius, UBIGINT pSliceCount, UBIGINT pStackCount, MeshData& pMeshData)
{
	pMeshData.Vertices.clear();
	pMeshData.Indices.clear();

	//
	// Compute the vertices stating at the top pole and moving down the stacks.
	//

	// Poles: note that there will be texture coordinate distortion as there is
	// not a unique point on the texture map to assign to the pole when mapping
	// a rectangular texture onto a sphere.
	EnhancedVertex lTopVertex( 0.0f, +pRadius, 0.0f, 0.0f, +1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f );
	EnhancedVertex lBottomVertex( 0.0f, -pRadius, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f );

	pMeshData.Vertices.push_back( lTopVertex );

	FFLOAT lPhiStep   = PI / pStackCount;
	FFLOAT lThetaStep = 2.0f * PI / pSliceCount;

	// Compute vertices for each stack ring (do not count the poles as rings).
	for(UBIGINT lCurrStack = 1; lCurrStack <= pStackCount - 1; lCurrStack++)
	{
		FFLOAT lPhi = lCurrStack * lPhiStep;

		// Vertices of ring.
		for(UBIGINT lCurrSlice = 0; lCurrSlice <= pSliceCount; lCurrSlice++)
		{
			FFLOAT lTheta = lCurrSlice * lThetaStep;

			EnhancedVertex lCurrVertex;

			// spherical to cartesian
			lCurrVertex.Position.x( pRadius * sinf(lPhi) * cosf(lTheta) );
			lCurrVertex.Position.y( pRadius * cosf(lPhi) );
			lCurrVertex.Position.z( pRadius * sinf(lPhi) * sinf(lTheta) );

			// Partial derivative of P with respect to lTheta
			lCurrVertex.Tangent.x( -pRadius * sinf(lPhi) * sinf(lTheta) );
			lCurrVertex.Tangent.y( 0.0f );
			lCurrVertex.Tangent.z( +pRadius * sinf(lPhi) * cosf(lTheta) );

			Vector4 lTangent    = lCurrVertex.Tangent;
			lCurrVertex.Tangent = lTangent.Vec4Normalise();

			Vector3 lPosition  = lCurrVertex.Position;
			lCurrVertex.Normal = lPosition.Vec3Normalise();

			lCurrVertex.TexCoord.x( lTheta / PISQUARE );
			lCurrVertex.TexCoord.y( lPhi / PI );

			pMeshData.Vertices.push_back( lCurrVertex );
		}
	}

	pMeshData.Vertices.push_back( lBottomVertex );

	//
	// Compute indices for top stack.  The top stack was written first to the lCurrVertex buffer
	// and connects the top pole to the first ring.
	//
	for(UBIGINT lCurrStack = 1; lCurrStack <= pSliceCount; lCurrStack++)
	{
		pMeshData.Indices.push_back( 0 );
		pMeshData.Indices.push_back( lCurrStack + 1 );
		pMeshData.Indices.push_back( lCurrStack );
	}
	
	//
	// Compute indices for inner stacks (not connected to poles).
	//

	// Offset the indices to the index of the first lCurrVertex in the first ring.
	// This is just skipping the top pole lCurrVertex.
	UBIGINT lBaseIndex = 1;
	UBIGINT lRingVertexCount = pSliceCount + 1;
	for(UBIGINT lCurrStack = 0; lCurrStack < pStackCount - 2; lCurrStack++)
	{
		for(UBIGINT lCurrSlice = 0; lCurrSlice < pSliceCount; lCurrSlice++)
		{
			pMeshData.Indices.push_back( lBaseIndex + lCurrStack * lRingVertexCount + lCurrSlice );
			pMeshData.Indices.push_back( lBaseIndex + lCurrStack * lRingVertexCount + lCurrSlice + 1 );
			pMeshData.Indices.push_back( lBaseIndex + (lCurrStack + 1) * lRingVertexCount + lCurrSlice );

			pMeshData.Indices.push_back( lBaseIndex + (lCurrStack + 1) * lRingVertexCount + lCurrSlice );
			pMeshData.Indices.push_back( lBaseIndex + lCurrStack * lRingVertexCount + lCurrSlice + 1 );
			pMeshData.Indices.push_back( lBaseIndex + (lCurrStack + 1) * lRingVertexCount + lCurrSlice + 1 );
		}
	}

	//
	// Compute indices for bottom stack.  The bottom stack was written last to the lCurrVertex buffer
	// and connects the bottom pole to the bottom ring.
	//

	// South pole lCurrVertex was added last.
	UBIGINT lSouthPoleIndex = (UBIGINT)pMeshData.Vertices.size() - 1;

	// Offset the indices to the index of the first lCurrVertex in the last ring.
	lBaseIndex = lSouthPoleIndex - lRingVertexCount;
	for(UBIGINT lCurrStack = 0; lCurrStack < pSliceCount; lCurrStack++)
	{
		pMeshData.Indices.push_back( lSouthPoleIndex );
		pMeshData.Indices.push_back( lBaseIndex + lCurrStack );
		pMeshData.Indices.push_back( lBaseIndex + lCurrStack + 1 );
	}
}

/*************************************************************************************************/
VVOID GeometryHelper::CreateGeosphere(FFLOAT pRadius, UBIGINT pNumSubdivisions, MeshData& pMeshData)
{
	// Put a cap on the number of subdivisions.
	pNumSubdivisions = Smaller( pNumSubdivisions, 5u );

	// Approximate a sphere by tessellating an icosahedron.
	const FFLOAT lX = 0.525731f; 
	const FFLOAT lZ = 0.850651f;

	Vector3 lPositions[12] = 
	{
		Vector3(-lX, 0.0f, lZ),  Vector3(lX, 0.0f, lZ),  
		Vector3(-lX, 0.0f, -lZ), Vector3(lX, 0.0f, -lZ),    
		Vector3(0.0f, lZ, lX),   Vector3(0.0f, lZ, -lX), 
		Vector3(0.0f, -lZ, lX),  Vector3(0.0f, -lZ, -lX),    
		Vector3(lZ, lX, 0.0f),   Vector3(-lZ, lX, 0.0f), 
		Vector3(lZ, -lX, 0.0f),  Vector3(-lZ, -lX, 0.0f)
	};

	UBIGINT lIndices[60] = 
	{
		1,4,0,  4,9,0,  4,5,9,  8,5,4,  1,8,4,    
		1,10,8, 10,3,8, 8,3,5,  3,2,5,  3,7,2,    
		3,10,7, 10,6,7, 6,11,7, 6,0,11, 6,1,0, 
		10,1,6, 11,0,9, 2,11,9, 5,2,9,  11,2,7 
	};

	pMeshData.Vertices.resize(12);
	pMeshData.Indices.resize(60);

	for(UBIGINT lCurrPosition = 0; lCurrPosition < 12; lCurrPosition++)
	{
		pMeshData.Vertices[lCurrPosition].Position = lPositions[lCurrPosition];
	}

	for(UBIGINT lCurrIndex = 0; lCurrIndex < 60; lCurrIndex++)
	{
		pMeshData.Indices[lCurrIndex] = lIndices[lCurrIndex];
	}

	for(UBIGINT lCurrPass = 0; lCurrPass < pNumSubdivisions; lCurrPass++)
	{
		GeometryHelper::Subdivide( pMeshData );
	}

	// Project vertices onto sphere and scale.
	for(UBIGINT lCurrVertex = 0; lCurrVertex < pMeshData.Vertices.size(); lCurrVertex++)
	{
		// Project onto unit sphere.
		Vector3 lPosition = pMeshData.Vertices[lCurrVertex].Position;
		Vector3 lNormal   = lPosition.Vec3Normalise();

		// Project onto sphere.
		Vector3 lNewPosition = pRadius * lNormal;

		pMeshData.Vertices[lCurrVertex].Position = lNewPosition;
		pMeshData.Vertices[lCurrVertex].Normal   = lNormal;

		// Derive texture coordinates from spherical coordinates.
		FFLOAT lTheta = AngleFromXY( pMeshData.Vertices[lCurrVertex].Position.getX(), 
									 pMeshData.Vertices[lCurrVertex].Position.getZ() );

		FFLOAT lPhi = acosf(pMeshData.Vertices[lCurrVertex].Position.getY() / pRadius);

		pMeshData.Vertices[lCurrVertex].TexCoord.x( lTheta / PISQUARE );
		pMeshData.Vertices[lCurrVertex].TexCoord.y( lPhi / PI );

		// Partial derivative of P with respect to Theta
		pMeshData.Vertices[lCurrVertex].Tangent.x( -pRadius * sinf(lPhi) * sinf(lTheta) );
		pMeshData.Vertices[lCurrVertex].Tangent.y( 0.0f );
		pMeshData.Vertices[lCurrVertex].Tangent.z( +pRadius * sinf(lPhi) * cosf(lTheta) );

		Vector4 lTangent = pMeshData.Vertices[lCurrVertex].Tangent;
		pMeshData.Vertices[lCurrVertex].Tangent = lTangent.Vec4Normalise();
	}
}

/*************************************************************************************************/
VVOID GeometryHelper::CreateCylinder(FFLOAT pBottomRadius, FFLOAT pTopRadius, FFLOAT pHeight, UBIGINT pSliceCount, UBIGINT pStackCount, MeshData& pMeshData)
{
	pMeshData.Vertices.clear();
	pMeshData.Indices.clear();

	//
	// Build Stacks.
	// 
	FFLOAT lStackHeight = pHeight / pStackCount;

	// Amount to increment radius as we move up each stack level from bottom to top.
	FFLOAT lRadiusStep = (pTopRadius - pBottomRadius) / pStackCount;

	UBIGINT lRingCount = pStackCount + 1;

	// Compute vertices for each stack ring starting at the bottom and moving up.
	for(UBIGINT lCurrRing = 0; lCurrRing < lRingCount; lCurrRing++)
	{
		FFLOAT lY = -0.5f * pHeight + lCurrRing * lStackHeight;
		FFLOAT lCurrRadius = pBottomRadius + lCurrRing * lRadiusStep;

		// vertices of ring
		FFLOAT lDTheta = 2.0f * PI / pSliceCount;
		for(UBIGINT lCurrSlice = 0; lCurrSlice <= pSliceCount; lCurrSlice++)
		{
			EnhancedVertex lCurrVertex;

			FFLOAT lCurrCosAngle = cosf(lCurrSlice * lDTheta);
			FFLOAT lCurrSinAngle = sinf(lCurrSlice * lDTheta);

			lCurrVertex.Position = Vector3(lCurrRadius * lCurrCosAngle, lY, lCurrRadius * lCurrSinAngle);

			lCurrVertex.TexCoord.x( (FFLOAT)lCurrSlice / pSliceCount );
			lCurrVertex.TexCoord.y( 1.0f - (FFLOAT)lCurrRing / pStackCount );

			// Cylinder can be parameterized as follows, where we introduce v
			// parameter that goes in the same direction as the v tex-coord
			// so that the bitangent goes in the same direction as the v tex-coord.
			//   Let r0 be the bottom radius and let r1 be the top radius.
			//   y(v) = h - hv for v in [0,1].
			//   lCurrRadius(v) = r1 + (r0-r1)v
			//
			//   x(t, v) = lCurrRadius(v)*cos(t)
			//   y(t, v) = h - hv
			//   z(t, v) = lCurrRadius(v)*sin(t)
			// 
			//  dx/dt = -lCurrRadius(v)*sin(t)
			//  dy/dt = 0
			//  dz/dt = +lCurrRadius(v)*cos(t)
			//
			//  dx/dv = (r0-r1)*cos(t)
			//  dy/dv = -h
			//  dz/dv = (r0-r1)*sin(t)

			// This is unit length.
			lCurrVertex.Tangent = Vector4( -lCurrSinAngle, 0.0f, lCurrCosAngle, 0.0f );

			FFLOAT lDr = pBottomRadius - pTopRadius;
			Vector3 lBiTangent( lDr * lCurrCosAngle, -pHeight, lDr * lCurrSinAngle );

			Vector3 lTangent   = Vector3( lCurrVertex.Tangent.getX(), lCurrVertex.Tangent.getY(), lCurrVertex.Tangent.getZ() );
			Vector3 lBinormal  = lBiTangent;
			Vector3 lNormal    = lTangent.Vec3CrossProduct(lBinormal).Vec3Normalise();
			lCurrVertex.Normal = lNormal;

			pMeshData.Vertices.push_back( lCurrVertex );
		}
	}

	// Add one because we duplicate the first and last lCurrVertex per ring
	// since the texture coordinates are different.
	UBIGINT lRingVertexCount = pSliceCount + 1;

	// Compute indices for each stack.
	for(UBIGINT lCurrRing = 0; lCurrRing < pStackCount; lCurrRing++)
	{
		for(UBIGINT lCurrSlice = 0; lCurrSlice < pSliceCount; lCurrSlice++)
		{
			pMeshData.Indices.push_back( lCurrRing * lRingVertexCount + lCurrSlice );
			pMeshData.Indices.push_back( (lCurrRing + 1) * lRingVertexCount + lCurrSlice );
			pMeshData.Indices.push_back( (lCurrRing + 1) * lRingVertexCount + lCurrSlice + 1 );

			pMeshData.Indices.push_back( lCurrRing * lRingVertexCount + lCurrSlice );
			pMeshData.Indices.push_back( (lCurrRing + 1) * lRingVertexCount + lCurrSlice + 1 );
			pMeshData.Indices.push_back( lCurrRing * lRingVertexCount + lCurrSlice + 1 );
		}
	}

	GeometryHelper::BuildCylinderTopCap( pBottomRadius, pTopRadius, pHeight, pSliceCount, pStackCount, pMeshData );
	GeometryHelper::BuildCylinderBottomCap( pBottomRadius, pTopRadius, pHeight, pSliceCount, pStackCount, pMeshData );
}

/*************************************************************************************************/
VVOID GeometryHelper::CreateGrid(FFLOAT pWidth, FFLOAT pDepth, UBIGINT pRowCount, UBIGINT pColumnCount, MeshData& pMeshData)
{
	UBIGINT lVerticesCount = pRowCount * pColumnCount;
	UBIGINT lFaceCount   = (pRowCount - 1) * (pColumnCount - 1) * 2;

	//
	// Create the vertices.
	//
	FFLOAT lHalfWidth = 0.5f * pWidth;
	FFLOAT lHalfDepth = 0.5f * pDepth;

	FFLOAT lDx = pWidth / (pColumnCount - 1);
	FFLOAT lDz = pDepth / (pRowCount - 1);

	FFLOAT lDu = 1.0f / (pColumnCount - 1);
	FFLOAT lDv = 1.0f / (pRowCount - 1);

	pMeshData.Vertices.resize(lVerticesCount);
	for(UBIGINT lCurrRow = 0; lCurrRow < pRowCount; lCurrRow++)
	{
		FFLOAT z = lHalfDepth - lCurrRow * lDz;
		for(UBIGINT lCurrColumn = 0; lCurrColumn < pColumnCount; lCurrColumn++)
		{
			FFLOAT x = -lHalfWidth + lCurrColumn * lDx;

			pMeshData.Vertices[ lCurrRow * pColumnCount + lCurrColumn ].Position = Vector3( x, 0.0f, z );
			pMeshData.Vertices[ lCurrRow * pColumnCount + lCurrColumn ].Normal   = Vector3( 0.0f, 1.0f, 0.0f );
			pMeshData.Vertices[ lCurrRow * pColumnCount + lCurrColumn ].Tangent  = Vector4( 1.0f, 0.0f, 0.0f, 0.0f );

			// Stretch texture over grid.
			pMeshData.Vertices[ lCurrRow * pColumnCount + lCurrColumn ].TexCoord.x( lCurrColumn * lDu );
			pMeshData.Vertices[ lCurrRow * pColumnCount + lCurrColumn ].TexCoord.y( lCurrRow * lDv );
		}
	}
 
    //
	// Create the indices.
	//
	pMeshData.Indices.resize( lFaceCount * 3 ); // 3 indices per face

	// Iterate over each quad and compute indices.
	UBIGINT lIndex = 0;
	for(UBIGINT lCurrRow = 0; lCurrRow < pRowCount - 1; lCurrRow++)
	{
		for(UBIGINT lCurrColumn = 0; lCurrColumn < pColumnCount - 1; ++lCurrColumn)
		{
			pMeshData.Indices[ lIndex ]     = lCurrRow * pColumnCount + lCurrColumn;
			pMeshData.Indices[ lIndex + 1 ] = lCurrRow * pColumnCount + lCurrColumn + 1;
			pMeshData.Indices[ lIndex + 2 ] = (lCurrRow + 1) * pColumnCount + lCurrColumn;

			pMeshData.Indices[ lIndex + 3 ] = (lCurrRow + 1) * pColumnCount + lCurrColumn;
			pMeshData.Indices[ lIndex + 4 ] = lCurrRow * pColumnCount + lCurrColumn + 1;
			pMeshData.Indices[ lIndex + 5 ] = (lCurrRow + 1) * pColumnCount + lCurrColumn + 1;

			lIndex += 6; // next quad
		}
	}
}

/*************************************************************************************************/
VVOID GeometryHelper::CreateFullscreenQuad(MeshData& pMeshData)
{
	pMeshData.Vertices.resize(4);
	pMeshData.Indices.resize(6);

	// Position coordinates specified in NDC space.
	pMeshData.Vertices[0] = EnhancedVertex( -1.0f, -1.0f, 0.0f, 
											0.0f, 0.0f, -1.0f,
											1.0f, 0.0f, 0.0f,
											0.0f, 1.0f);

	pMeshData.Vertices[1] = EnhancedVertex( -1.0f, +1.0f, 0.0f, 
											0.0f, 0.0f, -1.0f,
											1.0f, 0.0f, 0.0f,
											0.0f, 0.0f);

	pMeshData.Vertices[2] = EnhancedVertex( +1.0f, +1.0f, 0.0f, 
											0.0f, 0.0f, -1.0f,
											1.0f, 0.0f, 0.0f,
											1.0f, 0.0f);

	pMeshData.Vertices[3] = EnhancedVertex( +1.0f, -1.0f, 0.0f, 
											0.0f, 0.0f, -1.0f,
											1.0f, 0.0f, 0.0f,
											1.0f, 1.0f);

	pMeshData.Indices[0] = 0;
	pMeshData.Indices[1] = 1;
	pMeshData.Indices[2] = 2;

	pMeshData.Indices[3] = 0;
	pMeshData.Indices[4] = 2;
	pMeshData.Indices[5] = 3;
}
