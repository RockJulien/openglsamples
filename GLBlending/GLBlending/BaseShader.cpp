#include "BaseShader.h"

using namespace std;

CCHAR* BaseShader::LoadShaderFromFile(CCHAR* shaderPath)
{
	// This function will be in charge of loading
	// string containing shader code from file.
	ifstream inFile;
	BIGINT fileSize = 0;
	CCHAR* buffer = NULL;
	CCHAR input;

	// Open the file.
	inFile.open(shaderPath);
	if(inFile.fail())
		return NULL;

	// Read the first element.
	inFile.get(input);

	// Loop through the file
	// and grab the char count
	while(!inFile.eof())
	{
		fileSize++;
		inFile.get(input);
	}

	// Close the file
	inFile.close();

	// Now the size is known, size the buffer
	// and read the file content into.
	buffer = new CCHAR[fileSize + 1]; // +1 for NULL terminating.
	if(!buffer)
		return NULL;

	// Re-open the file.
	inFile.open(shaderPath);

	// Read the shader code into the buffer.
	inFile.read(buffer, fileSize);

	// Close the file once it is done.
	inFile.close();

	// NULL terminate the string.
	buffer[fileSize] = '\0';

	return buffer;
}

BaseShader::BaseShader() : 
IShader(), m_iVertShaderID(0), m_iPixShaderID(0), m_iMeshShader(0) 
{
	
}

BaseShader::~BaseShader()
{

}
