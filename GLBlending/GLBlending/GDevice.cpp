#include "GDevice.h"

GDevice::GDevice() :
m_deviceContext(NULL), m_renderContext(NULL)
{

}

GDevice::~GDevice()
{
	
}

BBOOL GDevice::LoadExtension()
{
	// Load all the OpenGL extensions needed.
	wglChoosePixFmtARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)wglGetProcAddress("wglChoosePixelFormatARB");
	if(!wglChoosePixFmtARB)
		return false;

	wglCreateContextAttrARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");
	if(!wglCreateContextAttrARB)
		return false;

	wglSwapIntervalExt = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
	if(!wglSwapIntervalExt)
		return false;

	glAttachShader = (PFNGLATTACHSHADERPROC)wglGetProcAddress("glAttachShader");
	if(!glAttachShader)
		return false;

	glBindBuffer = (PFNGLBINDBUFFERPROC)wglGetProcAddress("glBindBuffer");
	if(!glBindBuffer)
		return false;

	glBindVertArray = (PFNGLBINDVERTEXARRAYPROC)wglGetProcAddress("glBindVertexArray");
	if(!glBindVertArray)
		return false;

	glBufferData = (PFNGLBUFFERDATAPROC)wglGetProcAddress("glBufferData");
	if(!glBufferData)
		return false;

	glCompileShader = (PFNGLCOMPILESHADERPROC)wglGetProcAddress("glCompileShader");
	if(!glCompileShader)
		return false;

	glCreateProgram = (PFNGLCREATEPROGRAMPROC)wglGetProcAddress("glCreateProgram");
	if(!glCreateProgram)
		return false;

	glCreateShader = (PFNGLCREATESHADERPROC)wglGetProcAddress("glCreateShader");
	if(!glCreateShader)
		return false;

	glDeleteBuffers = (PFNGLDELETEBUFFERSPROC)wglGetProcAddress("glDeleteBuffers");
	if(!glDeleteBuffers)
		return false;

	glDeleteProgram = (PFNGLDELETEPROGRAMPROC)wglGetProcAddress("glDeleteProgram");
	if(!glDeleteProgram)
		return false;

	glDeleteShader = (PFNGLDELETESHADERPROC)wglGetProcAddress("glDeleteShader");
	if(!glDeleteShader)
		return false;

	glDeleteVertArrays = (PFNGLDELETEVERTEXARRAYSPROC)wglGetProcAddress("glDeleteVertexArrays");
	if(!glDeleteVertArrays)
		return false;

	glDetachShader = (PFNGLDETACHSHADERPROC)wglGetProcAddress("glDetachShader");
	if(!glDetachShader)
		return false;

	glEnableVertAttrArrays = (PFNGLENABLEVERTEXATTRIBARRAYPROC)wglGetProcAddress("glEnableVertexAttribArray");
	if(!glEnableVertAttrArrays)
		return false;

	glGenBuffers = (PFNGLGENBUFFERSPROC)wglGetProcAddress("glGenBuffers");
	if(!glGenBuffers)
		return false;

	glGenVertArrays = (PFNGLGENVERTEXARRAYSPROC)wglGetProcAddress("glGenVertexArrays");
	if(!glGenVertArrays)
		return false;

	glGetAttrLocation = (PFNGLGETATTRIBLOCATIONPROC)wglGetProcAddress("glGetAttribLocation");
	if(!glGetAttrLocation)
		return false;

	glGetProgramInfoLog = (PFNGLGETPROGRAMINFOLOGPROC)wglGetProcAddress("glGetProgramInfoLog");
	if(!glGetProgramInfoLog)
		return false;

	glGetProgramIV = (PFNGLGETPROGRAMIVPROC)wglGetProcAddress("glGetProgramiv");
	if(!glGetProgramIV)
		return false;

	glGetShaderInfoLog = (PFNGLGETSHADERINFOLOGPROC)wglGetProcAddress("glGetShaderInfoLog");
	if(!glGetShaderInfoLog)
		return false;

	glGetShaderIV = (PFNGLGETSHADERIVPROC)wglGetProcAddress("glGetShaderiv");
	if(!glGetShaderIV)
		return false;

	glLinkProgram = (PFNGLLINKPROGRAMPROC)wglGetProcAddress("glLinkProgram");
	if(!glLinkProgram)
		return false;

	glShaderSource = (PFNGLSHADERSOURCEPROC)wglGetProcAddress("glShaderSource");
	if(!glShaderSource)
		return false;

	glUseProgram = (PFNGLUSEPROGRAMPROC)wglGetProcAddress("glUseProgram");
	if(!glUseProgram)
		return false;

	glVertAttrPointer = (PFNGLVERTEXATTRIBPOINTERPROC)wglGetProcAddress("glVertexAttribPointer");
	if(!glVertAttrPointer)
		return false;

	glBindAttrLocation = (PFNGLBINDATTRIBLOCATIONPROC)wglGetProcAddress("glBindAttribLocation");
	if(!glBindAttrLocation)
		return false;

	glGetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC)wglGetProcAddress("glGetUniformLocation");
	if(!glGetUniformLocation)
		return false;

	glUniformMat4fv = (PFNGLUNIFORMMATRIX4FVPROC)wglGetProcAddress("glUniformMatrix4fv");
	if(!glUniformMat4fv)
		return false;

	glActiveTexture = (PFNGLACTIVETEXTUREPROC)wglGetProcAddress("glActiveTexture");
	if(!glActiveTexture)
		return false;

	glCompressTextImg2D = (PFNGLCOMPRESSEDTEXTIMAGE2DARB)wglGetProcAddress("glCompressedTexImage2DARB");
	if(!glCompressTextImg2D)
		return false;

	glTexImage3D = (PFNGLTEXIMAGE3DPROC)wglGetProcAddress("glTexImage3D");
	if(!glTexImage3D)
		return false;

	glCompressTextImg3D = (PFNGLCOMPRESSEDTEXIMAGE3DPROC)wglGetProcAddress("glCompressedTexImage3D");
	if(!glCompressTextImg3D)
		return false;

	glUniform1i = (PFNGLUNIFORM1IPROC)wglGetProcAddress("glUniform1i");
	if(!glUniform1i)
		return false;

	glUniform2iv = (PFNGLUNIFORM2IVPROC)wglGetProcAddress("glUniform2iv");
	if(!glUniform2iv)
	{
		return false;
	}

	glGenerateMipMap = (PFNGLGENERATEMIPMAPPROC)wglGetProcAddress("glGenerateMipmap");
	if(!glGenerateMipMap)
		return false;

	glDisableVertAttrArray = (PFNGLDISABLEVERTEXATTRIBARRAYPROC)wglGetProcAddress("glDisableVertexAttribArray");
	if(!glDisableVertAttrArray)
		return false;

	glUniform1f = (PFNGLUNIFORM1FPROC)wglGetProcAddress("glUniform1f");
	if(!glUniform1f)
		return false;

	glUniform1fv = (PFNGLUNIFORM1FVPROC)wglGetProcAddress("glUniform1fv");
	if(!glUniform1fv)
	{
		return false;
	}

	glUniform3fv = (PFNGLUNIFORM3FVPROC)wglGetProcAddress("glUniform3fv");
	if(!glUniform3fv)
		return false;

	glUniform4fv = (PFNGLUNIFORM4FVPROC)wglGetProcAddress("glUniform4fv");
	if(!glUniform4fv)
		return false;

	glDrawRangeElements = (PFNGLDRAWRANGEELEMENTSPROC)wglGetProcAddress("glDrawRangeElements");
	if(!glDrawRangeElements)
	{
		return false;
	}

	glBindRenderbuffer = (PFNGLBINDRENDERBUFFERPROC)wglGetProcAddress("glBindRenderbuffer");
	if(!glBindRenderbuffer)
	{
		return false;
	}

	glRenderbufferStorage = (PFNGLRENDERBUFFERSTORAGEPROC)wglGetProcAddress("glRenderbufferStorage");
	if(!glRenderbufferStorage)
	{
		return false;
	}

	glFramebufferRenderbuffer = (PFNGLFRAMEBUFFERRENDERBUFFERPROC)wglGetProcAddress("glFramebufferRenderbuffer");
	if(!glFramebufferRenderbuffer)
	{
		return false;
	}

	glGenFrameBuffers = (PFNGLGENFRAMEBUFFERSPROC)wglGetProcAddress("glGenFramebuffers");
	if(!glGenFrameBuffers)
	{
		return false;
	}

	glGenRenderBuffers = (PFNGLGENRENDERBUFFERSPROC)wglGetProcAddress("glGenRenderbuffers");
	if(!glGenRenderBuffers)
	{
		return false;
	}

	glBindFramebuffer = (PFNGLBINDFRAMEBUFFERPROC)wglGetProcAddress("glBindFramebuffer");
	if(!glBindFramebuffer)
	{
		return false;
	}

	glFrameBufferTexture = (PFNGLFRAMEBUFFERTEXTUREPROC)wglGetProcAddress("glFramebufferTexture");
	if(!glFrameBufferTexture)
	{
		return false;
	}

	glFrameBufferTexture1D = (PFNGLFRAMEBUFFERTEXTURE1DPROC)wglGetProcAddress("glFramebufferTexture1D");
	if(!glFrameBufferTexture1D)
	{
		return false;
	}

	glFrameBufferTexture2D = (PFNGLFRAMEBUFFERTEXTURE2DPROC)wglGetProcAddress("glFramebufferTexture2D");
	if(!glFrameBufferTexture2D)
	{
		return false;
	}

	glFrameBufferTexture3D = (PFNGLFRAMEBUFFERTEXTURE3DPROC)wglGetProcAddress("glFramebufferTexture3D");
	if(!glFrameBufferTexture3D)
	{
		return false;
	}

	glTexSubImage3D = (PFNGLTEXSUBIMAGE3DPROC)wglGetProcAddress("glTexSubImage3D");
	if(!glTexSubImage3D)
	{
		return false;
	}

	glTextureStorage1D = (PFNGLTEXTURESTORAGE1DPROC)wglGetProcAddress("glTextureStorage1DEXT");
	if(!glTextureStorage1D)
	{
		return false;
	}
	
	glTextureStorage2D = (PFNGLTEXTURESTORAGE2DPROC)wglGetProcAddress("glTextureStorage2DEXT");
	if(!glTextureStorage2D)
	{
		return false;
	}
	
	glTextureStorage3D = (PFNGLTEXTURESTORAGE3DPROC)wglGetProcAddress("glTextureStorage3DEXT");
	if(!glTextureStorage3D)
	{
		return false;
	}

	glTextureSubImage1D = (PFNGLTEXTURESUBIMAGE1DPROC)wglGetProcAddress("glTextureSubImage1DEXT");
	if(!glTextureSubImage1D)
	{
		return false;
	}

	glTextureSubImage2D = (PFNGLTEXTURESUBIMAGE2DPROC)wglGetProcAddress("glTextureSubImage2DEXT");
	if(!glTextureSubImage2D)
	{
		return false;
	}

	glBindImageTexture  = (PFNGLBINDIMAGETEXTUREPROC)wglGetProcAddress("glBindImageTexture");
	if(!glBindImageTexture)
	{
		return false;
	}

	glDeleteFrameBuffers = (PFNGLDELETEFRAMEBUFFERSPROC)wglGetProcAddress("glDeleteFramebuffers");
	if(!glDeleteFrameBuffers)
	{
		return false;
	}

	glDeleteRenderBuffers = (PFNGLDELETERENDERBUFFERSPROC)wglGetProcAddress("glDeleteRenderbuffers");
	if(!glDeleteRenderBuffers)
	{
		return false;
	}

	glDrawBuffers = (PFNGLDRAWBUFFERSPROC)wglGetProcAddress("glDrawBuffers");
	if(!glDrawBuffers)
	{
		return false;
	}

	glDrawElementsBaseVertex = (PFNGLDRAWELEMENTSBASEVERTEXPROC)wglGetProcAddress("glDrawElementsBaseVertex");
	if(!glDrawElementsBaseVertex)
	{
		return false;
	}

	glDrawElementsInstancedBaseVertex = (PFNGLDRAWELEMENTSINSTANCEDBASEVERTEXPROC)wglGetProcAddress("glDrawElementsInstancedBaseVertex");
	if( !glDrawElementsInstancedBaseVertex )
	{
		return false;
	}

	glCheckFrameBufferStatus = (PFNGLCHECKFRAMEBUFFERSTATUSPROC)wglGetProcAddress("glCheckFramebufferStatus");
	if(!glCheckFrameBufferStatus)
	{
		return false;
	}

	glClearBufferiv  = (PFNGLCLEARBUFFERIVPROC)wglGetProcAddress("glClearBufferiv");
	if(!glClearBufferiv)
	{
		return false;
	}

	glClearBufferuiv = (PFNGLCLEARBUFFERUIVPROC)wglGetProcAddress("glClearBufferuiv");
	if(!glClearBufferuiv)
	{
		return false;
	}

	glClearBufferfv  = (PFNGLCLEARBUFFERFVPROC)wglGetProcAddress("glClearBufferfv");
	if(!glClearBufferfv)
	{
		return false;
	}

	glClearBufferfi  = (PFNGLCLEARBUFFERFIPROC)wglGetProcAddress("glClearBufferfi");
	if(!glClearBufferfi)
	{
		return false;
	}

	glMapBuffer = (PFNGLMAPBUFFERPROC)wglGetProcAddress("glMapBuffer");
	if(!glMapBuffer)
	{
		return false;
	}

	glUnmapBuffer = (PFNGLUNMAPBUFFERPROC)wglGetProcAddress("glUnmapBuffer");
	if(!glUnmapBuffer)
	{
		return false;
	}

	glBlendEquationSeparate = (PFNGLBLENDEQUATIONSEPARATEPROC)wglGetProcAddress("glBlendEquationSeparate");
	if(!glBlendEquationSeparate)
	{
		return false;
	}

	glBlendFuncSeparate = (PFNGLBLENDFUNCSEPARATEPROC)wglGetProcAddress("glBlendFuncSeparate");
	if(!glBlendFuncSeparate)
	{
		return false;
	}

	glBlendColor = (PFNGLBLENDCOLORPROC)wglGetProcAddress("glBlendColor");
	if(!glBlendColor)
	{
		return false;
	}

	glDispatchCompute = (PFNGLDISPATCHCOMPUTEPROC)wglGetProcAddress("glDispatchCompute");
	if(!glDispatchCompute)
	{
		return false;
	}

	return true;
}

BBOOL  GDevice::InitializeExtensions(HWND winInst)
{
	HDC deviceCont;
	HGLRC renderCont;
	PIXELFORMATDESCRIPTOR pixFmt;
	BIGINT error;
	BBOOL hr;

	// Get the device context.
	deviceCont = GetDC(winInst);
	if(!deviceCont)
		return false;

	// Set temporary pixFmt
	error = SetPixelFormat(deviceCont, 1, &pixFmt);
	if(error != 1)
		return false;

	// Set temporary render context.
	renderCont = wglCreateContext(deviceCont);
	if(!renderCont)
		return false;

	// Set the render context as the one for the current win.
	error = wglMakeCurrent(deviceCont, renderCont);
	if(error != 1)
		return false;

	// Initialize the xtensions.
	hr = LoadExtension();
	if(!hr)
		return false;

	// Release the render context.
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(renderCont);
	renderCont = NULL;

	// Release the device context.
	ReleaseDC(winInst, deviceCont);
	deviceCont = NULL;

	return true;
}

BBOOL  GDevice::InitializeDevice(HWND winInst, BIGINT width, BIGINT height, FFLOAT fFar, FFLOAT fNear, BBOOL vsync)
{
	BIGINT  attrList19[19];
	BIGINT  pixFmt[1];
	UBIGINT fmtCount;
	BIGINT  hr;
	PIXELFORMATDESCRIPTOR pixFmtDesc;
	BIGINT  attrList5[5];
	CCHAR*  GPUName, *APIName;

	// Get the device context.
	m_deviceContext = GetDC(winInst);
	if(!m_deviceContext)
		return false;

	GLenum lError = glGetError();

	// Prepare the attr list for support checking.
	// OpenGL support.
	attrList19[0] = WGL_SUPPORT_OPENGL_ARB;
	attrList19[1] = TRUE;

	// Render to window support.
	attrList19[2] = WGL_DRAW_TO_WINDOW_ARB;
	attrList19[3] = TRUE;

	// Graphic acceleration support
	attrList19[4] = WGL_ACCELERATION_ARB;
	attrList19[5] = WGL_FULL_ACCELERATION_ARB;

	// 24Bits Color support.
	attrList19[6] = WGL_COLOR_BITS_ARB;
	attrList19[7] = 24;

	// 24Bits Depth support.
	attrList19[8] = WGL_DEPTH_BITS_ARB;
	attrList19[9] = 24;

	// Double buffer support.
	attrList19[10] = WGL_DOUBLE_BUFFER_ARB;
	attrList19[11] = TRUE;

	// Swapping chain support.
	attrList19[12] = WGL_SWAP_METHOD_ARB;
	attrList19[13] = WGL_SWAP_EXCHANGE_ARB;

	// Alpha support
	attrList19[14] = WGL_PIXEL_TYPE_ARB;
	attrList19[15] = WGL_TYPE_RGBA_ARB;

	// 8Bits Stencil buffer support.
	attrList19[16] = WGL_STENCIL_BITS_ARB;
	attrList19[17] = 8;

	// Null Terminate the list.
	attrList19[18] = NULL;

	// Check for an appropriate pixel format.
	hr = wglChoosePixFmtARB(m_deviceContext, attrList19, NULL, 1, pixFmt, &fmtCount);
	if(hr != 1)
		return false;

	lError = glGetError();

	// If support the desired pixel fmt, set as current.
	hr = SetPixelFormat(m_deviceContext, pixFmt[0], &pixFmtDesc);
	if(hr != 1)
		return false;

	lError = glGetError();

	// Then, Set the version of openGL.
	attrList5[0] = WGL_CONTEXT_MAJOR_VERSION_ARB;
	attrList5[1] = 4; // 4.0 and up to 4.last version.
	attrList5[2] = WGL_CONTEXT_MINOR_VERSION_ARB;
	attrList5[3] = 0;

	// Null Terminate the list.
	attrList5[4] = NULL;

	// Create the OpenGL render context.
	m_renderContext = wglCreateContextAttrARB(m_deviceContext, NULL, attrList5);
	if(!m_renderContext)
		return false;

	lError = glGetError();

	// Set the current render context
	hr = wglMakeCurrent(m_deviceContext, m_renderContext);
	if(hr != 1)
		return false;

	lError = glGetError();

	// Clear the depth buffer.
	glClearDepth(1.0f);

	// Enable depth testing
	glEnable(GL_DEPTH_TEST);

	// Set the polygon winding as clockwise for left-handed system.
	glFrontFace(GL_CW);

	// Enable back face culling.
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	// Grab the GPU and device names.
	GPUName = (CCHAR*)glGetString(GL_VENDOR);
	APIName = (CCHAR*)glGetString(GL_RENDERER);

	// Store above info in the class array
	strcpy_s(m_cGPUDesc, GPUName);
	strcat_s(m_cGPUDesc, "-");
	strcat_s(m_cGPUDesc, APIName);

	// Turn on or not the vertical synchronisation
	if(vsync)
		hr = wglSwapIntervalExt(1); // 60.
	else
		hr = wglSwapIntervalExt(0); // all cycles you can give me.

	lError = glGetError();

	// Check vsync.
	if(hr != 1)
		return false;

	return true;
}

BBOOL GDevice::StartRender(BBOOL clearPix, BBOOL clearDepthStencil)
{
	// Set the clear color for clearing buffers.
	glClearColor( 0.0f, 0.0f, 0.0f, 1.0f ); // Black
	glClearDepth( 1.0f );

	if
		( clearPix )
	{
		glClear(GL_COLOR_BUFFER_BIT);
	}

	if
		( clearDepthStencil )
	{
		glClear(GL_DEPTH_BUFFER_BIT);
	}

	return true;
}

VVOID  GDevice::StopRender()
{
	// Present the back buffer to the screen.
	SwapBuffers(m_deviceContext); // Equal to Present( vsync, ..) in DirectX
}

VVOID  GDevice::Release(HWND winInst)
{
	// Release the render context.
	if(m_renderContext)
	{
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(m_renderContext);
		m_renderContext = 0;
	}

	// Release the device context.
	if(m_deviceContext)
	{
		ReleaseDC(winInst, m_deviceContext);
		m_deviceContext = NULL;
	}
}
