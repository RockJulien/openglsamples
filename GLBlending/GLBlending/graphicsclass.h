////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _GRAPHICSCLASS_H_
#define _GRAPHICSCLASS_H_

///////////////////////
// INCLUDES //
///////////////////////
#include "GDevice.h"
#include "Camera\OcelotCamera.h"
#include "Camera\OcelotCameraFactory.h"
#include "GeometryHelper.h"
#include "OcelotLight.h"
#include "Material.h"
#include "Wave.h"
#include "Effects\Effects.h"
#include "Effects\InputLayouts.h"
#include "TextureManager\TextureManager.h"

using namespace Ocelot;

/////////////
// GLOBALS //
/////////////
const BBOOL  FULL_SCREEN   = false;
const BBOOL  VSYNC_ENABLED = true;
const FFLOAT SCREEN_DEPTH  = 1.0f;
const FFLOAT SCREEN_NEAR   = 0.1f;

///////////////////////
// Rendering Options //
///////////////////////
enum RenderOptions
{
	eLighting = 0,
	eTextures = 1,
	eTexturesAndFog = 2
};

////////////////////////////////////////////////////////////////////////////////
// Class name: GraphicsClass
////////////////////////////////////////////////////////////////////////////////
class GraphicsClass
{
public:

	// Constructor & Destructor
	GraphicsClass();
	~GraphicsClass();

	// Methods
	BBOOL Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight);
	VVOID OnResize(BIGINT pClientWidth, BIGINT pClientHeight);
	VVOID Shutdown();
	BBOOL Update(FFLOAT pDeltaTime);
	BBOOL Render();

	// Accessors
	IOcelotCameraController* Controller();
	VVOID SetOption(RenderOptions pOption);

private:

	// Forbidden
	GraphicsClass(const GraphicsClass&);

	// Private Methods
	FFLOAT  GetHillHeight( FFLOAT pX, FFLOAT pZ ) const;
	Vector3 GetHillNormal( FFLOAT pX, FFLOAT pZ ) const;
	VVOID   BuildLandGeometryBuffers();
	VVOID   BuildWaveGeometryBuffers();
	VVOID   BuildCrateGeometryBuffers();

private:

	// Attributes
	// Graphic device
	OpenGLClass*             mOpenGL;

	// Camera
	OcelotCamera*            mCamera;
	IOcelotCameraController* mController;
	
	// Buffers
	UBIGINT					 mLandVAO;
	UBIGINT					 mLandVB;
	UBIGINT					 mLandIB;
	UBIGINT					 mWaveVAO;
	UBIGINT					 mWaveVB;
	UBIGINT					 mWaveIB;
	UBIGINT					 mBoxVAO;
	UBIGINT					 mBoxVB;
	UBIGINT					 mBoxIB;

	// Wave
	Wave					 mWaves;

	// Lights
	Ocelot::OcelotParallelLight mDirLights[3];
	Material				    mLandMat;
	Material				    mWaveMat;
	Material					mBoxMat;

	// Transforms
	// Define transformations from local spaces to world space.
	Matrix4x4		 mLandWorld;
	Matrix4x4		 mWaveWorld;
	Matrix4x4		 mBoxWorld;
	Matrix4x4		 mLandTexTransform;
	Matrix4x4		 mWaveTexTransform;

	// Index and Offset Count
	UBIGINT			 mLandIndexCount;
	Vector2			 mWaterTexOffset;

	// Texture Manager
	TextureManager   mTexManager;
	UBIGINT			 mLandMapRV;
	UBIGINT			 mWaveMapRV;
	UBIGINT			 mBoxMapRV;

	UBIGINT			 mDiffuseTexUnit;

	// Effects
	EffectTechnique* mLight3Tech;
	EffectTechnique* mLight3TexTech;
	EffectTechnique* mLight3TexFogTech;
	EffectTechnique* mLight3TexAlphaClipTech;
	EffectTechnique* mLight3TexAlphaClipFogTech;

	EffectTechnique* mBoxTech;
	EffectTechnique* mLandNWaveTech;

	// Options
	RenderOptions    mRenderOptions;
};

#endif