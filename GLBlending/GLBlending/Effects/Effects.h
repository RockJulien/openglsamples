#ifndef DEF_EFFECTS_H
#define DEF_EFFECTS_H

// Includes
#include "BasicEffect.h"

// Namespaces

// Class definition
class Effects
{
private:

	// Attributes
	static BasicEffect*	sBasicFX;
	
public:

	// Methods
	static VVOID InitializeAll(GDevice* pDevice);
	static VVOID DestroyAll();

	// Shaders handles
	static BasicEffect*	BasicFX();

};

#endif