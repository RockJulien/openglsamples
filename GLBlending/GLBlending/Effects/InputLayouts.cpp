#include "InputLayouts.h"
#include "..\VertexTypes.h"
#include "..\GDevice.h"

// Namespaces used
using namespace Ocelot;

// Static variables initialization
InputLayout* InputLayouts::sPosNormalTexLayout = NULL;

/******************************************************************************************************/
VVOID InputLayouts::InitializeAll(GDevice* pDevice)
{
	if( InputLayouts::sPosNormalTexLayout == NULL )
	{
		UBIGINT lStride = sizeof(SimpleVertex);
		InputLayouts::sPosNormalTexLayout = new InputLayout( pDevice );
		InputLayouts::sPosNormalTexLayout->AddElement(InputElementDesc("PosL",    0, lStride, 3, GL_FLOAT));
		InputLayouts::sPosNormalTexLayout->AddElement(InputElementDesc("NormalL", 1, lStride, 3, GL_FLOAT, (UCCHAR*)NULL + (3 * sizeof(FFLOAT))));
		InputLayouts::sPosNormalTexLayout->AddElement(InputElementDesc("TexIn",   2, lStride, 2, GL_FLOAT, (UCCHAR*)NULL + (6 * sizeof(FFLOAT))));
	}
}

/******************************************************************************************************/
VVOID InputLayouts::DestroyAll()
{
	DeletePointer( InputLayouts::sPosNormalTexLayout );
}

/******************************************************************************************************/
InputLayout* InputLayouts::PosNormalTexLayout()
{
	return InputLayouts::sPosNormalTexLayout;
}
